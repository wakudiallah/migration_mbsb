<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GetTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:table1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get All table name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $dbname = \DB::connection('mysql')->getDatabaseName();

        
        $tables = \DB::connection('mysql')->select('SHOW TABLES'); // returns an array of stdObjects

        
        // tables.blade.php
        //foreach($tables as $table){
            //dd($table->db1);  // you need to know the name of your database in advance
        //}
        


        $tbl = \DB::connection('mysql2')->getDoctrineSchemaManager()->listTableNames();

        dd($tbl);


    }
}
