<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrameworkController extends Controller
{
    
    public function CodeAcademics(Request $request)
    {

        $table = CodeAcademics::all();
        $filename = "tweets.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Id', 'CreatedBy', 'CreatedDate', 'Act', 'AcadCode', 'AcadDesc'));

        foreach($table as $row) {
            $today = date("Y-m-d H:i:s");

            fputcsv($handle, array($row['Id'], $row['CreatedBy'], '"'.$today, $row['Act'], $row['AcadCode'], $row['AcadDesc']));
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        return Response::download($filename, 'tweets.csv', $headers);

    }

    
}
