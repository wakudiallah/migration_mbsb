<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Ramsey\Uuid\Uuid;
use App\Models\Table1\codebeneficiary;

class Parameter1Controller extends Controller
{
    public function Parameter1(Request $request)
    {
        

        $today = date("Y-m-d H:i:s");

        $status = true;
        
        /* ========= CodeAcademics ========= */

        $datas = DB::connection('sqlsrv')
                ->table('codeAcademic')
               ->select('*')
               ->get();
        

        foreach($datas as $data){
            DB::connection('sqlsrv2')
            ->table('CodeAcademics')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'AcadCode' => $data->AcadCode,
                'AcadDesc' => $data->AcadDesc
            ]);
        }


        $table_name = 'CodeAcademics';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeAcademic')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeAcademics')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);


       



        $datas = DB::connection('sqlsrv')
                ->table('codeaddtype')
                ->select('*')
                ->get();
        

        foreach($datas as $data){
            DB::connection('sqlsrv2')
            ->table('CodeAddTypes')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' => $today,
                'Act' => true,
                'AddTyCode' => $data->AddTyCode,
                'AddTyDesc' => $data->AddTyDesc
            ]);
        }


        $table_name = 'CodeAddTypes';


        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeaddtype')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeAddTypes')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }


        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);






        $datas = DB::connection('sqlsrv')
                ->table('CodeAdminFee')
               ->select('*')
               ->get();
        

        foreach($datas as $data){

            if($data->Active == 1){
                $status = true;
            }
            else{
                $status = false;
            }
            DB::connection('sqlsrv2')
            ->table('CodeAdminFees')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => $status,
                'AdminFeeCode' => $data->AdminFeeCode,
                'AdminFeeAmt' => $data->AdminFeeAmt,
                'AdminFeedesc' => $data->AdminFeeDesc,
                'BenePaymentDesc' => $data->BenePaymentDesc,
                'AdminFeeGLCode' => $data->AdminFeeGLCode,
                'GSTRate' => $data->GSTRate,
                'GSTTaxCode' => $data->GSTTaxCode,
                'GSTChgTy' => $data->GSTChgTy
            ]);
        }


        $table_name = 'CodeAdminFees';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('CodeAdminFee')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeAdminFees')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);





        $datas = DB::connection('sqlsrv')
                ->table('codeAGBranch')
               ->select('*')
               ->get(); 
        

        foreach($datas as $data){
            DB::connection('sqlsrv2')
            ->table('CodeAGBranches')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' => $today,
                'Act' => true,
                'AGBranchCode' => $data->AGBranchCode,
                'AGBranchDesc' => $data->AGBranchDesc,
                'AGBranchStatus' => true
            ]);
        }


        $table_name = 'CodeAGBranches';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeAGBranch')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeAGBranches')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);





        $datas = DB::connection('sqlsrv')
                ->table('codeAGDept')
               ->select('*')
               ->get();
        

        foreach($datas as $data){
            if($data->AGDeptStatus == 1){
                $status = true;
            }
            else{
                $status = false;
            }
            
            DB::connection('sqlsrv2')
            ->table('CodeAGDepts')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' => $today,
                'Act' => true,
                'AGDeptCode' => $data->AGDeptCode,
                'AGDeptDesc' => $data->AGDeptDesc,
                'AGDeptStatus' => $status
            ]);
        }


        $table_name = 'CodeAGDepts';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeAGDept')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeAGDepts')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);





        $datas = DB::connection('sqlsrv')
                ->table('codeAGState')
               ->select('*')
               ->get();
        

        foreach($datas as $data){
            if($data->AGStateStatus == 'Y'){
                $status = true;
            }
            else{
                $status = false;
            }

            DB::connection('sqlsrv2')
            ->table('CodeAGStates')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'AGStateCode' => $data->AGStateCode,
                'AGStateDesc' => $data->AGStateDesc
            ]);
        }


        $table_name = 'CodeAGStates';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeAGState')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeAGStates')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);





        $datas = DB::connection('sqlsrv')
                ->table('codeAkadScript')
               ->select('*')
               ->get();
        

        foreach($datas as $data){
            DB::connection('sqlsrv2')
            ->table('CodeAkadScripts')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,                  
                'AkadCode' => $data->AkadCode,
                'AkadDesc' => $data->AkadDesc
            ]);
        }


        $table_name = 'CodeAkadScripts';


        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeAkadScript')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeAkadScripts')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }


        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);




        $datas = DB::connection('sqlsrv')
                ->table('codeappvaut')
               ->select('*')
               ->get();
        

        foreach($datas as $data){
            DB::connection('sqlsrv2')
            ->table('CodeAppAuts')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'AppvAutCode' => $data->AppvAutCode,
                'AppvAutDesc' => $data->AppvAutDesc
            ]);
        }


        $table_name = 'CodeAppAuts';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeappvaut')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeAppAuts')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);

                    

        $datas = DB::connection('sqlsrv')
                ->table('codeappstatusfix')
               ->select('*')
               ->get();
        

        foreach($datas as $data){
            DB::connection('sqlsrv2')
            ->table('CodeAppStatuss')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'ApplStatCode' => $data->AppStatCode,
                'ApplStatDesc' => $data->AppStatDesc
            ]);
        }


        $table_name = 'CodeAppStatuss';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeappstatusfix')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeAppStatuss')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);


        $datas = DB::connection('sqlsrv')
                ->table('CodeARB')
               ->select('*')
               ->get();
        

        foreach($datas as $data){

            if($data->Active == 'Y'){
                $status = true;
            }
            else{
                $status = false;
            }
            DB::connection('sqlsrv2')
            ->table('CodeARBs')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'ARBCode' => $data->ARBCode,       
                "ARBType"=> $data->ARBType,
                "ARBDesc"=> $data->ARBDesc,
                "ARBAmt"=> $data->ARBAmt,
                "ARBCommAmt"=> $data->ARBCommAmt,
                "ARBTotal"=> $data->ARBTotal,
                "ARBGLCode"=> $data->ARBGLCode,
                "ARBPaymentDesc"=> $data->ARBPaymentDesc,
                "ARBCommGLCode"=> $data->ARBCommGLCode,
                "ARBCommPaymentDesc"=> $data->ARBCommPaymentDesc,
                "Banca2Code"=> $data->Banca2Code,
                "GSTRate"=> $data->GSTRate,
                "GSTTaxCode"=> $data->GSTTaxCode,
                "GSTChgTy"=> $data->GSTChgTy


            ]);
        }


        $table_name = 'CodeARBs';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('CodeARB')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeARBs')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);




        $datas = DB::connection('sqlsrv')
                ->table('codeBankrup')
               ->select('*')
               ->get();
        

        foreach($datas as $data){
            DB::connection('sqlsrv2')
            ->table('CodeBankrups')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'BankrupCode' => $data->BankrupCode,
                'BankrupDesc' => $data->BankrupDesc
            ]);
        }


        $table_name = 'CodeBankrups';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeBankrup')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeBankrups')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);






        $datas = DB::connection('sqlsrv')
                ->table('codebank')
                ->select('*')
                ->get();
        
              
        foreach($datas as $data){
            DB::connection('sqlsrv2')
            ->table('CodeBanks')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' => $today,
                'Act' => true,
                'BankCode' => $data->BankCode,
                'BankDesc' => $data->BankDesc
            ]);
        }
        

        $table_name = 'CodeBanks';


        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codebank')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeBanks')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }


        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);




        $datas = codebeneficiary::get();

        /*$datas = DB::connection('sqlsrv')
                ->table('codebeneficiary')
                ->join('sqlsrv2.CodeBeneBank', 'sqlsrv2.CodeBeneBank.BeneBankCode', '=', 'sqlsrv.codebeneficiary.BeneCode')
                ->select('*')
                ->get();    */


        foreach($datas as $data){

            $zone = CodeClrZones::where('ClrZoneCode', $data->BeneClrZone)->first();


            if($data->BeneAct == 1){
                $status = true;
            }
            else{
                $status = false;
            }
            DB::connection('sqlsrv2')
            ->table('CodeBeneficiarys')
            ->insert([
                'BeneId' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'Addr1' => $data->BeneAdd1,
                'Addr2' => $data->BeneAdd2,
                'Addr3' => $data->BeneAdd3,
                /*'Postcode' => $data->
                'City' => $data->
                'State' => $data->*/
                'BeneCode' => $data->BeneCode,
                'BeneRegNo' => $data->BeneRegNo,
                'BeneDesc' => $data->BeneDesc,
                'BeneBank' => $data->codebeneficiary_to_CodeBeneBank->Id ?? NULL,
                'BeneACNo' => $data->BeneACNo,
                'BenePayeeName' => $data->BenePayeeName,
                'BenePaymentDesc' => $data->BenePaymentDesc,
                'BeneCharges' => $data->BeneCharges,
                'BeneMOPCode' => $data->codebeneficiary_to_CodeModeOfPayments->Id ?? NULL,
                'BeneGLCode' => $data->codebeneficiary_to_CodeGLs->Id ?? NULL,
                'BeneGLCode2' => $data->BeneGLCode2,
                'BeneDeliMet'=> $data->codebeneficiary_to_CodeDelimets->Id ?? NULL,
                'BeneDispTo'=> $data->codebeneficiary_to_CodeDispatchTos->Id ?? NULL,
                'BenePayTab'=> $data->BenePayTab,
                'BeneClearingZone'=> $zone->codebeneficiary_to_CodeClrZones->Id ?? NULL 
                


            ]);
        }


        $table_name = 'CodeBeneficiarys';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codebeneficiary')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeBeneficiarys')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);




        $datas = DB::connection('sqlsrv')
                ->table('codeBumi')
               ->select('*')
               ->get();
        

        foreach($datas as $data){
            DB::connection('sqlsrv2')
            ->table('CodeBumis')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'BumiCode' => $data->BumiCode,
                'BumiDesc' => $data->BumiDesc,
                'BumiBNMCode' => $data->BumiBNMCode
            ]);
        }


        $table_name = 'CodeBumis';

        $count_data_origin = DB::connection('sqlsrv')
                        ->table('codeBumi')
                        ->count();


        $count_data_destination = DB::connection('sqlsrv2')
                        ->table('CodeBumis')
                        ->count();


        if($count_data_origin == $count_data_destination){
            $status = 1;
        }else{
            $status = 0;
        }

        $insert_data = DB::connection('sqlsrv3')
                        ->table('migrated_statuses')
                        ->insert([
                        'table' => $table_name, 
                        'total_row_origin'=> $count_data_origin, 
                        'total_row_destination' =>  $count_data_destination,
                        'status' => $status
                    ]);

        



    }
}
