<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Schema;
use DateTime;
use Storage;
use Carbon;
use App\Models\Table1\appldisburse;
use App\Models\Table1\applapprove;
use App\Models\Table1\applchecklist;
use App\Models\Table1\applrefnotabtemp;
use App\Models\Table1\applrefnotab;
use App\Models\Table1\applkpi;
use App\Models\Table1\applworkflow;
use App\Models\Table1\bla;
use App\Models\Table1\address;
use App\Models\Table1\cif;
use App\Models\Table1\caaffacility;
use App\Models\Table1\caafredemption;
use App\Models\Table1\caaftab;
use App\Models\Table1\caafunsexposure;
use App\Models\Table1\calabsenttab;
use App\Models\Table1\CCRISTaxonomy;
use App\Models\Table1\DSP_CifDetailGST;
use App\Models\Table1\DSP_CifDetailGST_1;
use App\Models\Table1\DSP_CIFMastDetail;
use App\Models\Table1\DSP_CIFMastDetail_1;
use App\Models\Table1\DSP_EmpDetail;
use App\Models\Table1\DSP_EmpDetail_1;
use App\Models\Table1\Dsp_GetCifNo;
use App\Models\Table1\DSP_GetCifNo_1;
use App\Models\Table1\guarantor;
use App\Models\Table1\sumcurrentapplstat;
use App\Models\Table1\usertasklist;
use App\Models\Table1\submissiontype;
use App\Models\Table1\iSMS_InComp;
use App\Models\Table1\DCA_financial;
use App\Models\Table1\cmscaafredemption;
use App\Models\Table1\DSP_AAFacility;
use App\Models\Table1\DSP_AccountCreation;
use App\Models\Table1\DSP_IslamicFile;



use App\Models\Table2\ApplRefNoTabTemps;


class ProcessController extends Controller
{
    
	
	public function ApplRefNoTabs(Request $request)
    {
    	
        ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	$date_start = date("Y-m-d H:i:s");
    	       

        $datas = applrefnotab::
        get();


		foreach($datas as $data){

			if($data->CCRIS_Chk == "Y"){
				$CCRIS_Chk = true;	
			}else{
				$CCRIS_Chk = false;
			}

			if($data->Akad == "Y"){
				$Akad = true;
			}else{
				$Akad = false;
			}


			if($data->MiCOBMntFlag == "Y"){
				$MiCOBMntFlag = true;
			}else{
				$MiCOBMntFlag = false;
			}


			if($data->MiCOBFlag == "Y"){
				$MiCOBFlag = true;
			}else{
				$MiCOBFlag = false;
			}

			if($data->DSP_DisbEC == "Y"){
				$DSP_DisbEC = true;
			}else{
				$DSP_DisbEC = false;
			}

			if($data->DSP_BlacklistSp == "Y"){
				$DSP_BlacklistSp = true;
			}else{
				$DSP_BlacklistSp = false;
			}


			if($data->DSP_CifSp == "Y"){
				$DSP_CifSp = true;
			}else{
				$DSP_CifSp = false;
			}

			if($data->DSP_DisbBulk == "Y"){
				$DSP_DisbBulk = true;
			}else{
				$DSP_DisbBulk = false;
			}

			
			if($data->DSP_Disb == "Y"){
				$DSP_Disb = true;
			}else{
				$DSP_Disb = false;
			}


			if($data->DSP_StagPayment == "Y"){
				$DSP_StagPayment = true;
			}else{
				$DSP_StagPayment = false;
			}


			if($data->DSP_AccountCreation == "Y"){
				$DSP_AccountCreation = true;
			}else{
				$DSP_AccountCreation = false;
			}

			if($data->DSP_IslamicFile == "Y"){
				$DSP_IslamicFile = true;
			}else{
				$DSP_IslamicFile = false;
			}

			if($data->DSP_AAFacility == "Y"){
				$DSP_AAFacility = true;
			}else{
				$DSP_AAFacility = false;
			}


			if($data->DSP_Blacklist == "Y"){
				$DSP_Blacklist = true;
			}else{
				$DSP_Blacklist = false;
			}


			if($data->DSP_Cif == "Y"){
				$DSP_Cif = true;
			}else{
				$DSP_Cif = false;
			}

			if($data->ccris_Det_chk == "Y"){
				$ccris_Det_chk = true;
			}else{
				$ccris_Det_chk = false;
			}


			if($data->ccris_Sum_chk == "Y"){
				$ccris_Sum_chk = true;
			}else{
				$ccris_Sum_chk = false;
			}


			if($data->CCRIS_OldCase == "Y"){
				$CCRIS_OldCase = true;
			}else{
				$CCRIS_OldCase = false;
			}

			if($data->JobConfirm == "Y"){
				$JobConfirm = true;
			}else{
				$JobConfirm = false;
			}

			if($data->SMSRejFlag == "Y"){
				$SMSRejFlag = true;
			}else{
				$SMSRejFlag = false;
			}

	
			if($data->SMSInComFlag == "Y"){
				$SMSInComFlag = true;
			}else{
				$SMSInComFlag = false;
			}	


			if($data->LiveFlag == "Y"){
				$LiveFlag = true;
			}else{
				$LiveFlag = false;
			}	

			if($data->SMSFlag == "Y"){
				$SMSFlag = true;
			}else{
				$SMSFlag = false;
			}

			if($data->TypeComodity == "YES"){
				$TypeComodity = true;
			}else{
				$TypeComodity = false;
			}	


			if($data->ManualAkad == "Y"){
				$ManualAkad = true;
			}else{
				$ManualAkad = false;
			}	

			/*if($data->TypeAkad == 2){ //Tukar 
				$TypeAkad = true;
			}else{
				$TypeAkad = false;
			}  //End Tukar*/


			if($data->TradeFlag == "Y"){
				$TradeFlag = true;
			}else{
				$TradeFlag = false;
			}


			if($data->SMSResendFlag == 1){
				$SMSResendFlag = true;
			}else{
				$SMSResendFlag = false;
			}

			if($data->Phone_Chk == "Y"){
				$Phone_Chk = true;
			}else{
				$Phone_Chk = false;
			}


			if($data->CSUFlag == 1){
				$CSUFlag = true;
			}else{
				$CSUFlag = false;
			}


			if($data->Teman == "Y"){
				$Teman = true;
			}else{
				$Teman = false;
			}


			if($data->Wadiah == "Y"){
				$Wadiah = true;
			}else{
				$Wadiah = false;
			}

			if($data->BL == "Y"){
				$BL = true;
			}else{
				$BL = false;
			}

			if($data->OA == "Y"){
				$OA = true;
			}else{
				$OA = false;
			}


			if($data->DocComB == "Y"){
				$DocComB = true;
			}else{
				$DocComB = false;
			}


			if($data->DocCom == "Y"){
				$DocCom = true;
			}else{
				$DocCom = false;
			}

			if($data->ManualAkad == "Y"){
				$ManualAkad = true;
			}else{
				$ManualAkad = false;
			}
			
		

			//AppliedDate 
			if (DateTime::createFromFormat('Y-m-d H:i:s', $data->SMSReqDt) == FALSE) {
			  $SMSReqDt = "";
			}
			else{
				$SMSReqDt = $data->SMSReqDt;
			}
			//End  AppliedDate


			DB::connection('sqlsrv2')
			->table('ApplRefNoTabs')
		    ->insert([
		    	
				//"CountId"=>   $data->	
				"ApplId"=>   $data->applrefnotab_to_ApplRefNoTabTemps->ApplId ?? NULL,	
				"DocCom"=>   $DocCom,
				"DocComB"=>   $DocComB,
				"LnPackCode"=>   $data->LnPackCode,
				"Akad"=>   $Akad,
				"Oa"=>   $OA,
				"Bl"=>   $BL,
				"Arb"=>   $data->ARB,
				"Wadiah"=>   $Wadiah,
				"WadBranch"=>   $data->WadBranch,
				"Teman"=>   $Teman,
				"Csuflag"=>   $CSUFlag,
				"Isave"=>   $data->ISave,
				"PayMode"=>   $data->PayMode,
				"PayBank"=>   $data->PayBank,
				"AcNo"=>   $data->AcNo,
				"PhoneChk"=>   $Phone_Chk,
				"PhoneChkResult"=>   $data->Phone_Chk_Result,
				"SmsreqDt"=> date('Y-m-d H:i:s', strtotime($data->SMSReqDt)),
				"SmsreplyDt"=> date('Y-m-d H:i:s', strtotime($data->SMSReplyDt)),
				"Smsresult"=>   $data->SMSResult,
				"SmsresendFlag"=>   $SMSResendFlag,
				"ModePayment"=>   $data->ModePayment,
				"TradeFlag"=>   $TradeFlag,
				"TradeDt"=>    date('Y-m-d H:i:s', strtotime($data->TradeDt)),
				"TradeResult"=>   $data->TradeResult,
				"TradeSystemNo"=>   $data->TradeSystemNo,
				"TypeAkad"=>   $data->TypeAkad,
				"UsrResponse"=>   $data->UsrResponse,
				"CallBy"=>   $data->CallBy,
				"ManualAkadRmk"=>   $data->ManualAkad_Rmk,
				"SmsRmk"=>   $data->SMS_Rmk,
				"ManualAkad"=>   $ManualAkad,
				"TypeComodity"=>   $TypeComodity,
				"Wakalah"=>   $data->Wakalah,
				"Smsflag"=>   $SMSFlag,
				"SendBy"=>   $data->SendBy,
				"LiveFlag"=>   $LiveFlag,
				"SmsinComFlag"=>   $SMSInComFlag,
				"SmsinComDt"=>   date('Y-m-d H:i:s', strtotime($data->SMSInComDt)),
				"SmsinComStatus"=>   $data->SMSInComStatus,
				"SmsinComSendBy"=>   $data->SMSInComSendBy,
				"SmsrejFlag"=>   $SMSRejFlag,
				"SmsrejDt"=>  date('Y-m-d H:i:s', strtotime($data->SMSRejDt)),
				"SmsrejStatus"=>   $data->SMSRejStatus,
				"SmsrejSendBy"=>   $data->SMSRejSendBy,
				"AdminFee"=>   $data->AdminFee,
				"CeilingRate"=>   $data->CeilingRate,
				"VarianceRate"=>   $data->VarianceRate,
				"JobConfirm"=>   $JobConfirm,
				"CcrisOldCase"=>   $CCRIS_OldCase,
				"CcrisSumChk"=>   $ccris_Sum_chk,
				"CcrisDetChk"=>   $ccris_Det_chk,
				"SectionTy"=>   $data->SectionTy,
				"DspCif"=>   $DSP_Cif,
				"DspBlacklist"=>   $DSP_Blacklist,
				"DspAafacility"=>   $DSP_AAFacility,
				"DspIslamicFile"=>   $DSP_IslamicFile,
				"DspAccountCreation"=>   $DSP_AccountCreation,
				"DspStagPayment"=>   $DSP_StagPayment,
				"DspDisb"=>   $DSP_Disb,
				"DspDisbBulk"=>   $DSP_DisbBulk,
				"MiCobcifNo"=>   $data->MiCOBCifNo,
				"DspCifSp"=>   $DSP_CifSp,
				"DspBlacklistSp"=>   $DSP_BlacklistSp,
				"MiCobcifNoSp"=>   $data->MiCOBCifNoSp,
				"DspDisbEc"=>   $DSP_DisbEC,
				"JournalSeq"=>   $data->JournalSeq,
				"BrchProc"=>   $data->BrchProc,
				"MiCobflag"=>   $MiCOBFlag,
				"MiCobname"=>   $data->MiCOBName,
				"MiCobidty"=>   $data->MiCOBIDTy,
				"MiCobidno"=>   $data->MiCOBIDNo,
				"MiCobmntFlag"=>   $MiCOBMntFlag,
				"MiCobmntUsrId"=>   $data->MiCOBMntUsrID,
				"MiCobmntDt"=>  date('Y-m-d H:i:s', strtotime($data->MiCOBMntDt)),
				"InsCode"=>   $data->InsCode,
				"AkadScript"=>   $data->AkadScript,
				"CcrisAppStatus"=>   $data->CCRIS_AppStatus,
				"CertNo1"=>   $data->CertNo1,
				"CertNo2"=>   $data->CertNo2,
				"CertNo3"=>   $data->CertNo3,
				"Ccris_Chk"=>   $CCRIS_Chk,
				//"CodeARBId"=>   $data->	
				//"CodeAdminFeeId"=>   $data->	
				//"CodeAkadScriptId"=>   $data->	
				//"CodeBeneBankId"=>   $data->	
				//"CodeBranchId"=>   $data->	
				//"CodeCeilingRateId"=>   $data->	
				//"CodeISaveId"=>   $data->	
				//"CodeInsId"=>   $data->	
				//"CodeLoanPackageId"=>   $data->	
				//"CodeModeOfPaymentMOPId"=>   $data->	
				//"CodeModeOfPaymentPMId"=>   $data->	
				//"CodeVarianceRateId"=>   $data->	
				//"SubTy"=>   $data->	
				//"ApplRefNoTabTempId"=>   $data->AcID	    	
	
		    ]);
		}


    	$date_end = date("Y-m-d H:i:s");


		$table_name = 'ApplRefNoTabs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('applrefnotab')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('ApplRefNoTabs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status,
				    	'created_at' => $date_start,
      					'updated_at' => $date_end

				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);

    }

    //From Here
    public function Dsp_CifMastDetails(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DSP_CIFMastDetail::
    			get();
    	

		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('Dsp_CifMastDetails')
		    ->insert([
		    	

					"ApplId"	=>   $data->DSP_CIFMastDetail_to_ApplRefNoTabTemps->ApplId ?? NULL,
					"ApplRefNo"	 => $data->ApplRefNo,
					"ScenarioNo"	=>   $data->ScenarioNo,
					"ActionCode"	=>   $data->ActionCode,
					"TransactionCode"	=>   $data->TransactionCode,
					"Hdrnum"	=>   $data->HDRNUM,
					"ClientDt"	=>   $data->ClientDt,
					"ClientTm"	=>   $data->ClientTm,
					"UserId"	=>   $data->UserID,
					"UserGroup"	=>   $data->UserGroup,
					"Branch"	=>   $data->Branch,
					"Status"	=>   $data->Status,
					"ErrorCode"	=>   $data->ErrorCode,
					"ErrorReason"	=>   $data->ErrorReason,
					"MiCobcifNo"	=>   $data->MiCOBCifNo,
					"CustIdno"	=>   $data->CustIDNo,
					"Individual"	=>   $data->Individual,
					"ShortName"	=>   $data->ShortName,
					"Vip"	=>   $data->VIP,
					"Insider"	=>   $data->Insider,
					"InquiryCode"	=>   $data->InquiryCode,
					"MotherMaidenName"	=>   $data->MotherMaidenName,
					"DateOfBirth"	=>   $data->DateOfBirth,
					"Country"	=>   $data->Country,
					"Nationality"	=>   $data->Nationality,
					"Bumi"	=>   $data->Bumi,
					"Residence"	=>   $data->Residence,
					"Gender"	=>   $data->Gender,
					"Race"	=>   $data->Race,
					"ReligionCode"	=>   $data->ReligionCode,
					"LanguageIdentifier"	=>   $data->LanguageIdentifier,
					"MaritalStatus"	=>   $data->MaritalStatus,
					"MaritalStatusDate"	=>   $data->MaritalStatusDate,
					"Occupation"	=>   $data->Occupation,
					"CustomerStatus"	=>   $data->CustomerStatus,
					"CustomerStatusDate"	=>   $data->CustomerStatusDate,
					"DeceasedStatus"	=>   $data->DeceasedStatus,
					"DeceaseDate"	=>   $data->DeceaseDate,
					"CostCenter"	=>   $data->CostCenter,
					"HoldMailCode"	=>   $data->HoldMailCode,
					"PromotionMailCode"	=>   $data->PromotionMailCode,
					"CombinedCycle"	=>   $data->CombinedCycle,
					"AddressSeq"	=>   $data->AddressSeq,
					"StartRelationship"	=>   $data->StartRelationship,
					"ReviewDate"	=>   $data->ReviewDate,
					"CreationDate"	=>   $data->CreationDate,
					"Retention"	=>   $data->Retention,
					"SpecialInfoCode1"	=>   $data->SpecialInfoCode1,
					"SpecialInfoCode2"	=>   $data->SpecialInfoCode2,
					"SpecialInfoCode3"	=>   $data->SpecialInfoCode3,
					"SpecialInfoCode4"	=>   $data->SpecialInfoCode4,
					"SpecialInfoCode5"	=>   $data->SpecialInfoCode5,
					"SpecialInfoCode6"	=>   $data->SpecialInfoCode6,
					"SpecialInfoCode7"	=>   $data->SpecialInfoCode7,
					"SpecialInfoCode8"	=>   $data->SpecialInfoCode8,
					"SpecialInfoCode1Flag"	=>   $data->SpecialInfoCode1Flag,
					"SpecialInfoCode2Flag"	=>   $data->SpecialInfoCode2Flag,
					"SpecialInfoCode3Flag"	=>   $data->SpecialInfoCode3Flag,
					"SpecialInfoCode4Flag"	=>   $data->SpecialInfoCode4Flag,
					"SpecialInfoCode5Flag"	=>   $data->SpecialInfoCode5Flag,
					"SpecialInfoCode6Flag"	=>   $data->SpecialInfoCode6Flag,
					"SpecialInfoCode7Flag"	=>   $data->SpecialInfoCode7Flag,
					"SpecialInfoCode8Flag"	=>   $data->SpecialInfoCode8Flag,
					"CustomerInfoCode1"	=>   $data->CustomerInfoCode1,
					"CustomerInfoCode2"	=>   $data->CustomerInfoCode2,
					"CustomerInfoCode3"	=>   $data->CustomerInfoCode3,
					"CustomerInfoCode4"	=>   $data->CustomerInfoCode4,
					"CustomerInfoCode5"	=>   $data->CustomerInfoCode5,
					"CustomerInfoCode6"	=>   $data->CustomerInfoCode6,
					"CustomerInfoCode7"	=>   $data->CustomerInfoCode7,
					"CustomerInfoCode8"	=>   $data->CustomerInfoCode8,
					"CustomerInfCode1Flag"	=>   $data->CustomerInfCode1Flag,
					"CustomerInfCode2Flag"	=>   $data->CustomerInfCode2Flag,
					"CustomerInfCode3Flag"	=>   $data->CustomerInfCode3Flag,
					"CustomerInfCode4Flag"	=>   $data->CustomerInfCode4Flag,
					"CustomerInfCode5Flag"	=>   $data->CustomerInfCode5Flag,
					"CustomerInfCode6Flag"	=>   $data->CustomerInfCode6Flag,
					"CustomerInfCode7Flag"	=>   $data->CustomerInfCode7Flag,
					"CustomerInfCode8Flag"	=>   $data->CustomerInfCode8Flag,
					"Consitution"	=>   $data->Consitution,
					"Placeofbirth"	=>   $data->Placeofbirth,
					"CustomerSegmentationCode"	=>   $data->CustomerSegmentationCode,
					"NumberofEmployees"	=>   $data->NumberofEmployees,
					"HighRiskFlag"	=>   $data->HighRiskFlag,
					"HighRiskCode"	=>   $data->HighRiskCode,
					"StaffFlag"	=>   $data->StaffFlag,
					"StaffIdnumber"	=>   $data->StaffIDNumber,
					"CustomerAge"	=>   $data->CustomerAge,
					"Durationofstayatpresentaddress"	=>   $data->Durationofstayatpresentaddress,
					"HouseOwnershipCode"	=>   $data->HouseOwnershipCode,
					"TypeofDwelling"	=>   $data->TypeofDwelling,
					"Numberofdependents"	=>   $data->Numberofdependents,
					"BusinessPremisesOwnershipcode"	=>   $data->BusinessPremisesOwnershipcode,
					"TypeofBusinessPremises"	=>   $data->TypeofBusinessPremises,
					"BusinessOperatingHours"	=>   $data->BusinessOperatingHours,
					"Idnumber"	=>   $data->IDNumber,
					"Idtype"	=>   $data->IDType,
					"Salutation"	=>   $data->Salutation,
					"AddressLine1"	=>   $data->AddressLine1,
					"AddressLine2"	=>   $data->AddressLine2,
					"AddressLine3"	=>   $data->AddressLine3,
					"AddressLine4"	=>   $data->AddressLine4,
					"City"	=>   $data->City,
					"PostalCode"	=>   $data->PostalCode,
					"HousePhone"	=>   $data->HousePhone,
					"MobilePhone"	=>   $data->MobilePhone,
					"EmailAddress"	=>   $data->EmailAddress,
					"Dateofbirth2"	=>   $data->DATEOFBIRTH2,
					"Startoperationhours"	=>   $data->STARTOPERATIONHOURS,
					"Endoperationhours"	=>   $data->ENDOPERATIONHOURS,
					"BranchNumber"	=>   $data->BranchNumber,
					"OfficePhone"	=>   $data->OfficePhone,
					"ContactName"	=>   $data->ContactName,
					"Fax"	=>   $data->Fax,
					"ContactName1"	=>   $data->ContactName1,
					"ContactName2"	=>   $data->ContactName2,
					"CreationDate2"	=>   $data->CreationDate2,
					"Year"	=>   $data->Year,
					"AnnualTurnover"	=>   $data->AnnualTurnover,
					"Sector"	=>   $data->Sector,
					"NoofFullTimeEmp"	=>   $data->NoofFullTimeEmp,
					"TypeofClient"	=>   $data->TypeofClient,
					"Customername1"	=>   $data->Customername1,
					"DataSend"	=>   $data->DataSend,
					"DataReturn"	=>   $data->DataReturn,
					'DateSend'	=>   $data->DateSend,
					"DateReceive"	=>   $data->DateReceive
	
		    ]);
		}



		$table_name = 'Dsp_CifMastDetails';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_CIFMastDetail')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Dsp_CifMastDetails')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function Dsp_CifMastDetail_1(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DSP_CIFMastDetail_1:://take(300)->
    			//where('ApplRefNo', 'WAPB0000089672')->
    			get();
    	

		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('Dsp_CifMastDetail_1')
		    ->insert([
		    		
		    		//"No"	=>   $data->No,
					"ApplId"	=> $data->DSP_CIFMastDetail_1_to_ApplRefNoTabTemps->ApplId ?? Null,
					"ApplRefNo"	=>   $data->ApplRefNo,
					"ScenarioNo"	=>   $data->ScenarioNo,
					"ActionCode"	=>   $data->ActionCode,
					"TransactionCode"	=>   $data->TransactionCode,
					"Hdrnum"	=>   $data->HDRNUM,
					"ClientDt"	=>   $data->ClientDt,
					"ClientTm"	=>   $data->ClientTm,
					"UserId"	=>   $data->UserID,
					"UserGroup"	=>   $data->UserGroup,
					"Branch"	=>   $data->Branch,
					"Status"	=>   $data->Status,
					"ErrorCode"	=>   $data->ErrorCode,
					"ErrorReason"	=>   $data->ErrorReason,
					"MiCobcifNo"	=>   $data->MiCOBCifNo,
					"CustIdno"	=>   $data->CustIDNo,
					"Individual"	=>   $data->Individual,
					"ShortName"	=>   $data->ShortName,
					"Vip"	=>   $data->VIP,
					"Insider"	=>   $data->Insider,
					"InquiryCode"	=>   $data->InquiryCode,
					"MotherMaidenName"	=>   $data->MotherMaidenName,
					"DateOfBirth"	=>   $data->DateOfBirth,
					"Country"	=>   $data->Country,
					"Nationality"	=>   $data->Nationality,
					"Bumi"	=>   $data->Bumi,
					"Residence"	=>   $data->Residence,
					"Gender"	=>   $data->Gender,
					"Race"	=>   $data->Race,
					"ReligionCode"	=>   $data->ReligionCode,
					"LanguageIdentifier"	=>   $data->LanguageIdentifier,
					"MaritalStatus"	=>   $data->MaritalStatus,
					"MaritalStatusDate"	=>   $data->MaritalStatusDate,
					"Occupation"	=>   $data->Occupation,
					"CustomerStatus"	=>   $data->CustomerStatus,
					"CustomerStatusDate"	=>   $data->CustomerStatusDate,
					"DeceasedStatus"	=>   $data->DeceasedStatus,
					"DeceaseDate"	=>   $data->DeceaseDate,
					"CostCenter"	=>   $data->CostCenter,
					"HoldMailCode"	=>   $data->HoldMailCode,
					"PromotionMailCode"	=>   $data->PromotionMailCode,
					"CombinedCycle"	=>   $data->CombinedCycle,
					"AddressSeq"	=>   $data->AddressSeq,
					"StartRelationship"	=>   $data->StartRelationship,
					"ReviewDate"	=>   $data->ReviewDate,
					"CreationDate"	=>   $data->CreationDate,
					"Retention"	=>   $data->Retention,
					"SpecialInfoCode1"	=>   $data->SpecialInfoCode1,
					"SpecialInfoCode2"	=>   $data->SpecialInfoCode2,
					"SpecialInfoCode3"	=>   $data->SpecialInfoCode3,
					"SpecialInfoCode4"	=>   $data->SpecialInfoCode4,
					"SpecialInfoCode5"	=>   $data->SpecialInfoCode5,
					"SpecialInfoCode6"	=>   $data->SpecialInfoCode6,
					"SpecialInfoCode7"	=>   $data->SpecialInfoCode7,
					"SpecialInfoCode8"	=>   $data->SpecialInfoCode8,
					"SpecialInfoCode1Flag"	=>   $data->SpecialInfoCode1Flag,
					"SpecialInfoCode2Flag"	=>   $data->SpecialInfoCode2Flag,
					"SpecialInfoCode3Flag"	=>   $data->SpecialInfoCode3Flag,
					"SpecialInfoCode4Flag"	=>   $data->SpecialInfoCode4Flag,
					"SpecialInfoCode5Flag"	=>   $data->SpecialInfoCode5Flag,
					"SpecialInfoCode6Flag"	=>   $data->SpecialInfoCode6Flag,
					"SpecialInfoCode7Flag"	=>   $data->SpecialInfoCode7Flag,
					"SpecialInfoCode8Flag"	=>   $data->SpecialInfoCode8Flag,
					"CustomerInfoCode1"	=>   $data->CustomerInfoCode1,
					"CustomerInfoCode2"	=>   $data->CustomerInfoCode2,
					"CustomerInfoCode3"	=>   $data->CustomerInfoCode3,
					"CustomerInfoCode4"	=>   $data->CustomerInfoCode4,
					"CustomerInfoCode5"	=>   $data->CustomerInfoCode5,
					"CustomerInfoCode6"	=>   $data->CustomerInfoCode6,
					"CustomerInfoCode7"	=>   $data->CustomerInfoCode7,
					"CustomerInfoCode8"	=>   $data->CustomerInfoCode8,
					"CustomerInfCode1Flag"	=>   $data->CustomerInfCode1Flag,
					"CustomerInfCode2Flag"	=>   $data->CustomerInfCode2Flag,
					"CustomerInfCode3Flag"	=>   $data->CustomerInfCode3Flag,
					"CustomerInfCode4Flag"	=>   $data->CustomerInfCode4Flag,
					"CustomerInfCode5Flag"	=>   $data->CustomerInfCode5Flag,
					"CustomerInfCode6Flag"	=>   $data->CustomerInfCode6Flag,
					"CustomerInfCode7Flag"	=>   $data->CustomerInfCode7Flag,
					"CustomerInfCode8Flag"	=>   $data->CustomerInfCode8Flag,
					"Consitution"	=>   $data->Consitution,
					"Placeofbirth"	=>   $data->Placeofbirth,
					"CustomerSegmentationCode"	=>   $data->CustomerSegmentationCode,
					"NumberofEmployees"	=>   $data->NumberofEmployees,
					"HighRiskFlag"	=>   $data->HighRiskFlag,
					"HighRiskCode"	=>   $data->HighRiskCode,
					"StaffFlag"	=>   $data->StaffFlag,
					"StaffIdnumber"	=>   $data->StaffIDNumber,
					"CustomerAge"	=>   $data->CustomerAge,
					"Durationofstayatpresentaddress"	=>   $data->Durationofstayatpresentaddress,
					"HouseOwnershipCode"	=>   $data->HouseOwnershipCode,
					"TypeofDwelling"	=>   $data->TypeofDwelling,
					"Numberofdependents"	=>   $data->Numberofdependents,
					"BusinessPremisesOwnershipcode"	=>   $data->BusinessPremisesOwnershipcode,
					"TypeofBusinessPremises"	=>   $data->TypeofBusinessPremises,
					"BusinessOperatingHours"	=>   $data->BusinessOperatingHours,
					"Idnumber"	=>   $data->IDNumber,
					"Idtype"	=>   $data->IDType,
					"Salutation"	=>   $data->Salutation,
					"AddressLine1"	=>   $data->AddressLine1,
					"AddressLine2"	=>   $data->AddressLine2,
					"AddressLine3"	=>   $data->AddressLine3,
					"AddressLine4"	=>   $data->AddressLine4,
					"City"	=>   $data->City,
					"PostalCode"	=>   $data->PostalCode,
					"HousePhone"	=>   $data->HousePhone,
					"MobilePhone"	=>   $data->MobilePhone,
					"EmailAddress"	=>   $data->EmailAddress,
					"Dateofbirth2"	=>   $data->DATEOFBIRTH2,
					"Startoperationhours"	=>   $data->STARTOPERATIONHOURS,
					"Endoperationhours"	=>   $data->ENDOPERATIONHOURS,
					"BranchNumber"	=>   $data->BranchNumber,
					"OfficePhone"	=>   $data->OfficePhone,
					"ContactName"	=>   $data->ContactName,
					"Fax"	=>   $data->Fax,
					"ContactName1"	=>   $data->ContactName1,
					"ContactName2"	=>   $data->ContactName2,
					"CreationDate2"	=>   $data->CreationDate2,
					"Year"	=>   $data->Year,
					"AnnualTurnover"	=>   $data->AnnualTurnover,
					"Sector"	=>   $data->Sector,
					"NoofFullTimeEmp"	=>   $data->NoofFullTimeEmp,
					"TypeofClient"	=>   $data->TypeofClient,
					"Customername1"	=>   $data->Customername1,
					"DataSend"	=>   $data->DataSend,
					"DataReturn"	=>   $data->DataReturn,
					"DateSend"	=>   $data->DateSend,
					"DateReceive"	=>   $data->DateReceive
				
	
		    ]);
		}


		$table_name = 'Dsp_CifMastDetail_1';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_CIFMastDetail_1')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Dsp_CifMastDetail_1')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }




    public function DSP_EmpDetails(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DSP_EmpDetail::
    			 get();
    	
		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('DSP_EmpDetails')
		    ->insert([
		    	
				"ApplId"	=> $data->DSP_EmpDetail_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"ApplRefNo"	=>   $data->ApplRefNo,
				"ScenarioNo"	=>   $data->ScenarioNo,
				"ActionCode"	=>   $data->ActionCode,
				"TransactionCode"	=>   $data->TransactionCode,
				"Hdrnum"	=>   $data->HDRNUM,
				"ClientDt"	=>   $data->ClientDt,
				"ClientTm"	=>   $data->ClientTm,
				"UserId"	=>   $data->UserID,
				"UserGroup"	=>   $data->UserGroup,
				"Branch"	=>   $data->Branch,
				"Status"	=>   $data->Status,
				"ErrorCode"	=>   $data->ErrorCode,
				"ErrorReason"	=>   $data->ErrorReason,
				"MiCobcifNo"	=>   $data->MiCOBCifNo,
				"EmpHisSeq"	=>   $data->EmpHisSeq,
				"EmpStat"	=>   $data->EmpStat,
				"EmpStDt"	=>   $data->EmpStDt,
				"OccuCode"	=>   $data->OccuCode,
				"JobDsgnCode"	=>   $data->JobDsgnCode,
				"TyEmp"	=>   $data->TyEmp,
				"EmpIndCode"	=>   $data->EmpIndCode,
				"EmployerCode"	=>   $data->EmployerCode,
				"EmployerRate"	=>   $data->EmployerRate,
				"EmployerName"	=>   $data->EmployerName,
				"IncomeBracketCode"	=>   $data->IncomeBracketCode,
				"Income"	=>   $data->Income,
				"OthIncome"	=>   $data->OthIncome,
				"Expenditure"	=>   $data->Expenditure,
				"EmployeeId"	=>   $data->EmployeeID,
				"Remark1"	=>   $data->Remark1,
				"Remark2"	=>   $data->Remark2,
				"Remark3"	=>   $data->Remark3,
				"EmploymentEndDt1"	=>   $data->EmploymentEndDt1,
				"EmploymentEndDt2"	=>   $data->EmploymentEndDt2,
				"EmployerAdd1"	=>   $data->EmployerAdd1,
				"EmployerAdd2"	=>   $data->EmployerAdd2,
				"EmployerAdd3"	=>   $data->EmployerAdd3,
				"EmployerAdd4"	=>   $data->EmployerAdd4,
				"EmployerAdd5"	=>   $data->EmployerAdd5,
				"EmployerCity"	=>   $data->EmployerCity,
				"ForeignAdd"	=>   $data->ForeignAdd,
				"City"	=>   $data->City,
				"State"	=>   $data->State,
				"Postalcode"	=>   $data->Postalcode,
				"Country"	=>   $data->Country,
				"Dept"	=>   $data->Dept,
				"BusinessType"	=>   $data->BusinessType,
				"EmpSec"	=>   $data->EmpSec,
				"CustPayClass"	=>   $data->CustPayClass,
				"OffNo"	=>   $data->OffNo,
				"FaxNo"	=>   $data->FaxNo,
				"HpNo"	=>   $data->HpNo,
				"HrphoneNo"	=>   $data->HRPhoneNo,
				"Remark4"	=>   $data->Remark4,
				"MonthlyIncome"	=>   $data->MonthlyIncome,
				"MonthlyOtherIncome"	=>   $data->MonthlyOtherIncome,
				"MonthlyExpenditure"	=>   $data->MonthlyExpenditure,
				"DateSend"	=>   $data->DateSend,
				"DateReceive"	=>   $data->DateReceive
		    	
	
		    ]);
		}



		$table_name = 'DSP_EmpDetails';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_EmpDetail')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('DSP_EmpDetails')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function DSP_EmpDetail_1(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DSP_EmpDetail_1::
    			 get();
    	
		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('DSP_EmpDetail_1')
		    ->insert([
		    	
				"ApplId"	=> $data->DSP_EmpDetail_1_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"ApplRefNo"	=>   $data->ApplRefNo,
				"ScenarioNo"	=>   $data->ScenarioNo,
				"ActionCode"	=>   $data->ActionCode,
				"TransactionCode"	=>   $data->TransactionCode,
				"Hdrnum"	=>   $data->HDRNUM,
				"ClientDt"	=>   $data->ClientDt,
				"ClientTm"	=>   $data->ClientTm,
				"UserId"	=>   $data->UserID,
				"UserGroup"	=>   $data->UserGroup,
				"Branch"	=>   $data->Branch,
				"Status"	=>   $data->Status,
				"ErrorCode"	=>   $data->ErrorCode,
				"ErrorReason"	=>   $data->ErrorReason,
				"MiCobcifNo"	=>   $data->MiCOBCifNo,
				"EmpHisSeq"	=>   $data->EmpHisSeq,
				"EmpStat"	=>   $data->EmpStat,
				"EmpStDt"	=>   $data->EmpStDt,
				"OccuCode"	=>   $data->OccuCode,
				"JobDsgnCode"	=>   $data->JobDsgnCode,
				"TyEmp"	=>   $data->TyEmp,
				"EmpIndCode"	=>   $data->EmpIndCode,
				"EmployerCode"	=>   $data->EmployerCode,
				"EmployerRate"	=>   $data->EmployerRate,
				"EmployerName"	=>   $data->EmployerName,
				"IncomeBracketCode"	=>   $data->IncomeBracketCode,
				"Income"	=>   $data->Income,
				"OthIncome"	=>   $data->OthIncome,
				"Expenditure"	=>   $data->Expenditure,
				"EmployeeId"	=>   $data->EmployeeID,
				"Remark1"	=>   $data->Remark1,
				"Remark2"	=>   $data->Remark2,
				"Remark3"	=>   $data->Remark3,
				"EmploymentEndDt1"	=>   $data->EmploymentEndDt1,
				"EmploymentEndDt2"	=>   $data->EmploymentEndDt2,
				"EmployerAdd1"	=>   $data->EmployerAdd1,
				"EmployerAdd2"	=>   $data->EmployerAdd2,
				"EmployerAdd3"	=>   $data->EmployerAdd3,
				"EmployerAdd4"	=>   $data->EmployerAdd4,
				"EmployerAdd5"	=>   $data->EmployerAdd5,
				"EmployerCity"	=>   $data->EmployerCity,
				"ForeignAdd"	=>   $data->ForeignAdd,
				"City"	=>   $data->City,
				"State"	=>   $data->State,
				"Postalcode"	=>   $data->Postalcode,
				"Country"	=>   $data->Country,
				"Dept"	=>   $data->Dept,
				"BusinessType"	=>   $data->BusinessType,
				"EmpSec"	=>   $data->EmpSec,
				"CustPayClass"	=>   $data->CustPayClass,
				"OffNo"	=>   $data->OffNo,
				"FaxNo"	=>   $data->FaxNo,
				"HpNo"	=>   $data->HpNo,
				"HrphoneNo"	=>   $data->HRPhoneNo,
				"Remark4"	=>   $data->Remark4,
				"MonthlyIncome"	=>   $data->MonthlyIncome,
				"MonthlyOtherIncome"	=>   $data->MonthlyOtherIncome,
				"MonthlyExpenditure"	=>   $data->MonthlyExpenditure,
				"DateSend"	=>   $data->DateSend,
				"DateReceive"	=>   $data->DateReceive
		    	
	
		    ]);
		}



		$table_name = 'DSP_EmpDetail_1';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_EmpDetail_1')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('DSP_EmpDetail_1')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function Dsp_GetCifNos(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DSP_GetCifNo::
    			get();
    	

		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('Dsp_GetCifNos')
		    ->insert([
		    	
				"ApplId"	=> $data->DSP_GetCifNo_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"ApplRefNo"	=>   $data->ApplRefNo,
				"ScenarioNo"	=>   $data->ScenarioNo,
				"ActionCode"	=>   $data->ActionCode,
				"TransactionCode"	=>   $data->TransactionCode,
				"Hdrnum"	=>   $data->HDRNUM,
				"ClientDt"	=>   $data->ClientDt,
				"ClientTm"	=>   $data->ClientTm,
				"UserId"	=>   $data->UserID,
				"UserGroup"	=>   $data->UserGroup,
				"Branch"	=>   $data->Branch,
				"Status"	=>   $data->Status,
				"ErrorCode"	=>   $data->ErrorCode,
				"ErrorReason"	=>   $data->ErrorReason,
				"MiCobcifNo"	=>   $data->MiCOBCifNo,
				"CustIdno"	=>   $data->CustIDNo,
				"CustIdty"	=>   $data->CustIDTy,
				"Idno"	=>   $data->IDNo,
				"IdtypeCode"	=>   $data->IDTypeCode,
				"AliasFlag"	=>   $data->AliasFlag,
				"CustomerName1"	=>   $data->CustomerName1,
				"FormattedIdNo"	=>   $data->FormattedIdNo,
				"Country"	=>   $data->Country,
				"BranchNo"	=>   $data->BranchNo,
				"CifNo"	=>   $data->CifNo,
				"CustomerName2"	=>   $data->CustomerName2,
				"ShortName"	=>   $data->ShortName,
				"Individual"	=>   $data->Individual,
				"ElectronicAddDesc"	=>   $data->ElectronicAddDesc,
				"City"	=>   $data->City,
				"ZipCode"	=>   $data->ZipCode,
				"CollateralName"	=>   $data->CollateralName,
				"Address1"	=>   $data->Address1,
				"Address2"	=>   $data->Address2,
				"StaffIdno"	=>   $data->StaffIDNo,
				"Idmnuusr"	=>   $data->IDMNUUSR,
				"DataSend"	=>   $data->DataSend,
				"DataReturn"	=>   $data->DataReturn,
				"DateSend"	=>   $data->DateSend,
				"DateReceive"	=>   $data->DateReceive
		    	
	
		    ]);
		}



		$table_name = 'Dsp_GetCifNos';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_GetCifNo')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Dsp_GetCifNos')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function DSP_GetCifNo_1(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DSP_GetCifNo_1::
    			get();
    	

		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('DSP_GetCifNo_1')
		    ->insert([
		    	
				"ApplId"	=> $data->DSP_GetCifNo_1_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"ApplRefNo"	=>   $data->ApplRefNo,
				"ScenarioNo"	=>   $data->ScenarioNo,
				"ActionCode"	=>   $data->ActionCode,
				"TransactionCode"	=>   $data->TransactionCode,
				"Hdrnum"	=>   $data->HDRNUM,
				"ClientDt"	=>   $data->ClientDt,
				"ClientTm"	=>   $data->ClientTm,
				"UserId"	=>   $data->UserID,
				"UserGroup"	=>   $data->UserGroup,
				"Branch"	=>   $data->Branch,
				"Status"	=>   $data->Status,
				"ErrorCode"	=>   $data->ErrorCode,
				"ErrorReason"	=>   $data->ErrorReason,
				"MiCobcifNo"	=>   $data->MiCOBCifNo,
				"CustIdno"	=>   $data->CustIDNo,
				"CustIdty"	=>   $data->CustIDTy,
				"Idno"	=>   $data->IDNo,
				"IdtypeCode"	=>   $data->IDTypeCode,
				"AliasFlag"	=>   $data->AliasFlag,
				"CustomerName1"	=>   $data->CustomerName1,
				"FormattedIdNo"	=>   $data->FormattedIdNo,
				"Country"	=>   $data->Country,
				"BranchNo"	=>   $data->BranchNo,
				"CifNo"	=>   $data->CifNo,
				"CustomerName2"	=>   $data->CustomerName2,
				"ShortName"	=>   $data->ShortName,
				"Individual"	=>   $data->Individual,
				"ElectronicAddDesc"	=>   $data->ElectronicAddDesc,
				"City"	=>   $data->City,
				"ZipCode"	=>   $data->ZipCode,
				"CollateralName"	=>   $data->CollateralName,
				"Address1"	=>   $data->Address1,
				"Address2"	=>   $data->Address2,
				"StaffIdno"	=>   $data->StaffIDNo,
				"Idmnuusr"	=>   $data->IDMNUUSR,
				"DataSend"	=>   $data->DataSend,
				"DataReturn"	=>   $data->DataReturn,
				"DateSend"	=>   $data->DateSend,
				"DateReceive"	=>   $data->DateReceive
		    	
	
		    ]);
		}



		$table_name = 'DSP_GetCifNo_1';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_GetCifNo_1')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('DSP_GetCifNo_1')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function Guarantors(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	
    	$datas = guarantor::
    			get();
    	

		foreach($datas as $data){

			
			DB::connection('sqlsrv2')
			->table('Guarantors')
		    ->insert([		    	
				
		    	"ApplId"	=>   $data->guarantor_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"CustAdd"	=>   $data->CustAddNo,

				"CustRelation"	=>   $data->CustRelation,
				"CustIdTy"	=>   $data->CustNewIDTy,
				"CustIdNo"	=>   $data->CustNewIDNo,
				"SendStatement"	=>   $data->SendStatement,
				"CustRemark"	=>   $data->CustRemark
					
		    ]);
		}


		$table_name = 'Guarantors';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('guarantor')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Guarantors')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function ApplCheckLists(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	
    	$datas = applchecklist:://take(300)->
    		
    			get();
    	

		foreach($datas as $data){

			
			DB::connection('sqlsrv2')
			->table('ApplCheckLists')
		    ->insert([		    	
						    	
				"CreatedBy"	 => "",
				"CreatedDate" => "",
				"Act"	=> "1",
				"ApplId" =>	$data->applchecklist_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"ClistCode"	=>   $data->CListCode,
				"ClistType"	=>   $data->CListType,
				"TypeDesc"	=> "",
					
		    ]);
		}


		$table_name = 'ApplCheckLists';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('applchecklist')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('ApplCheckLists')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function ApplApproves(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	
    	$datas = applapprove::
    		//where('ApplRefNo', 'WAP00000223502')->
    			get();
    	

		foreach($datas as $data){

			if($data->DocCOM == "Y"){
				$DocCOM = true;
			}
			else{
				$DocCOM = false;
			}


			$UploadDt = $data->UploadDt;
			$string_date = substr($UploadDt, 0, 2);
			$string_month = substr($UploadDt, 2, 2);
			$string_year = substr($UploadDt, 4, 4);
			//$Month_TransDtTm = date('m', strtotime($string_month));


			//dd($string_month."-".$string_date."-".$string_year);


			$UploadDt_get = $string_year.'-'.$string_month.'-'.$string_date;
			$UploadDt_convert = date('Y-m-d H:i:s', strtotime($UploadDt_get));


			$OfferDt_convert = date('Y-m-d H:i:s', strtotime($data->OfferDt));

			$AcceptDt_convert = date('Y-m-d H:i:s', strtotime($data->AcceptDt));


			
			DB::connection('sqlsrv2')
			->table('ApplApproves')
		    ->insert([		    	
						    	
				"ApplId"	=>$data->applapprove_to_ApplRefNoTabTemps->ApplId ?? NULL,		
				"AppvAutCode"	=>$data->AppvAutCode,
				"AppvAmt"	=>$data->AppvAmt,
				"AppvTenure"	=>$data->AppvTenure,
				"AppvRate"	=>$data->AppvRate,
				"AppvInst"	=>$data->AppvInst,
				"AppStatCode"	=>$data->AppStatCode,
				"DecisionBy"	=>$data->DecisionBy,
				"RejReasCode"	=>$data->RejReasCode,
				"OfferDt"	=>$OfferDt_convert,
				"AcceptDt"	=>$AcceptDt_convert,
				"AppvRemark"	=>$data->AppvRemark,
				"Mbsflag"	=>$data->MBSFlag,
				"UploadDt"	=>$UploadDt_convert,
				"DocCom"	=>$DocCOM,
				"AppvDeferment"	=>$data->AppvDeferment,
				"AppvAdInst"	=>$data->AppvAdInst,
				"Appvtpc"	=>$data->Appvtpc,
				"Appvshm"	=>$data->Appvshm,
				"Lmsflag"	=>$data->LMSFlag,
				"Csuflag"	=>$data->CSUFlag,
				"ApplRefNo2"	=>$data->ApplRefNo2,
				"AppvAmt2"	=>$data->AppvAmt2,
				"AppvRate2"	=>$data->AppvRate2,
				"AppvInst2"	=>$data->AppvInst2,
				"DecisionBy2"	=>$data->DecisionBy2,
				"DualSignId"	=>$data->DualSignID,
					
		    ]);
		}


		$table_name = 'ApplApproves';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('applapprove')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('ApplApproves')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function ISms_Incomp(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	
    	$datas = iSMS_InComp::take(300)->
    			get();
    	

		foreach($datas as $data){

			
			DB::connection('sqlsrv2')
			->table('ISms_Incomp')
		    ->insert([		    	
			

				"CreatedBy"	=> "",
				"CreatedDate"	=> "",
				"ApplId"	=> $data->iSMS_InComp_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"MsgSend" =>	$data->MsgSend
					
		    ]);
		}


		$table_name = 'ISms_Incomp';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('iSMS_InComp')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('ISms_Incomp')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    

    public function Cifs(Request $request)
    {


    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = cif::
    	//where('CustName', 'NORLIA BINTI MAHADI')->
    			get();
    	

		foreach($datas as $data){
			
			//$date = '1988-05-29 00:00:00.000';


			if($data->ResStatus == "Y"){
				$ResStatus = true;
			}
			else{
				$ResStatus = false;
			}

			if($data->CustWorkFinc == "Y"){
				$CustWorkFinc = true;
			}
			else{
				$CustWorkFinc = false;
			}

			if($data->WaveFlag == "Y"){
				$WaveFlag = true;
			}
			else{
				$WaveFlag = false;
			}

			if($data->CustPDPA == "Y"){
				$CustPdpa = true;
			}else{
				$CustPdpa = false;
			}


			$date_CustEmployDt = substr($data->CustEmployDt, 0, 2);
			$month_CustEmployDt = substr($data->CustEmployDt, 2, 2);
			$year_CustEmployDt = substr($data->CustEmployDt, 4, 4);

			if(!empty($data->CustEmployDt)){
				$CustEmployDt_x = $year_CustEmployDt.'-'.$month_CustEmployDt.'-'.$date_CustEmployDt;

				$CustEmployDt = date("Y-m-d", strtotime($CustEmployDt_x) );


			}else{
				$CustEmployDt_x = "";
				$CustEmployDt = date("Y-m-d", strtotime($CustEmployDt_x) );
			}


			$date_CustDob = substr($data->CustDOB, 0, 2);
			$month_CustDob = substr($data->CustDOB, 2, 2);
			$year_CustDob = substr($data->CustDOB, 0, 4);


			if(!empty($data->CustDob)){
				$CustDob = $year_CustDob.'-'.$month_CustDob.'-'.$date_CustDob;
			}else{
				$CustDob = "";
			}


			//$c = date("Y-m-d", strtotime($data->CustDOB) );

			//$CustEmployDt = date("Y-m-d", strtotime($data->CustDob) );

			//$Cdd = date('Y-m-d H:i:s', strtotime($data->CustEmployDt));

			//dd($data->CustDOB);

			//date('Y-m-d H:i:s', strtotime(null));

			
			
			DB::connection('sqlsrv2')
			->table('Cifs')
		    ->insert([		    					

				"ApplId"=>   $data->cif_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"CustName"=>   $data->CustName,
				"CustAbbreName"=>   $data->CustAbbreName,
				"CustTitle"=>   $data->CustTitle,
				"CustNewIdty"=>   $data->CustNewIDTy,
				"CustNewIdno"=>  $data->CustNewIDNo,
				"CustPrevIdty"=>   $data->CustPrevIDTy,
				"CustPrevIdno"=>   $data->CustPrevIDNo,
				"ResStatus"=>   $ResStatus,
				"CustBumi"=>   $data->CustBumi,
				"CustHp"=>   $data->CustHP,
				"CustFax"=>   $data->CustFax,
				"CustPagerCenter"=>   $data->CustPagerCenter,
				"CustPagerNo"=>   $data->CustPagerNo,
				"CustEmail"=>   $data->CustEmail,
				"CustNation"=>   $data->CustNation,
				"CustRace"=>   $data->CustRace,
				"CustGender"=>   $data->CustGender,
				"CustDob"=>   $data->CustDOB,
				"CustMarital"=>   $data->CustMarital,
				"CustReligion"=>   $data->CustReligion,
				"CustLanguage"=>   $data->CustLanguage,
				"CustDepend"=>   $data->CustDepend,
				"CustProffesion"=>   $data->CustProffesion,
				"CustEmployer"=>   $data->CustEmployer,
				"CustNob"=>   $data->CustNOB,
				"CustDesignation"=>   $data->CustDesignation,
				"CustEmployDt"=>   $CustEmployDt,
				"CustIncomeTy"=>   $data->CustIncomeTy,
				"CustIncomeRange"=>   $data->CustIncomeRange,
				"CustActualIncome"=>   $data->CustActualIncome,
				"CustIncomeDt"=>   $data->CustIncomeDt,
				"CustJobStat"=>   $data->CustJobStat,
				"AgbranchCode"=>   $data->AGBranchCode,
				"AgpayCentre"=>   $data->AGPayCentre,
				"AgdepartmentCode"=>   $data->AGDepartmentCode,
				"AgpersonnelNo"=>   $data->AGPersonnelNo,
				"Agind"=>   $data->AGInd,
				"CustEduLvl"=>   $data->CustEduLvl,
				"CustIncmTyp"=>   $data->CustIncmTyp,
				"CustWrkSec"=>   $data->CustWrkSec,
				"CustDeptNm"=>   $data->CustDeptNm,
				"CustFixedIncome"=>   $data->CustFixedIncome,
				"CustOtherIncome"=>   $data->CustOtherIncome,
				"CustWorkFinc"=>   $CustWorkFinc,
				"WaveFlag"=>   $WaveFlag,
				"Lmsflag"=>   $data->LMSFlag,
				"UploadDt"=>   $data->UploadDt,
				"CustPdpa"=>   $CustPdpa,
				"CustGstregNo"=>   $data->CustGSTRegNo,
				"CustEmpSec"=>   $data->CustEmpSec,
				"CustEmpTy"=>   $data->CustEmpTy,
				"CustOffP"=>  "",

				"CustHouseP"=>   ""	
					
		    ]);
		}


		$table_name = 'Cifs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('cif')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Cifs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }





    public function Guarantors_csv_test(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	$txt = "";
    	$table_name = 'Guarantors';
    	$fileName = 'Guarantors.csv';

    	
    	$tasks = guarantor::take(200)->
    			get();
    	
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Id', 'ApplId', 'CustAdd', 'CustRelation', 'CustIdTy', 'CustIdNo', 'SendStatement', 'CustRemark');


        $callback = function() use($tasks, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($tasks as $task) {
                $row['Id']  = "";
                $row['ApplId']    = $data->guarantor_to_applrefnotab->applrefnotab_to_ApplRefNoTabTemps->ApplId ?? NULL;
                $row['CustAdd']    = $data->CustAddNo;
                $row['CustRelation']  = $data->CustRelation;
                $row['CustIdTy']  = $data->CustNewIDTy;
                $row['CustIdNo']  = $data->CustNewIDNo;
                $row['SendStatement']  = $data->endStatement;
                $row['CustRemark']  = $data->CustRemark;

                fputcsv($file, array($row['Id'], $row['ApplId'], $row['CustAdd'], $row['CustRelation'], $row['CustIdTy'], $row['CustIdNo'], $row['SendStatement'], $row['CustRemark']));
            }

            fclose($file);
        };
        
        alert()->success($table_name,'Data Migration Successfully');

        return response()->stream($callback, 200, $headers);
		
    }


    public function SumCurrentApplStats(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = sumcurrentapplstat::
    			 get();
    	

		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('SumCurrentApplStats')
		    ->insert([
		    	
				"ApplId"	=>   $data->submissiontype_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"StatusDt"	=>   $data->StatusDt,
				"AppStatus"	=>   $data->AppStatus
					
		    ]);
		}


		$table_name = 'SumCurrentApplStats';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('sumcurrentapplstat')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('SumCurrentApplStats')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function UserTaskLists(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = usertasklist::
    			 get();
    	

		foreach($datas as $data){
			$string_date_StRouteDt = substr($data->StRouteDt, 8, 2);
			$string_month_StRouteDt = substr($data->StRouteDt, 5, 2);
			$string_year_StRouteDt = substr($data->StRouteDt, 0, 4);

			$StRouteDt = $string_year_StRouteDt.'-'.$string_month_StRouteDt.'-'.$string_date_StRouteDt;

			$string_hh_StRouteTm = substr($data->StRouteTm, 0, 2);
			$string_mm_StRouteTm = substr($data->StRouteTm, 4, 2);
			$string_ss_StRouteTm = substr($data->StRouteTm, 7, 2);

			$StStRouteTm = $string_hh_StRouteTm.'-'.$string_mm_StRouteTm.'-'.$string_ss_StRouteTm;

			//dd($StRouteDt.' '.$data->StRouteTm);

			DB::connection('sqlsrv2')
			->table('UserTaskLists')
		    ->insert([
		    	
				"ApplId" =>   $data->usertasklist_to_ApplRefNoTabTemps->ApplId ?? NULL,	
				"UserId"	=>   $data->UsrID,
				"StRouteDt"	=>   $StRouteDt.' '.$data->StRouteTm
					
		    ]);
		}


		$table_name = 'UserTaskLists';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('usertasklist')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('UserTaskLists')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    ///until Here




    public function DSP_CifDetailGST_1(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DSP_CifDetailGST_1::
    			get();
    	

		foreach($datas as $data){			
			DB::connection('sqlsrv2')
			->table('DSP_CifDetailGST_1')
		    ->insert([
		    	
				"ApplId" => $data->DSP_CifDetailGST_1_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"ApplRefNo"	=>$data->ApplRefNo,
				"ScenarioNo"	=>$data->ScenarioNo,
				"ActionCode"	=>$data->ActionCode,
				"TransactionCode"	=>$data->TransactionCode,
				"Hdrnum"	=>$data->HDRNUM,
				"ClientDt"	=>$data->ClientDt,
				"ClientTm"	=>$data->ClientTm,
				"UserId"	=>$data->UserID,
				"UserGroup"	=>$data->UserGroup,
				"Branch"	=>$data->Branch,
				"Status"	=>$data->Status,
				"ErrorCode"	=>$data->ErrorCode,
				"ErrorReason"	=>$data->ErrorReason,
				"MiCobcifNo"	=>$data->MiCOBCifNo,
				"CustIdno"	=>$data->CustIDNo,
				"Id1"	=>$data->ID1,
				"CustomerIdno2"	=>$data->CustomerIDNo2,
				"IdtypeCode2"	=>$data->IDTypeCode2,
				"IdissueCountry2"	=>$data->IDIssueCountry2,
				"Shortname"	=>$data->Shortname,
				"CustomerIdno1"	=>$data->CustomerIDNo1,
				"IdtypeCode1"	=>$data->IDTypeCode1,
				"IdissueCountry1"	=>$data->IDIssueCountry1,
				"CustomerIdnoNob2"	=>$data->CustomerIDNoNOB2,
				"IdtypeCodeNob2"	=>$data->IDTypeCodeNOB2,
				"IdissueCountryNob2"	=>$data->IDIssueCountryNOB2,
				"Branchnumber"	=>$data->Branchnumber,
				"Customernumber"	=>$data->Customernumber,
				"Salutation"	=>$data->Salutation,
				"Customername1"	=>$data->Customername1,
				"Customername2"	=>$data->Customername2,
				"Class1"	=>$data->Class1,
				"Dob"	=>$data->DOB,
				"Individual"	=>$data->Individual,
				"Countryofcitizenship"	=>$data->Countryofcitizenship,
				"RaceCountry"	=>$data->RaceCountry,
				"ResidenceBusOper"	=>$data->ResidenceBusOper,
				"Gender"	=>$data->Gender,
				"Marital"	=>$data->Marital,
				"Residentcode"	=>$data->Residentcode,
				"Businesstype"	=>$data->Businesstype,
				"Id2"	=>$data->ID2,
				"Addresssequence1"	=>$data->Addresssequence1,
				"Addresstype1"	=>$data->Addresstype1,
				"Addressformat1"	=>$data->Addressformat1,
				"Foreignaddress1"	=>$data->Foreignaddress1,
				"Addressline11"	=>$data->Addressline11,
				"Addressline12"	=>$data->Addressline12,
				"Addressline13"	=>$data->Addressline13,
				"Citystatezip1"	=>$data->Citystatezip1,
				"Postalcode1"	=>$data->Postalcode1,
				"State1"	=>$data->State1,
				"Country1"	=>$data->Country1,
				"Addressseq2"	=>$data->Addressseq2,
				"Addresstype2"	=>$data->Addresstype2,
				"Addressformat2"	=>$data->Addressformat2,
				"Foreignaddress2"	=>$data->Foreignaddress2,
				"Addressline21"	=>$data->Addressline21,
				"Addressline22"	=>$data->Addressline22,
				"Addressline23"	=>$data->Addressline23,
				"Citystatezip2"	=>$data->Citystatezip2,
				"Postalcode2"	=>$data->Postalcode2,
				"State2"	=>$data->State2,
				"Country2"	=>$data->Country2,
				"ContactSeq1"	=>$data->ContactSeq1,
				"ContactType1"	=>$data->ContactType1,
				"ContactDetail1"	=>$data->ContactDetail1,
				"ContactName1"	=>$data->ContactName1,
				"ContactSeq2"	=>$data->ContactSeq2,
				"ContactType2"	=>$data->ContactType2,
				"ContactDetail2"	=>$data->ContactDetail2,
				"ContactName2"	=>$data->ContactName2,
				"Costcenter"	=>$data->Costcenter,
				"CustomerSegmentationCode"	=>$data->CustomerSegmentationCode,
				"NumberofEmployees"	=>$data->NumberofEmployees,
				"HighRiskFlag"	=>$data->HighRiskFlag,
				"HighRiskCode"	=>$data->HighRiskCode,
				"StaffFlag"	=>$data->StaffFlag,
				"StaffIdnumber"	=>$data->StaffIDNumber,
				"Religioncode"	=>$data->Religioncode,
				"CustomerAge"	=>$data->CustomerAge,
				"Insidercode"	=>$data->Insidercode,
				"DurationStay"	=>$data->DurationStay,
				"HouseOwnershipCode"	=>$data->HouseOwnershipCode,
				"TypeofDwelling"	=>$data->TypeofDwelling,
				"Numberofdependents"	=>$data->Numberofdependents,
				"BusinessPremisesOwnershipCode"	=>$data->BusinessPremisesOwnershipCode,
				"TypeofBusinessPremises"	=>$data->TypeofBusinessPremises,
				"BusinessOperatingHours"	=>$data->BusinessOperatingHours,
				"Startoperationhours"	=>$data->STARTOPERATIONHOURS,
				"Endoperationhours"	=>$data->ENDOPERATIONHOURS,
				"BnmsectorCode"	=>$data->BNMSectorCode,
				"BnmstateCode"	=>$data->BNMStateCode,
				"BnmbumiCode"	=>$data->BNMBumiCode,
				"Vipcustomercode"	=>$data->VIPcustomercode,
				"BlacklistedFlag"	=>$data->BlacklistedFlag,
				"Sourcecode"	=>$data->Sourcecode,
				"Reasoncode"	=>$data->Reasoncode,
				"Listeddescription"	=>$data->Listeddescription,
				"GstregNo"	=>$data->GSTRegNo,
				"GstregFlag"	=>$data->GSTRegFlag,
				"DataSend"	=>$data->DataSend,
				"DataReturn"	=>$data->DataReturn,
				"DateSend"	=>$data->DateSend,
				"DateReceive"	=>$data->DateReceive
		    	
	
		    ]);
		}



		$table_name = 'DSP_CifDetailGST_1';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_CifDetailGST')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('DSP_CifDetailGST_1')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }





    public function DSP_CifDetailGSTs(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DSP_CifDetailGST::
    			get();
    	

		foreach($datas as $data){			
			DB::connection('sqlsrv2')
			->table('DSP_CifDetailGSTs')
		    ->insert([
		    	
				"ApplId" => $data->DSP_CifDetailGST_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"ApplRefNo"	=>$data->ApplRefNo,
				"ScenarioNo"	=>$data->ScenarioNo,
				"ActionCode"	=>$data->ActionCode,
				"TransactionCode"	=>$data->TransactionCode,
				"Hdrnum"	=>$data->HDRNUM,
				"ClientDt"	=>$data->ClientDt,
				"ClientTm"	=>$data->ClientTm,
				"UserId"	=>$data->UserID,
				"UserGroup"	=>$data->UserGroup,
				"Branch"	=>$data->Branch,
				"Status"	=>$data->Status,
				"ErrorCode"	=>$data->ErrorCode,
				"ErrorReason"	=>$data->ErrorReason,
				"MiCobcifNo"	=>$data->MiCOBCifNo,
				"CustIdno"	=>$data->CustIDNo,
				"Id1"	=>$data->ID1,
				"CustomerIdno2"	=>$data->CustomerIDNo2,
				"IdtypeCode2"	=>$data->IDTypeCode2,
				"IdissueCountry2"	=>$data->IDIssueCountry2,
				"Shortname"	=>$data->Shortname,
				"CustomerIdno1"	=>$data->CustomerIDNo1,
				"IdtypeCode1"	=>$data->IDTypeCode1,
				"IdissueCountry1"	=>$data->IDIssueCountry1,
				"CustomerIdnoNob2"	=>$data->CustomerIDNoNOB2,
				"IdtypeCodeNob2"	=>$data->IDTypeCodeNOB2,
				"IdissueCountryNob2"	=>$data->IDIssueCountryNOB2,
				"Branchnumber"	=>$data->Branchnumber,
				"Customernumber"	=>$data->Customernumber,
				"Salutation"	=>$data->Salutation,
				"Customername1"	=>$data->Customername1,
				"Customername2"	=>$data->Customername2,
				"Class1"	=>$data->Class1,
				"Dob"	=>$data->DOB,
				"Individual"	=>$data->Individual,
				"Countryofcitizenship"	=>$data->Countryofcitizenship,
				"RaceCountry"	=>$data->RaceCountry,
				"ResidenceBusOper"	=>$data->ResidenceBusOper,
				"Gender"	=>$data->Gender,
				"Marital"	=>$data->Marital,
				"Residentcode"	=>$data->Residentcode,
				"Businesstype"	=>$data->Businesstype,
				"Id2"	=>$data->ID2,
				"Addresssequence1"	=>$data->Addresssequence1,
				"Addresstype1"	=>$data->Addresstype1,
				"Addressformat1"	=>$data->Addressformat1,
				"Foreignaddress1"	=>$data->Foreignaddress1,
				"Addressline11"	=>$data->Addressline11,
				"Addressline12"	=>$data->Addressline12,
				"Addressline13"	=>$data->Addressline13,
				"Citystatezip1"	=>$data->Citystatezip1,
				"Postalcode1"	=>$data->Postalcode1,
				"State1"	=>$data->State1,
				"Country1"	=>$data->Country1,
				"Addressseq2"	=>$data->Addressseq2,
				"Addresstype2"	=>$data->Addresstype2,
				"Addressformat2"	=>$data->Addressformat2,
				"Foreignaddress2"	=>$data->Foreignaddress2,
				"Addressline21"	=>$data->Addressline21,
				"Addressline22"	=>$data->Addressline22,
				"Addressline23"	=>$data->Addressline23,
				"Citystatezip2"	=>$data->Citystatezip2,
				"Postalcode2"	=>$data->Postalcode2,
				"State2"	=>$data->State2,
				"Country2"	=>$data->Country2,
				"ContactSeq1"	=>$data->ContactSeq1,
				"ContactType1"	=>$data->ContactType1,
				"ContactDetail1"	=>$data->ContactDetail1,
				"ContactName1"	=>$data->ContactName1,
				"ContactSeq2"	=>$data->ContactSeq2,
				"ContactType2"	=>$data->ContactType2,
				"ContactDetail2"	=>$data->ContactDetail2,
				"ContactName2"	=>$data->ContactName2,
				"Costcenter"	=>$data->Costcenter,
				"CustomerSegmentationCode"	=>$data->CustomerSegmentationCode,
				"NumberofEmployees"	=>$data->NumberofEmployees,
				"HighRiskFlag"	=>$data->HighRiskFlag,
				"HighRiskCode"	=>$data->HighRiskCode,
				"StaffFlag"	=>$data->StaffFlag,
				"StaffIdnumber"	=>$data->StaffIDNumber,
				"Religioncode"	=>$data->Religioncode,
				"CustomerAge"	=>$data->CustomerAge,
				"Insidercode"	=>$data->Insidercode,
				"DurationStay"	=>$data->DurationStay,
				"HouseOwnershipCode"	=>$data->HouseOwnershipCode,
				"TypeofDwelling"	=>$data->TypeofDwelling,
				"Numberofdependents"	=>$data->Numberofdependents,
				"BusinessPremisesOwnershipCode"	=>$data->BusinessPremisesOwnershipCode,
				"TypeofBusinessPremises"	=>$data->TypeofBusinessPremises,
				"BusinessOperatingHours"	=>$data->BusinessOperatingHours,
				"Startoperationhours"	=>$data->STARTOPERATIONHOURS,
				"Endoperationhours"	=>$data->ENDOPERATIONHOURS,
				"BnmsectorCode"	=>$data->BNMSectorCode,
				"BnmstateCode"	=>$data->BNMStateCode,
				"BnmbumiCode"	=>$data->BNMBumiCode,
				"Vipcustomercode"	=>$data->VIPcustomercode,
				"BlacklistedFlag"	=>$data->BlacklistedFlag,
				"Sourcecode"	=>$data->Sourcecode,
				"Reasoncode"	=>$data->Reasoncode,
				"Listeddescription"	=>$data->Listeddescription,
				"GstregNo"	=>$data->GSTRegNo,
				"GstregFlag"	=>$data->GSTRegFlag,
				"DataSend"	=>$data->DataSend,
				"DataReturn"	=>$data->DataReturn,
				"DateSend"	=>$data->DateSend,
				"DateReceive"	=>$data->DateReceive
		    	
	
		    ]);
		}



		$table_name = 'DSP_CifDetailGSTs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_CifDetailGST')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('DSP_CifDetailGSTs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CalAbsentTabs(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = calabsenttab::get();
    	

		foreach($datas as $data){


			if($data->UsrStatCode == "A"){
				$UsrStatCode = true;
			}else{
				$UsrStatCode = false;
			}
			

			DB::connection('sqlsrv2')
			->table('CalAbsentTabs')
		    ->insert([

				"CreatedDate"	=> date("Y-m-d H:i:s"),
				"Act"	=>$UsrStatCode,
				"UserId"	=>$data->UsrID,
				//"EndDate"	=>$data->,
				"StartDate"	=>$data->CalDate,
				//"Name"	=>$data->,
		    ]);
		}



		$table_name = 'CalAbsentTabs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('calabsenttab')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CalAbsentTabs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }




    public function CaafUnsExposures(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = caafunsexposure::
    			//take(300)->
    			//where('ApplRefNo', 'WAPB0000039280')->
    			get();
    	

		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('CaafUnsExposures')
		    ->insert([

				"ApplId" =>$data->caafunsexposure_to_ApplRefNoTabTemps->ApplId ?? NULL,	
				"CustIdno"	=>$data->CustIDNo,
				"Facility"	=>$data->Facility,
				"BorrowerVal" =>$data->BorrowerVal,
				"GuarantorVal" =>$data->GuarantorVal,
				'CreatedDate' => ""

		    ]);
		}


		$table_name = 'CaafUnsExposures';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('caafunsexposure')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CaafUnsExposures')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }




    public function Addresses(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	$today_start = date("Y-m-d H:i:s");
    	
    	$datas = address::
    	get();
    	

		foreach($datas as $data){
	
			DB::connection('sqlsrv2')
			->table('Addresses')
		    ->insert([
		    	
				"ApplId"=>$data->address_to_ApplRefNoTabTemps->ApplId ?? NULL,	
				"NewIdtype"	=>$data->NewIDType,
				"NewIdno"	=>$data->NewIDNo,
				"AddNo"	=>$data->AddNo,
				"AddType"	=>$data->AddType,
				"AddOship"	=>$data->AddOShip,
				"Add1"	=>$data->Add1,
				"Add2"=>$data->Add2,
				"Add3"	=>$data->Add3,
				"AddCity"	=>$data->AddCity,
				"AddState"	=>$data->AddState,
				"AddCtry"	=>$data->AddCtry,
				"AddPcode"	=>$data->AddPCode,
				"AddTel1"	=>$data->AddTel1,
				"AddExt1"	=>$data->AddExt1,
				"AddTel2"	=>$data->AddTel2,
				"AddExt2"	=>$data->AddExt2,
				"AddTel3"	=>$data->AddTel3,
				"AddExt3"	=>$data->AddExt3,
				"Lmsflag"	=>$data->LMSFlag,
				"UploadDt"	=>$data->uploadDt,
				"ApplRefNoTabTempId" =>$data->address_to_ApplRefNoTabTemps->Id ?? NULL
		    	
		    ]);
		}

		$today_end = date("Y-m-d H:i:s");

		$table_name = 'Addresses';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('address')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Addresses')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status,
				    	'created_at'  => $today_start,
      					'updated_at' => $today_end
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


	public function ApplRefNoTabTemps(Request $request)
    {

        ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	$datas = applrefnotab::
    	get();



    	$get_years_setting = Config('setup.get_years_setting');

    	$start = date("Y-m-d H:i:s");


		foreach($datas as $data){

			if(DateTime::createFromFormat('Y-m-d H:i:s', $data->applrefnotab_to_applrefnotemp->EntryDt ?? NULL) == TRUE && $data->applrefnotab_to_applrefnotemp->EntryDt >= $get_years_setting){


					//AppliedDate 
					if (DateTime::createFromFormat('Y-m-d H:i:s', $data->applrefnotab_to_applrefnotemp->EntryDt ?? NULL) == FALSE) {
					  $AppliedDate = "";
					  
					}
					else{
						$AppliedDate = $data->applrefnotab_to_applrefnotemp->EntryDt;
					}
					//End  AppliedDate


					// create ID 
					$dateOldDB = date('my', strtotime($data->applrefnotab_to_applrefnotemp->EntryDt));


		        	$lastOrder = ApplRefNoTabTemps::latest('Id')->limit('1')->first();
					
		        	if ( ! $lastOrder ){
		            $number = 0;
			        }
			        else {
			            $number = $lastOrder->Id;
			        }

		        	$waps = 'WX'.$dateOldDB.str_pad($number + 1, 8, "0", STR_PAD_LEFT);


		        	$dateOldDB = date('my', strtotime($data->applrefnotab_to_applrefnotemp->EntryDt));


		        	//end create ID
					DB::connection('sqlsrv2')
					->table('ApplRefNoTabTemps')
				    ->insert([
				    	"CreatedDate"=>date("Y-m-d H:i:s"),
				    	"AppliedDate"=>$AppliedDate,


				    	"AcId"=>$data->AcID,
						"ApplRefNo"=>$data->ApplRefNo,
						"CustIdno"=>$data->CustIDNo,


						"CreatedBy"=>$data->applrefnotab_to_applrefnotemp->Userid ?? NULL,
						"CustIdty"=>$data->applrefnotab_to_applrefnotemp->CustIDTy ?? NULL,
						"BrCode"=>$data->applrefnotab_to_applrefnotemp->BrCode ?? NULL,
						"Status"=>$data->applrefnotab_to_applrefnotemp->Status ?? NULL,
						
						"LoanAmt"=>$data->applrefnotab_to_bla->LoanAmt ?? NULL,
						"BiroIndc"=>$data->applrefnotab_to_bla->BiroIndc ?? NULL,	
						"CoopCode"=>$data->applrefnotab_to_bla->CoopCode ?? NULL,
						"CodeBranchId"=>$data->applrefnotab_to_bla->OriBrch ?? NULL,
						"AppliedTenor"=>$data->applrefnotab_to_bla->ApplyPeriod ?? NULL,
						"FAName"=>$data->applrefnotab_to_bla->MOffIDNo ?? NULL,

						"Academic"=>$data->applrefnotab_to_cif->CustEduLvl ?? NULL,
						"JobStat"=>$data->applrefnotab_to_cif->CustJobStat ?? NULL,
						"PrevIdNo"=>$data->applrefnotab_to_cif->CustPrevIDNo ?? NULL,
						"WorkSector"=>$data->applrefnotab_to_cif->CustWrkSec ?? NULL,
							
						"OtherIncome"=>$data->applrefnotab_to_DSP_EmpDetail->OthIncome ?? NULL,
						
						"SubTy" => $data->applrefnotab_to_submissiontype->SubType ?? NULL,
			
						//"Id"=>$data->	/autoincrement
						"ApplId"=>$waps	
				    	
			
				    ]);

				//}
				/* End more than date */

			}
		}


		$table_name = 'ApplRefNoTabTemps';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('applrefnotab')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('ApplRefNoTabTemps')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}



		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status,
				    	'created_at' => $start,
				    	'updated_at' => date("Y-m-d H:i:s")
				    ]);



		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);

    }
    

    public function Blas(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = bla::
    			 get();
    	

		foreach($datas as $data){


			$MrgDt = $data->MrgDt;
			$AvailDate = $data->AvailDate;


			$string_date_MrgDt = substr($data->MrgDt, 0, 2);
			$string_month_MrgDt = substr($data->MrgDt, 2, 2);
			$string_year_MrgDt = substr($data->MrgDt, 4, 4);

			//check char or real date
		
			if (is_numeric($MrgDt)) {
			//is a number
				$MrgDt_get = $string_year_MrgDt.'-'.$string_month_MrgDt.'-'.$string_date_MrgDt;
			}else{
				$MrgDt_get = date('Y-m-d H:i:s', strtotime($MrgDt));
			}


			$string_date_AvailDate = substr($data->AvailDate, 0, 2);
			$string_month_AvailDate = substr($data->AvailDate, 2, 2);
			$string_year_AvailDate = substr($data->AvailDate, 4, 4);

			$AvailDate_get = $string_year_AvailDate.'-'.$string_month_AvailDate.'-'.$string_date_AvailDate;



			if($data->SMIIndicator == "Y"){
				$SMIIndicator = true;
			}else{
				$SMIIndicator = false;
			}

			if($data->LatePayIndicator == "Y"){
				$LatePayIndicator = true;
			}else{
				$LatePayIndicator = false;
			}

			DB::connection('sqlsrv2')
			->table('Blas')
		    ->insert([
		    	
				"ApplId"=>$data->bla_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"ProdCode"=>$data->ProdCode,
				"BiroIndc"=>$data->BiroIndc,
				"OriBrch"=>$data->OriBrch,
				"ApplDate"=> date('Y-m-d', strtotime($data->ApplDate)),
				"ApplBackDate"=>date('Y-m-d', strtotime($data->ApplBackDate)),
				"ApplyAmt"=>$data->ApplyAmt,
				"PurposeCode"=>$data->PurposeCode,
				"SectoralCode"=>$data->SectoralCode,
				"FacilityType"=>$data->FacilityType,
				"StateCode"=>$data->StateCode,
				"Smiindicator"=>$SMIIndicator,
				"PropSector"=>$data->PropSector,
				"MarketBy"=>$data->MarketBy,
				"SecuType"=>$data->SecuType,
				"AdvisoryId"=>$data->AdvisoryID,
				"AvailDate"=> "",
				"QuotedRate"=>$data->QuotedRate,
				"LpiquoteRate"=>$data->LPIQuoteRate,
				"ApplyPeriod"=>$data->ApplyPeriod,
				"LoanAmt"=>$data->LoanAmt,
				"InstRentalAmt"=>$data->InstRentalAmt,
				"InstAfterDeduct"=>$data->InstAfterDeduct,
				"LatePayIndicator"=>$LatePayIndicator,
				"EarlyInst"=>$data->EarlyInst,
				"UnearnProfit"=>$data->UnearnProfit,
				"CoopDealer"=>$data->CoopDealer,
				"AmtPayable"=>$data->AmtPayable,
				"CoopCode"=>$data->CoopCode,
				"CoopBrchCode"=>$data->CoopBrchCode,
				"CoopDealerCode"=>$data->CoopDealerCode,
				"CoopDealerBrchCode"=>$data->CoopDealerBrchCode,
				"MoffIdtype"=>$data->MOffIDType,
				"MoffIdno"=>$data->MOffIDNo,
				"Agind"=>$data->AGInd,
				"MrgDt"=>$MrgDt_get,
				"Lmsflag"=>$data->LMSFlag,
				"ApplSts"=>$data->ApplSts,
				"ApplBrchCd"=>$data->ApplBrchCd,
				"AprvAuth"=>$data->AprvAuth,
				"IntSprd"=>$data->IntSprd,
				"PltyRt"=>$data->PltyRt,
				"MrgnFinc"=>$data->MrgnFinc,
				"LastUpdUsrId"=>$data->LastUpdUsrId,
				"Csuflag"=>$data->CSUFlag,
				"ClerkId"=>$data->ClerkID,
				"OffId"=>$data->OffID
		    	
	
		    ]);
		}



		$table_name = 'Blas';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('bla')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Blas')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    

    public function CaafFacilities(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = caaffacility::
    				get();
    	

		foreach($datas as $data){

			
			DB::connection('sqlsrv2')
			->table('CaafFacilities')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 

					//"CreatedBy"=> "Bobonn3",
					"CreatedDate"=> date("Y-m-d H:i:s"),
					"ApplId"=> $data->caaffacility_to_ApplRefNoTabTemps->ApplId ?? NULL,
					"FacLoanLmt"=> $data->FacLoanLmt,
					"FacLoanLmtWlh"=> $data->FacLoanLmtWlh,
					"FacInstBpa"=> $data->FacInstBPA,
					"FacInstAf"=> $data->FacInstAF,
					"FacInstAfwlh"=> $data->FacInstAFWlh,
					"FacProcFee"=> $data->FacProcFee,
					"FacLawyerFee"=> $data->FacLawyerFee,
					"FacStampDuty"=> $data->FacStampDuty,
					"FacLolg"=> $data->FacLOLG,
					"FacInsurance"=> $data->FacInsurance,
					"FacMshipFee"=> $data->FacMShipFee,
					"FacCccharges"=> $data->FacCCCharges,
					"FacOtherFee"=> $data->FacOtherFee,
					"FacNetLoanAmt"=> $data->FacNetLoanAmt,
					"FacRate"=> $data->FacRate,
					"Facdeferment"=> $data->Facdeferment,
					"FacAdInst"=> $data->FacAdInst,
					"Factpc"=> $data->Factpc,
					"Facshm"=> $data->Facshm,
					"FacInsLife"=> $data->FacInsLife,
					"FacOa"=> $data->FacOA,
					"FacArb"=> $data->FacARB,
					"FacLetterGua"=> $data->FacLetterGua,
					"FacMemoDep"=> $data->FacMemoDep,
					"FacSetOff"=> $data->FacSetOff,
					"FacWadiah"=> $data->FacWadiah,
					"FacTeman"=> $data->FacTeman,
					"Csuflag"=> $data->CSUFlag,
					"FacReimbFee"=> $data->FacReimbFee,
					"FacNetLoanAmt2"=> $data->FacNetLoanAmt2,
					"FacInstAf2"=> $data->FacInstAF2,
					"FacTopUp1"=> $data->FacTopUp1,
					"FacTopUp2"=> $data->FacTopUp2,
					"FacFinalInst"=> $data->FacFinalInst,
					"FacGprocFee"=> $data->FacGProcFee,
					"FacGstampDuty"=> $data->FacGStampDuty,
					"FacGlolg"=> $data->FacGLOLG,
					"FacGinsurance"=> $data->FacGInsurance,
					"FacGotherFee"=> $data->FacGOtherFee,
					"FacGinsLife"=> $data->FacGInsLife,
					"FacGoa"=> $data->FacGOA,
					"FacGarb"=> $data->FacGARB,
					"FacGletterGua"=> $data->FacGLetterGua,
					"FacGmemoDep"=> $data->FacGMemoDep,
					"FacGsetOff"=> $data->FacGSetOff,
					"FacGwadiah"=> $data->FacGWadiah,
					"FacGteman"=> $data->FacGTeman
		    		//"FacSellingPrice"
					//"FacUnEarnedProfit"
					//"SalaryUtil"

		    	//"Role" => $data->

		    ]);
		}



		$table_name = 'CaafFacilities';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('caaffacility')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CaafFacilities')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }




    public function ApplWorkFlows(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	$today_start = date("Y-m-d H:i:s");
    	
    	$datas = applworkflow::
    			get();

    	

		foreach($datas as $data){

			$date_StProcessDt = date('Y-m-d', strtotime($data->StProcDt));
			$date_StProcessTm = date('H:i:s', strtotime($data->StProcTm));

			$date_EndProcTm = date('H:i:s', strtotime($data->EndProcTm));
			$date_EndProcDt = date('Y-m-d ', strtotime($data->EndProcDt));

			$date_StRouteDt = date('Y-m-d', strtotime($data->StRouteDt));
			$date_StRouteTm = date('H:i:s', strtotime($data->StRouteTm));

			$StProcessDtTm = $date_StProcessDt." ".$date_StProcessTm;
			$EndProcessDtTm = $date_EndProcDt." ".$date_EndProcTm;
			$StRouteDtTm = $date_StRouteDt." ".$date_StRouteTm;



			DB::connection('sqlsrv2')
			->table('ApplWorkFlows')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 

		    	"ApplId"=>   $data->applworkflow_to_ApplRefNoTabTemps->ApplId ?? NULL,	
		    	"UserId" => $data->UsrID,
		    	"PrevUserId" => $data->PrevUsrID,



		    	"StProcessDtTm" => $StProcessDtTm,
		    	"EndProcessDtTm" => $EndProcessDtTm,
		    	"StRouteDtTm" => $StRouteDtTm,



		    	"TotProcessTm" => $data->TotProcTm,
		    	"NetProcessTm" => $data->NetTm,
		    	"Remark" => $data->Rmrk,
		    	"AppStatus" => $data->AppStatus,
		    	"NextProcess" => $data->NextProccess,
		    	"DualSign" => $data->DualSignID,
		    	"PenReasCode" => $data->PenReasCode,
		    	"CSUFlag" => $data->CSUFlag,
		    	"BrCode" => $data->BrCode,
		    	"PickFlag" => $data->PickFlag,
		    	"UserId_Curr" => $data->Usrid_Curr,
		    	

		    	//"Role" => $data->

		    ]);
		}

		$today_end = date("Y-m-d H:i:s");

		$table_name = 'ApplWorkFlows';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('applworkflow')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('ApplWorkFlows')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status,
				    	'created_at' => $today_start,
				    	'updated_at' => $today_end
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CaafRedemptions(Request $request)
    {

    	ini_set('max_execution_time', 300000);
   	
    	$datas = caafredemption::
    			//where('ApplRefNo', 'WAP00000001226')->
    			get();
    	

		foreach($datas as $data){

			$check_char_or_numb = substr($data->expDt, 3, 1);
			
			if (is_numeric($check_char_or_numb)) {
				//is a number
				$newDate = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1",$data->expDt);
				$date_convert = date('Y-m-d H:i:s', strtotime($newDate));


			}else{
				//is a letter
				$date_get = substr($data->expDt, 0, 2);
				$month_get = substr($data->expDt, 3, 3);
				$year_get = substr($data->expDt, 7, 4);

				$change_lowercase = strtolower($month_get);

				if($change_lowercase == "jan"){
					$is_month = "01";
				}elseif($change_lowercase == "feb"){
					$is_month = "02";
				}elseif($change_lowercase == "mar"){
					$is_month = "03";
				}elseif($change_lowercase == "apr"){
					$is_month = "04";
				}elseif($change_lowercase == "may"){
					$is_month = "05";
				}elseif($change_lowercase == "jun"){
					$is_month = "06";
				}elseif($change_lowercase == "jul"){
					$is_month = "07";
				}elseif($change_lowercase == "aug"){
					$is_month = "08";
				}elseif($change_lowercase == "sep"){
					$is_month = "09";
				}elseif($change_lowercase == "oct"){
					$is_month = "10";
				}elseif($change_lowercase == "nov"){
					$is_month = "11";
				}elseif($change_lowercase == "des"){
					$is_month = "12";
				}

				$date = $date_get. "-". $is_month ."-". $year_get;
				$date_convert = date('Y-m-d H:i:s', strtotime($date));

			}



			DB::connection('sqlsrv2')
			->table('CaafRedemptions')
		    ->insert([
		    	
				"ApplId"=> $data->caafredemption_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"SetPtyCode"=> $data->SetPtyCode,
				"DeducVal"=> $data->DeducVal,
				"OsloanAmt"=> $data->OSLoanAmt,
				"DtSubmit"=> date('Y-m-d', strtotime($data->DtSubmit)),
				"DtObtReceipt"=> date('Y-m-d', strtotime($data->DtObtReceipt)),
				"VerifyBy"=> $data->VerifyBy,
				
				"ExpDt"=>  $date_convert,
				
				"Csuflag"=> $data->CSUFlag,
				//"CreatedBy"=> 'Bobonn3',
				//"CreatedDate"=> date("Y-m-d H:i:s")
				//"RedempId"
	

		    ]);
		}



		$table_name = 'CaafRedemptions';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('caafredemption')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CaafRedemptions')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CaafTabs(Request $request)
    {

    	ini_set('max_execution_time', 300000);
   	
    	
    	$datas = caaftab::
    	get();
    	
		foreach($datas as $data){


			if($data->GuarReq == "Y"){
				$GuarReq = true;
			}else{
				$GuarReq = false;
			}


			DB::connection('sqlsrv2')
			->table('CaafTabs')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 

		    	//"Role" => $data->
		    	//"CreatedDate"  => date("Y-m-d H:i:s"),
				
				"CreatedDate"  => "",
				"ApplId"=> $data->caaftab_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"SameMember"=> $data->SameMember,
				"MaxDeduc"=> $data->MaxDeduc,
				"MaxDeducWlh"=> $data->MaxDeducWlh,
				"CurrTotDeduc"=> $data->CurrTotDeduc,
				"InsPrem"=> $data->InsPrem,
				"TotRedemptD"=> $data->TotRedemptD,
				"TotRedemptAmt"=> $data->TotRedemptAmt,
				"TotDeduc"=> $data->TotDeduc,
				"AddDeduc"=> $data->AddDeduc,
				"AddDeducWlh"=> $data->AddDeducWlh,
				"CalcLoanLmt"=> $data->CalcLoanLmt,
				"CalcLoanLmtWlh"=> $data->CalcLoanLmtWlh,
				"PolicyLoanLmt"=> $data->PolicyLoanLmt,
				"PolicyLoanLmtWlh"=> $data->PolicyLoanLmtWlh,
				"RecommLimit"=> $data->RecommLimit,
				"RecommLimitWlh"=> $data->RecommLimitWlh,
				"PropLoanLmt"=> $data->PropLoanLmt,
				"PropLoanLmtWlh"=> $data->PropLoanLmtWlh,
				"Check2"=> $data->check2,
				"TotCurrUnsExpB"=> $data->TotCurrUnsExpB,
				"TotCurrUnsExpG"=> $data->TotCurrUnsExpG,
				"TotUnsExp"=> $data->TotUnsExp,
				"ExistLoan"=> $data->ExistLoan,
				"Nricverify"=> $data->NRICVerify,
				"AcardSubmit"=> $data->ACardSubmit,
				"BcertConfirm"=> $data->BCertConfirm,
				"VerifyBy"=> $data->VerifyBy,
				"SalaryFont"=> $data->SalaryFont,
				"PaySlipCertify"=> $data->PaySlipCertify,
				"EmpStatConfirm"=> $data->EmpStatConfirm,
				"PersContact"=> $data->PersContact,
				"DepartName"=> $data->DepartName,
				"PhoneNo"=> $data->PhoneNo,
				"CallDt"=> $data->CallDt,
				"BpaexDeduc"=> $data->BPAExDeduc,
				"BpacurrSalary"=> $data->BPACurrSalary,
				"BpaauthSign"=> $data->BPAAuthSign,
				"Mbscheck"=> $data->MBSCheck,
				"Ctoscheck"=> $data->CTOSCheck,
				"Ccrischeck"=> $data->CCRISCheck,
				"TermCond"=> $data->TermCond,
				"DevCode"=> $data->DevCode,
				"PropTenure"=> $data->PropTenure,
				"InTransit"=> $data->InTransit,
				"AgbranchCode"=> $data->AGBranchCode,
				"AgpayCentre"=> $data->AGPayCentre,
				"AgdepartmentCode"=> $data->AGDepartmentCode,
				"AgpersonnelNo"=> $data->AGPersonnelNo,
				"Agind"=> $data->AGInd,
				"AgbranchCodeRev"=> $data->AGBranchCode_Rev,
				"AgpayCentreRev"=> $data->AGPayCentre_Rev,
				"AgdepartmentCodeRev"=> $data->AGDepartmentCode_Rev,
				"AgpersonnelNoRev"=> $data->AGPersonnelNo_Rev,
				"AngkasaCode"=> $data->AngkasaCode,
				"Agstate"=> $data->AGState,
				"AngkasaCode2"=> $data->AngkasaCode2,
				"Dsrrate"=> $data->DSRRate,
				"CreditGrade"=> $data->CreditGrade,
				"Epf"=> $data->EPF,
				"Socso"=> $data->SOCSO,
				"Zakat"=> $data->Zakat,
				"Tax"=> $data->Tax,
				"CreditGradeRmk"=> $data->CreditGradeRmk,
				"TotActComm"=> $data->TotActComm,
				//"AngkasaId"=> $data->	
				//"NoGaji"=> $data->	
				//"NoGajiRev"=> $data->	
				//"SalaryDt"=> $data->	
				"GuarReq"=> $GuarReq


		    ]);
		}



		$table_name = 'CaafTabs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('caaftab')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CaafTabs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function ApplKpis(Request $request)
    {

    	ini_set('max_execution_time', 3000000);

    	$today = date("Y-m-d H:i:s");
    	
    	/*$datas = DB::connection('sqlsrv')
    			->table('applkpi')
               ->select('*')
               ->where('ApplRefNo', 'WAPB0000006759')
               ->get();*/


        $datas = applkpi::
            //->where('applkpi.ApplRefNo', 'WAPB0000000005')
            get();
    	

		foreach($datas as $data){

			
	      	$Procdt = date('Y-m-d H:i:s', strtotime($data->Procdt));
			
			DB::connection('sqlsrv2')
			->table('ApplKpis')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 

				"ApplId"	=>   $data->applkpi_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"SubmitDt"	=>   date('Y-m-d H:i:s', strtotime($data->SubmitDt)),
				"ReviewDt"	=>   date('Y-m-d H:i:s', strtotime($data->ReviewDt)),
				"AppvDt"	=>   date('Y-m-d H:i:s', strtotime($data->AppvDt)),
				"DisbDt"	=>   date('Y-m-d H:i:s', strtotime($data->DisbDt)),
				"SubAppvDay"	=>   $data->SubAppvDay,
				"SubDisbDay"	=>   $data->SubDisbDay,
				"AppvDisbDay"	=>   $data->AppvDisbDay,
				"Procdt"	=>   date('Y-m-d H:i:s', strtotime($data->Procdt)), 
				"SubProcDay"	=>   $data->SubProcDay,
				"SubRevDay"	=>   $data->SubRevDay

		    	//"Role" => $data->

		    ]);
		}



		$table_name = 'ApplKpis';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('applkpi')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('ApplKpis')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function ApplDisburses(Request $request)
    {
    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	$today_start = date("Y-m-d H:i:s");

    	
    	
    	
    	$datas = appldisburse::
               get();
    	

		foreach($datas as $data){

			
	      	$COdrDate = date('Y-m-d H:i:s', strtotime($data->COdrDate));
	      	$ReleaseDt = date('Y-m-d H:i:s', strtotime($data->ReleaseDt));


      		$TransDtTm = $data->TransDtTm;
      		$string_month = substr($TransDtTm, 0, 3);
			$string_date = substr($TransDtTm, 3, 2);
			$string_year = substr($TransDtTm, 5, 4);
			$Month_TransDtTm = date('m', strtotime($string_month));


			$TransDtTm_get = $string_year.'-'.$Month_TransDtTm.'-'.$string_date;
			$TransDtTm_convert = date('Y-m-d H:i:s', strtotime($TransDtTm_get));


			$UploadDtx = $data->UploadDt;

			$string_date_uploadDt = substr($UploadDtx, 0, 2);
			$string_month_uploadDt = substr($UploadDtx, 2, 2);
			$string_year_uploadDt = substr($UploadDtx, 4, 4);

			$UploadDtx_get = $string_year_uploadDt.'-'.$string_month_uploadDt.'-'.$string_date_uploadDt;
			$UploadDtx_convert = date('Y-m-d H:i:s', strtotime($UploadDtx_get));


			if(empty($data->Netloan2)){
				$Netloan2 = 0;
			}else{
				$Netloan2 = $data->Netloan2;
			}

			if(empty($data->TransAmt2)){
				$TransAmt2 = 0;
			}else{
				$TransAmt2 = $data->TransAmt2;
			}

			if(empty($data->NetBal2)){
				$NetBal2 = 0;
			}else{
				$NetBal2 = $data->NetBal2;
			}

			if(empty($data->AdminFee)){
				$AdminFee = 0;
			}else{
				$AdminFee = $data->AdminFee;
			}

			if(empty($data->Wadiah)){
				$Wadiah = 0;
			}else{
				$Wadiah = $data->Wadiah;
			}

			if(empty($data->ISave)){
				$Isave = 0;
			}else{
				$Isave = $data->ISave;
			}

			if(empty($data->Wakalah)){
				$Wakalah = 0;
			}else{
				$Wakalah = $data->Wakalah;
			}

			if(empty($data->Inc)){
				$Inc = 0;
			}else{
				$Inc = $data->Inc;
			}

			if(empty($data->postage)){
				$Postage = 0;
			}else{
				$Postage = $data->postage;
			}

			if(empty($data->CCCharge)){
				$Cccharge = 0;
			}else{
				$Cccharge = $data->CCCharge;
			}

			if(empty($data->TransAmtGTT1)){
				$TransAmtGtt1 = 0;
			}else{
				$TransAmtGtt1 = $data->TransAmtGTT1;
			}

			if(empty($data->TransAmtGTT2)){
				$TransAmtGtt2 = 0;
			}else{
				$TransAmtGtt2 = $data->TransAmtGTT2;
			}

			if(empty($data->Netloan)){
				$Netloan = 0;
			}else{
				$Netloan = $data->Netloan;
			}

			if(empty($data->NetBal)){
				$NetBal = 0;
			}else{
				$NetBal = $data->NetBal;
			}

			if(empty($data->StampDuty)){
				$StampDuty = 0;
			}else{
				$StampDuty = $data->StampDuty;
			}

			if(empty($data->SecDep)){
				$SecDep = 0;
			}else{
				$SecDep = $data->SecDep;
			}

			if(empty($data->ProcFee)){
				$ProcFee = 0;
			}else{
				$ProcFee = $data->ProcFee;
			}

			if(empty($data->TransAmt)){
				$TransAmt = 0;
			}else{
				$TransAmt = $data->TransAmt;
			}

			if(empty($data->TransAmtCO)){
				$TransAmtCo = 0;
			}else{
				$TransAmtCo = $data->TransAmtCO;
			}

			if(empty($data->TransAmtATT)){
				$TransAmtAtt = 0;
			}else{
				$TransAmtAtt = $data->TransAmtATT;
			}

			if(empty($data->TransAmtRCT)){
				$TransAmtRct = 0;
			}else{
				$TransAmtRct = $data->TransAmtRCT;
			}

			if(empty($data->TransAmtGTT)){
				$TransAmtGtt = 0;
			}else{
				$TransAmtGtt = $data->TransAmtGTT;
			}

			if(empty($data->OSBal)){
				$Osbal = 0;
			}else{
				$Osbal = $data->OSBal;
			}

			if(empty($data->LOAmt)){
				$Loamt = 0;
			}else{
				$Loamt = $data->LOAmt;
			}


			if(empty($data->ActAmt)){
				$ActAmt = 0;
			}else{
				$ActAmt = $data->ActAmt;
			}

			if(empty($data->GLTransAmt)){
				$GltransAmt = 0;
			}else{
				$GltransAmt = $data->GLTransAmt;
			}


			if(empty($data->Insurans)){
				$Insurans = 0;
			}else{
				$Insurans = $data->Insurans;
			}

			if(empty($data->MDTA)){
				$Mdta = 0;
			}else{
				$Mdta = $data->MDTA;
			}

			if(empty($data->IBGFee)){
				$Ibgfee = 0;
			}else{
				$Ibgfee = $data->IBGFee;
			}

			if(empty($data->OASearch)){
				$Oasearch = 0;
			}else{
				$Oasearch = $data->OASearch;
			}

			if(empty($data->ARB)){
				$Arb = 0;
			}else{
				$Arb = $data->ARB;
			}

			if(empty($data->CommARB)){
				$CommArb = 0;
			}else{
				$CommArb = $data->CommARB;
			}

			if(empty($data->Teman)){
				$Teman = 0;
			}else{
				$Teman = $data->Teman;
			}

			if(empty($data->GProcFee)){
				$GprocFee = 0;
			}else{
				$GprocFee = $data->GProcFee;
			}

			if(empty($data->GStampDuty)){
				$GstampDuty =0 ;
			}else{
				$GstampDuty = $data->GStampDuty ;
			}

			if(empty($data->GLOLG)){
				$Glolg = 0;
			}else{
				$Glolg = $data->GLOLG;
			}

			if(empty($data->GInsurance)){
				$Ginsurance = 0;
			}else{
				$Ginsurance = $data->GInsurance;
			}

			if(empty($data->GOtherFee)){
				$GotherFee = 0;
			}else{
				$GotherFee = $data->GOtherFee;
			}

			if(empty($data->GInsLife)){
				$GinsLife = 0;
			}else{
				$GinsLife = $data->GInsLife;
			}

			if(empty($data->GOA)){
				$Goa = 0;
			}else{
				$Goa = $data->GOA;
			}

			if(empty($data->GARB)){
				$Garb = 0;
			}else{
				$Garb = $data->GARB;
			}

			if(empty($data->GLetterGua)){
				$GletterGua = 0;
			}else{
				$GletterGua = $data->GLetterGua;
			}

			if(empty($data->GMemoDep)){
				$GmemoDep = 0;
			}else{
				$GmemoDep = $data->GMemoDep;
			}

			if(empty($data->GSetOff)){
				$GsetOff = 0;
			}else{
				$GsetOff = $data->GSetOff;
			}

			if(empty($data->GWadiah)){
				$Gwadiah =0;
			}else{
				$Gwadiah = $data->GWadiah;
			}

			if(empty($data->GTeman)){
				$Gteman  =0;
			}else{
				$Gteman  = $data->GTeman;
			}

			if(empty($data->GSecDep)){
				$GsecDep =0;
			}else{
				$GsecDep = $data->GSecDep;
			}

			if(empty($data->GMDTA)){
				$Gmdta =0;
			}else{
				$Gmdta = $data->GMDTA;
			}

			if(empty($data->GIBGFee)){
				$Gibgfee =0;
			}else{
				$Gibgfee = $data->GIBGFee;
			}

			if(empty($data->GOASearch)){
				$Goasearch =0;
			}else{
				$Goasearch = $data->GOASearch;
			}

			if(empty($data->GCommARB)){
				$GcommArb =0;
			}else{
				$GcommArb = $data->GCommARB;
			}

			if(empty($data->GISave)){
				$Gisave =0;
			}else{
				$Gisave = $data->GISave;
			}

			if(empty($data->GWakalah)){
				$Gwakalah =0;
			}else{
				$Gwakalah = $data->GWakalah;
			}

			if(empty($data->GAdminFee)){
				$GadminFee =0;
			}else{
				$GadminFee =$data->GAdminFee;
			}
			


			DB::connection('sqlsrv2')
			->table('ApplDisburses')
			->insert([

				"UploadDt"	=> $UploadDtx_convert,
				"TransDtTm"	=> $TransDtTm_convert,
				"CodrDate"	=> $COdrDate,
				"ReleaseDt"	=> $ReleaseDt,


				"ApplId"	=> $data->appldisburse_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"ApplRefNo"	=> $data->ApplRefNo,
				"LoanAccNo"	=> $data->LoanAccNo,
				"OSBal"=> $Osbal,
				"Loamt"	=> $Loamt,
				"ActAmt"	=> $ActAmt,
				"GltransAmt"	=> $GltransAmt,
				"Acno"	=> $data->ACNo,

				"TransBrCode"	=> $data->TransBrCode,
				"TransCodeCo"	=> $data->TransCodeCO,
				"TransCodeAtt"	=> $data->TransCodeATT,
				"TransCodeRct"	=> $data->TransCodeRCT,
				"TransCodeGtt"	=> $data->TransCodeGTT,
				"TransUsrId"	=> $data->TransUsrID,
				"GltransAmt2"	=> $data->GLTransAmt2,
				"TransAmt"	=> $TransAmt,
				"TransAmtRef"	=> $data->TransAmtRef,
				"TransAmtDesc"	=> $data->TransAmtDesc,
				"AuthUsrId"	=> $data->AuthUsrID,
				"TransAmtCo"	=> $TransAmtCo,
				"TransAmtAtt"=> $TransAmtAtt,
				"TransAmtRct"	=> $TransAmtRct,
				"TransAmtGtt"	=> $TransAmtGtt,
				"PayeeTy"	=> $data->PayeeTy,
				"PayeeName"	=> $data->PayeeName,
				"Glacco"	=> $data->GLACCO,
				"Glacatt"	=> $data->GLACATT,
				"Glacrct"	=> $data->GLACRCT,
				"Glacgtt"	=> $data->GLACGTT,
				"HoldDisb"	=> $data->HoldDisb,
				"Inc"		=> $Inc,
				"Postage"	=> $Postage,
				"Cccharge"	=> $Cccharge,
				"ReqDis"	=> $data->ReqDis,
				"Netloan"	=> $Netloan,
				"StampFee"	=> $data->StampFee,
				"DisbDeferment"	=> $data->DisbDeferment,
				"DisbAdInst"	=> $data->DisbAdInst,
				"Disbtpc"	=> $data->Disbtpc,
				"Disbtpcno"	=> $data->Disbtpcno,
				"Disbshm"	=> $data->Disbshm,
				"TranscodeGtt1"	=> $data->TranscodeGTT1,
				"TranscodeGtt2"	=> $data->TranscodeGTT2,
				"TransAmtGtt1"	=> $TransAmtGtt1,
				"TransAmtGtt2"	=> $TransAmtGtt2,
				"Glacgtt1"	=> $data->GLACGTT1,
				"Glacgtt2"	=> $data->GLACGTT2,
				"CancelTransact"	=> $data->CancelTransact,
				"Lmsflag"	=> $data->LMSFlag,
				"NetBal"	=> $NetBal,
				"GlcnetBal"	=> $data->GLCNetBal,
				"NetBalDesc"	=> $data->NetBalDesc,
				"NetBalRef"	=> $data->NetBalRef,
				"TransCodeStamp"	=> $data->TransCodeStamp,
				"Glcstamp"	=> $data->GLCStamp,
				"StampDuty"	=> $StampDuty,
				"StampDesc"	=> $data->StampDesc,
				"StampRef"	=> $data->StampRef,
				"Glcstamp2"	=> $data->GLCStamp2,
				"TransCodeSd"	=> $data->TransCodeSD,
				"Glcsd"	=> $data->GLCSD,
				"SecDep"	=> $SecDep,
				"SecDepDesc"	=> $data->SecDepDesc,
				"SecDepRef"	=> $data->SecDepRef,
				"Glcsd2"	=> $data->GLCSD2,
				"TransCodeProcFee"	=> $data->TransCodeProcFee,
				"GlcprocFee"	=> $data->GLCProcFee,
				"ProcFee"	=> $ProcFee,
				"ProcFeeDesc"	=> $data->ProcFeeDesc,
				"ProcFeeRef"	=> $data->ProcFeeRef,
				"GlcprocFee2"	=> $data->GLCProcFee2,
				"TransCodeIns"	=> $data->TransCodeIns,
				"Glcins"	=> $data->GLCIns,
				"Insurans"	=> $Insurans,
				"InsDesc"	=> $data->InsDesc,
				"InsRef"	=> $data->InsRef,
				"Glcins2"	=> $data->GLCIns2,
				"TransCodeMdta"	=> $data->TransCodeMDTA,
				"Glcmdta"	=> $data->GLCMDTA,
				"Mdta"	=> $Mdta,
				"Mdtadesc"	=> $data->MDTADesc,
				"Mdtaref"	=> $data->MDTARef,
				"Glcmdta2"	=> $data->GLCMDTA2,
				"TransCodeIbgfee"	=> $data->TransCodeIBGFee,
				"Glcibgfee"	=> $data->GLCIBGFee,
				"Ibgfee"	=> $Ibgfee,
				"Ibgdesc"	=> $data->IBGDesc,
				"Ibgref"	=> $data->IBGRef,
				"Glcibgfee2"	=> $data->GLCIBGFee2,
				"TransCodeOa"	=> $data->TransCodeOA,
				"Glcoa"	=> $data->GLCOA,
				"Oasearch"	=> $Oasearch,
				"Oadesc"	=> $data->OADesc,
				"Oaref"	=> $data->OARef,
				"Glcoa2"	=> $data->GLCOA2,
				"TransCodeArb"	=> $data->TransCodeARB,
				"Glcarb"	=> $data->GLCARB,
				"Arb"	=> $Arb,
				"Arbdesc"	=> $data->ARBDesc,
				"Arbref"	=> $data->ARBRef,
				"Glcarb2"	=> $data->GLCARB2,
				"TransCodeCommArb"	=> $data->TransCodeCommARB,
				"GlccommArb"	=> $data->GLCCommARB,
				"CommArb"	=> $CommArb,
				"CommArbdesc"	=> $data->CommARBDesc,
				"CommArbref"	=> $data->CommARBRef,
				"GlccommArb2"	=> $data->GLCCommARB2,
				"TranscodeWadiah"	=> $data->TranscodeWadiah,
				"Glcwadiah"	=> $data->GLCWadiah,
				"Wadiah"	=> $Wadiah,
				"WadiahDesc"	=> $data->WadiahDesc,
				"WadiahRef"	=> $data->WadiahRef,
				"Glcwadiah2"	=> $data->GLCWadiah2,
				"TranscodeTeman"	=> $data->TranscodeTeman,
				"Glcteman"	=> $data->GLCTeman,
				"Teman"	=> $Teman,
				"TemanDesc"	=> $data->TemanDesc,
				"TemanRef"	=> $data->TemanRef,
				"Glcteman2"	=> $data->GLCTeman2,
				"PayMode"	=> $data->payMode,
				"PayBank"	=> $data->payBank,
				"Arbcode"	=> $data->ARBCode,
				"WadiahFlag"	=> $data->WadiahFlag,
				"WadBranch"	=> $data->WadBranch,
				"TemanFlag"	=> $data->TemanFlag,
				"ReimbFee"	=> $data->ReimbFee,
				"IsaveFlag"	=> $data->ISaveFlag,
				"TranscodeIsave"	=> $data->TranscodeISave,
				"Glcisave"	=> $data->GLCISave,
				"Isave"	=> $Isave,
				"IsaveDesc"	=> $data->ISaveDesc,
				"IsaveRef"	=> $data->ISaveRef,
				"Glcisave2"	=> $data->GLCISave2,
				"WakalahFlag"	=> $data->WakalahFlag,
				"TranscodeWakalah"	=> $data->TranscodeWakalah,
				"Glcwakalah"	=> $data->GLCWakalah,
				"Wakalah"	=> $Wakalah,
				"WakalahDesc"	=> $data->WakalahDesc,
				"WakalahRef"	=> $data->WakalahRef,
				"Glcwakalah2"	=> $data->GLCWakalah2,
				"LiveFlag"	=> $data->LiveFlag,
				"Csuflag"	=> $data->CSUFlag,
				"ApplRefNo2"	=> $data->ApplRefNo2,
				"LoanAccNo2"	=> $data->LoanAccNo2,
				"Netloan2"=> $Netloan2,
				"TransAmt2"	=> $TransAmt2,
				"NetBal2"	=> $NetBal2,
				"AdminFeeFlag"	=> $data->AdminFeeFlag,
				"TranscodeAdminFee"	=> $data->TranscodeAdminFee,
				"GlcadminFee"	=> $data->GLCAdminFee,
				"AdminFee"	=> $AdminFee,
				"AdminFeeDesc"	=> $data->AdminFeeDesc,
				"AdminFeeRef"	=> $data->AdminFeeRef,
				"GlcadminFee2"	=> $data->GLCAdminFee2,
				"RepayTy"	=> $data->RepayTy,
				"GprocFee"	=> $GprocFee,
				"GstampDuty"	=> $GstampDuty,
				"Glolg"	=> $Glolg,
				"Ginsurance"	=> $Ginsurance,
				"GotherFee"	=> $GotherFee,
				"GinsLife"	=> $GinsLife,
				"Goa"	=> $Goa,
				"Garb"	=> $Garb,
				"GletterGua"	=> $GletterGua,
				"GmemoDep"	=> $GmemoDep,
				"GsetOff"	=> $GsetOff,
				"Gwadiah"	=> $Gwadiah,
				"Gteman"	=> $Gteman,
				"GsecDep"	=> $GsecDep,
				"Gmdta"	=> $Gmdta,
				"Gibgfee"	=> $Gibgfee,
				"Goasearch"	=> $Goasearch,
				"GcommArb"	=> $GcommArb,
				"Gisave"	=> $Gisave,
				"Gwakalah"	=> $Gwakalah,
				"GadminFee"	=> $GadminFee,
				"GtaxCodeProcFee"	=> $data->GTaxCodeProcFee,
				"GtaxCodeStampDuty"	=> $data->GTaxCodeStampDuty,
				"GtaxCodeInsurance"	=> $data->GTaxCodeInsurance,
				"GtaxCodeArb"	=> $data->GTaxCodeARB,
				"GtaxCodeWadiah"	=> $data->GTaxCodeWadiah,
				"GtaxCodeTeman"	=> $data->GTaxCodeTeman,
				"GtaxCodeSecDep"	=> $data->GTaxCodeSecDep,
				"GtaxCodeMdta"	=> $data->GTaxCodeMDTA,
				"GtaxCodeIbgfee"	=> $data->GTaxCodeIBGFee,
				"GtaxCodeOasearch"	=> $data->GTaxCodeOASearch,
				"GtaxCodeCommArb"	=> $data->GTaxCodeCommARB,
				"GtaxCodeIsave"	=> $data->GTaxCodeISave,
				"GtaxCodeWakalah"	=> $data->GTaxCodeWakalah,
				"GtaxCodeAdminFee"	=> $data->GTaxCodeAdminFee,
				"InsCode"	=> $data->InsCode

			]);
		}


    	$today_end = date("Y-m-d H:i:s");

		$table_name = 'ApplDisburses';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('appldisburse')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('ApplDisburses')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status,
				    	'created_at' => $today_start,
				    	'updated_at' => $today_end
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function SubmissionTypes(Request $request)
    {

    	ini_set('max_execution_time', 3000000);

    	$today = date("Y-m-d H:i:s");
    	

        $datas = submissiontype::
        		 get();
    	

		foreach($datas as $data){
			
	      	$Procdt = date('Y-m-d H:i:s', strtotime($data->Procdt));
			
			DB::connection('sqlsrv2')
			->table('SubmissionTypes')
		    ->insert([		    	

				"ApplId"	=>   $data->submissiontype_to_ApplRefNoTabTemps->ApplId ?? NULL,
				"CustNewIdNo"	=>   $data->CustNewIDNo,
				"LoanAmt"	=>   $data->LoanAmt,
				"SubType"	=>   $data->SubType

		    ]);
		}



		$table_name = 'SubmissionTypes';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('submissiontype')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('SubmissionTypes')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CMSCaafRedemptions(Request $request)
    {

    	ini_set('max_execution_time', 3000000);


    	$today = date("Y-m-d H:i:s");
    	

        $datas = cmscaafredemption::
        //take(200)->
           	//where('ApplRefNo', 'WAPB0000111120')->
            get();
    	

            foreach($datas as $data){

            	/*--------------- UpdateDt ------------*/
            	$UpdateDt = $data->UpdateDt;
				$string_month = substr($UpdateDt, 0, 3);
				$string_date = substr($UpdateDt, 3, 2);
				$string_year = substr($UpdateDt, 5, 4);

				$convert_month = date('m', strtotime($string_month));
				

				$UpdateDt_get = $string_year.'-'.$convert_month.'-'.$string_date;
				$UpdateDt_convert = date('Y-m-d H:i:s', strtotime($UpdateDt_get));



				/*--------------- ExpDt ------------*/

				$check_char_or_numb = substr($data->ExpDt, 3, 1);
			
				if (is_numeric($check_char_or_numb)) {
					//is a number
					$newDate = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1",$data->ExpDt);
					$date_convert = date('Y-m-d H:i:s', strtotime($newDate));


				}else{
					//is a letter
					$date_get = substr($data->ExpDt, 0, 2);
					$month_get = substr($data->ExpDt, 3, 3);
					$year_get = substr($data->ExpDt, 7, 4);

					$change_lowercase = strtolower($month_get);

					if($change_lowercase == "jan"){
						$is_month = "01";
					}elseif($change_lowercase == "feb"){
						$is_month = "02";
					}elseif($change_lowercase == "mar"){
						$is_month = "03";
					}elseif($change_lowercase == "apr"){
						$is_month = "04";
					}elseif($change_lowercase == "may"){
						$is_month = "05";
					}elseif($change_lowercase == "jun"){
						$is_month = "06";
					}elseif($change_lowercase == "jul"){
						$is_month = "07";
					}elseif($change_lowercase == "aug"){
						$is_month = "08";
					}elseif($change_lowercase == "sep"){
						$is_month = "09";
					}elseif($change_lowercase == "oct"){
						$is_month = "10";
					}elseif($change_lowercase == "nov"){
						$is_month = "11";
					}elseif($change_lowercase == "des"){
						$is_month = "12";
					}

					$date = $date_get. "-". $is_month ."-". $year_get;
					$date_convert = date('Y-m-d H:i:s', strtotime($date));
				}



            	DB::connection('sqlsrv2')
            	->table('CMSCaafRedemptions')
            	->insert([

            		//"RmdId"	=>	$data->RmtID,
            		"ApplId"	=>	$data->cmscaafredemption_to_ApplRefNoTabTemps->ApplId ?? NULL,
            		"SetPtyCode"	=>	$data->SetPtyCode,
            		"DeducVal"	=>	$data->DeducVal,
            		"OSLoanAmt"	=>	$data->OSLoanAmt,
            		"LnACNo"	=>	$data->LnACNo,
            		"UpdateBy"	=>	$data->UpdateBy,
            		"UpdateDt"	=>	$UpdateDt_convert,
            		"DtSubmit"	=>	$data->DtSubmit,
            		"DtObtReceipt"	=>	$data->DtObtReceipt,
            		"VerifyBy"	=>	$data->VerifyBy,
            		"Rmk"	=>	$data->Rmk,
            		"ExpDt"	=>	$date_convert,
            		"GLCCoop"	=>	$data->GLCCoop,
            		"CoopDesc"	=>	$data->CoopDesc,
            		"CoopRef"	=>	$data->CoopRef,
            		"AGTFlag"	=>	$data->AGTFlag,
            		"BankName"	=>	$data->BankName,
            		"BankCode"	=>	$data->BankCode,
            		"BankAccNo"	=>	$data->BankAccNo,
            		"BankBusRegNo"	=>	$data->BankBusRegNo

            	]);
            }



		$table_name = 'CMSCaafRedemptions';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('cmscaafredemption')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CMSCaafRedemptions')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function Dsp_Creation_AccNos(Request $request)
    {

    	ini_set('max_execution_time', 3000000);

    	$today = date("Y-m-d H:i:s");
    	

        $datas = DSP_AccountCreation:://take(100)->
            //->where('applkpi.ApplRefNo', 'WAPB0000000005')
            get();
    	

            foreach($datas as $data){

            	
            	DB::connection('sqlsrv2')
            	->table('Dsp_Creation_AccNos')
            	->insert([

            		//'DspCreationAcNoId'	=>	$data->	
            		'ApplID'	=>	$data->DSP_AccountCreation_to_ApplRefNoTabTemps->ApplId ?? NULL,
            		'UserName'	=>	$data->UserID,
            		//Role	=>	$data->	
            		'ScenarioNo'	=>	$data->ScenarioNo,
            		'ActionCode'	=>	$data->ActionCode,
            		'TrnCode'	=>	$data->TransactionCode,
            		'ClientDt'	=>	$data->ClientDt,
            		'ClientTm'	=>	$data->ClientTm,
            		'Branch'	=>	$data->Branch,
            		'MiCOBCifNo'	=>	$data->MiCOBCifNo,
            		//'DSPAAFacSeqNo'	=>	$data->	
            		//'FacilityCode'	=>	$data->	
            		//'SIBSCode'	=>	$data->	
            		//'AppvAmt'	=>	$data->	
            		//'AppvTenure'	=>	$data->	
            		//'ApplyDt'	=>	$data->	
            		//'BaseRateCode'	=>	$data->	
            		//'VarianceRate'	=>	$data->	
            		//'VarianceCode'	=>	$data->	
            		//AppvRate	=>	$data->	
            		//FacInstAF	=>	$data->	
            		//FacFinalInst	=>	$data->	
            		'HDRNUM'	=>	$data->HDRNUM,
            		//DSPUserID	=>	$data->	
            		//DSPURL	=>	$data->	
            		//DSPPort	=>	$data->	
            		'DateSend'	=>	$data->DateSend,
            		//DSPAccNoReqId	=>	$data->	
            		'Status'	=>	$data->Status,
            		'ErrorCode'	=>	$data->ErrorCode,
            		'ErrorReason'	=>	$data->ErrorReason,
            		'SeqNo'	=>	$data->SeqNo,
            		'LoanAccNo'	=>	$data->LoanAccNo

            	]);
            }



		$table_name = 'Dsp_Creation_AccNos';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_AccountCreation')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Dsp_Creation_AccNos')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function Dsp_AA_Facilitys(Request $request)
    {

    	ini_set('max_execution_time', 3000000);

    	$today = date("Y-m-d H:i:s");
    	

        $datas = DSP_AAFacility:://take(100)->
            //->where('applkpi.ApplRefNo', 'WAPB0000000005')
            get();
    	

            foreach($datas as $data){
            	
            	DB::connection('sqlsrv2')
            	->table('Dsp_AA_Facilitys')
            	->insert([

            		//'DspAAId'	=>		         		
            		'ApplID'	=>	$data->DSP_AAFacility_to_ApplRefNoTabTemps->ApplId ?? NULL,
            		'UserName'	=>	$data->UserID,
            		//'Role'	=>	$data->	
            		'ScenarioNo'	=>	$data->ScenarioNo,
            		'ActionCode'	=>	$data->ActionCode,
            		'TrnCode'	=>	$data->TransactionCode,
            		'ClientDt'	=>	$data->ClientDt,
            		'ClientTm'	=>	$data->ClientTm,
            		'Branch'	=>	$data->Branch,
            		'MiCOBCifNo'	=>	$data->MiCOBCifNo,
            		//'ApplyDate'	=>	$data->	
            		//'FacilityCode'	=>	$data->	
            		//'SibsCode'	=>	$data->	
            		//'AppvAmt'	=>	$data->	
            		//'ApprovalAuthority'	=>	$data->	
            		//'OfferDt'	=>	$data->	
            		//'FacFinalInst'	=>	$data->	
            		//'FacInstAF'	=>	$data->	
            		//'AcceptDt'	=>	$data->	
            		//'MOffIDNo'	=>	$data->	
            		//'BaseRateCode'	=>	$data->	
            		//'AppvTen'	=>	$data->	
            		//'AGDepartmentCode'	=>	$data->	
            		/*'AGBranchCode'	=>	$data->	
            		'AGPayCenter'	=>	$data->	
            		'AGDeductionCode'	=>	$data->	
            		'AngkasaCode'	=>	$data->	
            		'AGPersonnelNumber'	=>	$data->	
            		'CustProffesion'	=>	$data->	
            		'TypeOfPricing'	=>	$data->	
            		'VarianceRate'	=>	$data->	
            		'VarianceCode'	=>	$data->	
            		'CustomerCeillingRate'	=>	$data->	
            		'AFRATE'	=>	$data->	
            		'WAFRATE'	=>	$data->	
            		'AFDSRA'	=>	$data->	
            		'WAFDSRA'	=>	$data->	*/
            		'HDRNUM'	=>	$data->HDRNUM,
            		/*'DSPUserID'	=>	$data->	
            		'DSPURL'	=>	$data->	
            		'DSPPort'	=>	$data->	
            		'DateReq'	=>	$data->	
            		'DSPAAFacReqId'	=>	$data->	*/
            		'Status'	=>	$data->Status,
            		'ErrorCode'	=>	$data->ErrorCode,
            		'ErrorReason'	=>	$data->ErrorReason,
            		'SeqNo'	=>	$data->SeqNo,
            		'DateReceive'	=>	$data->DateReceive

            	]);
            }



		$table_name = 'Dsp_AA_Facilitys';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_AAFacility')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Dsp_AA_Facilitys')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function Dsp_Islamics(Request $request)
    {

    	ini_set('max_execution_time', 3000000);

    	$today = date("Y-m-d H:i:s");
    	

        $datas = DSP_IslamicFile:://take(100)->
            //->where('applkpi.ApplRefNo', 'WAPB0000000005')
            get();
    	

            foreach($datas as $data){
            	
            	DB::connection('sqlsrv2')
            	->table('Dsp_Islamics')
            	->insert([

            		
            		'ApplID'	=>	$data->DSP_IslamicFile_to_ApplRefNoTabTemps->ApplId ?? NULL,
            		'UserName'	=>	$data->UserID,
            		//'Role'	=>	$data->	
            		'ScenarioNo'	=>	$data->ScenarioNo,
            		'ActionCode'	=>	$data->ActionCode,
            		'TrnCode'	=>	$data->TransactionCode,
            		'ClientDt'	=>	$data->ClientDt,
            		'ClientTm'	=>	$data->ClientTm,
            		'Branch'	=>	$data->Branch,
            		'MiCOBCifNo'	=>	$data->MiCOBCifNo,
            		/*'DSPAAFacSeqNo'	=>	$data->	
            		'FacilityCode'	=>	$data->	
            		'SPTFSellingPrice'	=>	$data->	
            		'CeilingRateAICRAT'	=>	$data->	
            		'CeilingRateWAICRAT'	=>	$data->	
            		'VarianceRate'	=>	$data->	
            		'VarianceCode'	=>	$data->	
            		'BaseRateCode'	=>	$data->	
            		'FacAdInst'	=>	$data->	
            		'SecDepAmt'	=>	$data->	*/
            		'HDRNUM'	=>	$data->HDRNUM,
            		/*'DSPUserID'	=>	$data->	
            		'DSPURL'	=>	$data->	
            		'DSPPort'	=>	$data->	*/
            		'DateSend'	=>	$data->DateSend,
            		//'DSPIsmReqId'	=>	$data->	
            		'Status'	=>	$data->Status,
            		'ErrorCode'	=>	$data->ErrorCode,
            		'ErrorReason'	=>	$data->ErrorReason,
            		'SeqNo'	=>	$data->SeqNo

            	]);
            }



		$table_name = 'Dsp_Islamics';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('DSP_IslamicFile')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('Dsp_Islamics')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    
}
