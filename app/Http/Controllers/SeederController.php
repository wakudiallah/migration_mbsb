<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Ramsey\Uuid\Uuid;


class SeederController extends Controller
{
    
    

    /*public function CodeProducts(Request $request)
    {
    	
		DB::connection('sqlsrv2')
			->table('CodeProducts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ProdCode' => "",
		    	'ProdDesc' => "Null Data"
		    ]);
	
		

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }*/

    public function CodeNextRoutes(Request $request)
    {

    	$getIdRole = DB::connection('sqlsrv2')
    			->table('AspNetRoles')
               ->select('*')
               ->where('Name', 'ADMIN1')
               ->first();


    	DB::connection('sqlsrv2')
			->table('CodeNextRoutes')
		    ->insert([		    	
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Role' => "ADMIN1",
		    	'RouteTo' => "APPR AUTHORITIES",
		    	'RouteToId' => Uuid::uuid4()->tostring(),
		    	'RoleId' => $getIdRole->Id
		    ]);


		$getIdRole = DB::connection('sqlsrv2')
    			->table('AspNetRoles')
               ->select('*')
               ->where('Name', 'ADMIN1')
               ->first();

    	DB::connection('sqlsrv2')
			->table('CodeNextRoutes')
		    ->insert([		    	
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Role' => "ADMIN1",
		    	'RouteTo' => "BRANCH LIAISON",
		    	'RouteToId' => Uuid::uuid4()->tostring(),
		    	'RoleId' => $getIdRole->Id
		    ]);


		$getIdRole = DB::connection('sqlsrv2')
    			->table('AspNetRoles')
               ->select('*')
               ->where('Name', 'ADMIN')
               ->first();

    	DB::connection('sqlsrv2')
			->table('CodeNextRoutes')
		    ->insert([		    	
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Role' => $getIdRole->Name,
		    	'RoleId' => $getIdRole->Id,
		    	'RouteTo' => "ADMIN1",
		    	'RouteToId' => Uuid::uuid4()->tostring()
		    ]);



		$getIdRole = DB::connection('sqlsrv2')
    			->table('AspNetRoles')
               ->select('*')
               ->where('Name', 'DISBURSEMENT MAKER')
               ->first();

    	DB::connection('sqlsrv2')
			->table('CodeNextRoutes')
		    ->insert([		    	
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Role' => $getIdRole->Name,
		    	'RoleId' => $getIdRole->Id,
		    	'RouteTo' => "APPR AUTHORITIES",
		    	'RouteToId' => Uuid::uuid4()->tostring()
		    ]);



		$getIdRole = DB::connection('sqlsrv2')
    			->table('AspNetRoles')
               ->select('*')
               ->where('Name', 'DISBURSEMENT MAKER')
               ->first();

    	DB::connection('sqlsrv2')
			->table('CodeNextRoutes')
		    ->insert([		    	
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Role' => $getIdRole->Name,
		    	'RoleId' => $getIdRole->Id,
		    	'RouteTo' => "ADMINDSU",
		    	'RouteToId' => Uuid::uuid4()->tostring()
		    ]);


		$getIdRole = DB::connection('sqlsrv2')
    			->table('AspNetRoles')
               ->select('*')
               ->where('Name', 'DATA ENTRY')
               ->first();

    	DB::connection('sqlsrv2')
			->table('CodeNextRoutes')
		    ->insert([		    	
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Role' => $getIdRole->Name,
		    	'RoleId' => $getIdRole->Id,
		    	'RouteTo' => "PROCESSOR",
		    	'RouteToId' => Uuid::uuid4()->tostring()
		    ]);



		$getIdRole = DB::connection('sqlsrv2')
    			->table('AspNetRoles')
               ->select('*')
               ->where('Name', 'DATA ENTRY')
               ->first();

    	DB::connection('sqlsrv2')
			->table('CodeNextRoutes')
		    ->insert([		    	
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Role' => $getIdRole->Name,
		    	'RoleId' => $getIdRole->Id,
		    	'RouteTo' => "REVIEWER",
		    	'RouteToId' => Uuid::uuid4()->tostring()
		    ]);


    	return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeTypeOfProducts(Request $request)
    {

    	DB::connection('sqlsrv2')
			->table('CodeTypeOfProducts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ProdTyCode' => "Y",
		    	'ProdTyDesc' => "Biro"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeTypeOfProducts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ProdTyCode' => "S",
		    	'ProdTyDesc' => "AG State"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeTypeOfProducts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ProdTyCode' => "G",
		    	'ProdTyDesc' => "AG Federal"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeTypeOfProducts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ProdTyCode' => "N",
		    	'ProdTyDesc' => "Non Biro"
		    ]);



		DB::connection('sqlsrv2')
			->table('CodeTypeOfProducts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ProdTyCode' => "P",
		    	'ProdTyDesc' => "Private Sector"
		    ]);



		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);

    }



    public function AspNetRoles(Request $request)
    {
    	
    	DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "ADMIN", 
		    	'NormalizedName' =>  "ADMIN", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "ADMIN1", 
		    	'NormalizedName' =>  "ADMIN1", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);



		DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "DISBURSEMENT MAKER", 
		    	'NormalizedName' =>  "DISBURSEMENT MAKER", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()
		    ]);



		DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "APPR AUTHORITIES", 
		    	'NormalizedName' =>  "APPR AUTHORITIES", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "LOAN CONFIRMATION", 
		    	'NormalizedName' =>  "LOAN CONFIRMATION", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()
		    ]);


		 DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(),
		    	'Name'=> "SUPER ADMIN", 
		    	'NormalizedName' =>  "SUPER ADMIN", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(),
		    	'Name'=> "ADMINDSU", 
		    	'NormalizedName' =>  "ADMINDSU", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		 DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "PROCESSOR", 
		    	'NormalizedName' =>  "PROCESSOR", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);



		DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "BRANCH LIAISON", 
		    	'NormalizedName' =>  "BRANCH LIAISON",
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);



		DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "CR REVIEW", 
		    	'NormalizedName' =>  "CR REVIEW", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "ADMIN3", 
		    	'NormalizedName' =>  "ADMIN3", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		 DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "DATA ENTRY",
		    	'NormalizedName' =>  "DATA ENTRY", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);



		    DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "ADMIN2", 
		    	'NormalizedName' =>  "ADMIN2", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()


		    ]);


		    DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "SETTLEMENT UNIT", 
		    	'NormalizedName' =>  "SETTLEMENT UNIT", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		    DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "REVIEWER", 
		    	'NormalizedName' =>  "REVIEWER", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		    DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "DOC CHECKLIST B", 
		    	'NormalizedName' =>  "DOC CHECKLIST B", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()
		    ]);


		    DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "DOC CHECKLIST", 
		    	'NormalizedName' =>  "DOC CHECKLIST", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		    DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "DISBURSEMENT AUTHORIZER", 
		    	'NormalizedName' =>  "DISBURSEMENT AUTHORIZER", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]); 


		    DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "EXTERNAL REDEMPTION", 
		    	'NormalizedName' =>  "EXTERNAL REDEMPTION", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()

		    ]);


		    DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "JOB & SALARY CONFIRMATION", 
		    	'NormalizedName' =>  "JOB & SALARY CONFIRMATION", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()
		    ]);


		    DB::connection('sqlsrv2')
			->table('AspNetRoles')
		    ->insert([
		    	"Id" => Uuid::uuid4()->tostring(), 
		    	'Name'=> "ENQUIRY", 
		    	'NormalizedName' =>  "ENQUIRY", 
		    	'ConcurrencyStamp' => Uuid::uuid4()->tostring()
		    ]);



		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

	public function CodeLoanTenors(Request $request)
    {
    	DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "036M",
		    	'TenorDesc' => "3 Years",
		    	'intTenor' => "36"

		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "072M",
		    	'TenorDesc' => "6 Years",
		    	'intTenor' => "72"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "132M",
		    	'TenorDesc' => "11 Years",
		    	'intTenor' => "132"
		    ]);




		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "228M",
		    	'TenorDesc' => "19 Years",
		    	'intTenor' => "228"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "024M",
		    	'TenorDesc' => "2 Years",
		    	'intTenor' => "24"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "060M",
		    	'TenorDesc' => "5 Years",
		    	'intTenor' => "60"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "240M",
		    	'TenorDesc' => "20 Years",
		    	'intTenor' => "240"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "180M",
		    	'TenorDesc' => "15 Years",
		    	'intTenor' => "180"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "204M",
		    	'TenorDesc' => "17 Years",
		    	'intTenor' => "204"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "096M",
		    	'TenorDesc' => "8 years",
		    	'intTenor' => "96"
		    ]);



		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "156M",
		    	'TenorDesc' => "13 Years",
		    	'intTenor' => "156"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "168M",
		    	'TenorDesc' => "14 Years",
		    	'intTenor' => "168"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "144M",
		    	'TenorDesc' => "12 Years",
		    	'intTenor' => "144"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "012M",
		    	'TenorDesc' => "1 Years",
		    	'intTenor' => "12"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "048M",
		    	'TenorDesc' => "4 Years",
		    	'intTenor' => "48"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "084M",
		    	'TenorDesc' => "7 Years",
		    	'intTenor' => "84"
		    ]);


		 DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "120M",
		    	'TenorDesc' => "10 Years",
		    	'intTenor' => "120"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "108M",
		    	'TenorDesc' => "9 Years",
		    	'intTenor' => "108"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "192M",
		    	'TenorDesc' => "16 Years",
		    	'intTenor' => "192"
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeLoanTenors')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TenorCode' => "216M",
		    	'TenorDesc' => "18 Years",
		    	'intTenor' => "216"
		    ]);



		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    

    public function CodeCollectionTypes(Request $request)
    {
    	
			DB::connection('sqlsrv2')
			->table('CodeCollectionTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ColDesc' => "AGFEDERAL",
		    	'ColCode' => "G"
		    ]);


		    DB::connection('sqlsrv2')
			->table('CodeCollectionTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ColDesc' => "NON BIRO",
		    	'ColCode' => "N"
		    ]);


		    DB::connection('sqlsrv2')
			->table('CodeCollectionTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ColDesc' => "BIRO",
		    	'ColCode' => "Y"
		    ]);


		    DB::connection('sqlsrv2')
			->table('CodeCollectionTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ColDesc' => "PRIVATE",
		    	'ColCode' => "P"
		    ]);


		    DB::connection('sqlsrv2')
			->table('CodeCollectionTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ColDesc' => "AGSTATE",
		    	'ColCode' => "S"
		    ]);
	

			return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeSecuParams(Request $request)
    {
    	
		DB::connection('sqlsrv2')
			->table('CodeSecuParams')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,

		    	"SessionTimeout"  => '60',
		    	"PwdExpDays"  => '60',
		    	"PwdHist"  => '10',
		    	"PwdInactive"  => '60',
		    	"StrongPwd"  => true,
		    	"PwdMinLength"  => '7',
		    	"PwdMaxLength"  => '12',

		    	"FailedTry"  => '3',
		    	"LockedDuration"  => '1000',
		    	"RoundRobin"  => '1'

		    	//"Max_Age","Min_Age",
		    ]);
	
	

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeAngkasaCodes(Request $request)
    {

    	DB::connection('sqlsrv2')
			->table('CodeAngkasaCodes')
		    ->insert([
		    	"AngkasaCode" => "4101",
				"AngKasaCodeDesc" => "4101",
				"Act" => true,
				"CreatedBy" =>	 'Bobonn3',
				"CreatedDate"	=>  date("Y-m-d H:i:s")
		    ]);


		DB::connection('sqlsrv2')
			->table('CodeAngkasaCodes')
		    ->insert([
		    	"AngkasaCode" => "4102",
				"AngKasaCodeDesc" => "4102",
				"Act" => true,
				"CreatedBy" =>	 'Bobonn3',
				"CreatedDate"	=>  date("Y-m-d H:i:s")
		    ]);
		 

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);

    }


    public function sysFrameTabs(Request $request)
    {

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ADMINDASHBOARD",
				"FrameName"=>"ADMINDASHBOARD",
				"FrameDesc"=>"ADMIN DASHBOARD",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ADMINDSUTASKLIST",
				"FrameName"=>"ADMINDSUTASKLIST",
				"FrameDesc"=>"ADMIN DSU TASKLIST PAGE",
				"Area"=>"ADMINDSU"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ADMINDSUVIEWUSERS",
				"FrameName"=>"ADMINDSUVIEWUSERS",
				"FrameDesc"=>"DSU VIEW SYSTEM USER",
				"Area"=>"AdminDSU"
			]);


		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ADMINEDITAPPLICANTDETAILS",
				"FrameName"=>"ADMINEDITAPPLICANTDETAILS",
				"FrameDesc"=>"EDIT APPLICATION DETAILS (ADMIN ROLE)",
				"Area"=>"Admin"
			]);


		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ADMINTASKLIST",
				"FrameName"=>"ADMINTASKLIST",
				"FrameDesc"=>"ADMIN TASK LIST",
				"Area"=>"Admin"
			]);


		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ADMINVIEWLOGIN",
				"FrameName"=>"ADMINVIEWLOGIN",
				"FrameDesc"=>"VIEW LOG IN INFOS",
				"Area"=>"Admin"
			]);


		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ADMINVIEWUSERBYNAME",
				"FrameName"=>"ADMINVIEWUSERBYNAME",
				"FrameDesc"=>"VIEW USER BY NAME",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ADMINVIEWUSERS",
				"FrameName"=>"ADMINVIEWUSERS",
				"FrameDesc"=>"VIEW SYSTEM USER PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"APPLDETAILS",
				"FrameName"=>"APPLDETAILS",
				"FrameDesc"=>"APPLICATION DETAILES PAGE",
				"Area"=>"UserProcess"
			]);


		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"APPLICATIONWORKFLOW",
				"FrameName"=>"APPLICATIONWORKFLOW",
				"FrameDesc"=>"APPLICATION WORKFLOW PAGE",
				"Area"=>"UserProcess"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"APPLSTATUSLIST",
				"FrameName"=>"APPLSTATUSLIST",
				"FrameDesc"=>"APPLICATION STATUS LIST",
				"Area"=>"UserProcess"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"APPLSTATUSUPDATE",
				"FrameName"=>"APPLSTATUSUPDATE",
				"FrameDesc"=>"REMARK & APPLICATION STATUS UPDATE PAGE",
				"Area"=>"UserProcess"
			]);


		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"APPLTASKLIST",
				"FrameName"=>"APPLTASKLIST",
				"FrameDesc"=>"USER TASK LIST",
				"Area"=>"UserProcess"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"APPROVALAUTHORITY",
				"FrameName"=>"APPROVALAUTHORITY",
				"FrameDesc"=>"APPLICATION APPROVAL PAGE",
				"Area"=>"UserProcess"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CAAF",
				"FrameName"=>"CAAF",
				"FrameDesc"=>"CREDIT ASSESSMENT AND APPLICATION FORM",
				"Area"=>"UserProcess"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CALABSENTTAB",
				"FrameName"=>"CALABSENTTAB",
				"FrameDesc"=>"USERS ABSENT REGISTRATION SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CCRISTAXONOMY",
				"FrameName"=>"CCRISTAXONOMY",
				"FrameDesc"=>"CCRISTAXONOMY SETUP PAGE",
				"Area"=>"Admin"
			]);


		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEACADEMIC",
				"FrameName"=>"CODEACADEMIC",
				"FrameDesc"=>"CODE ACADEMIC SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEADDTYPE",
				"FrameName"=>"CODEADDTYPE",
				"FrameDesc"=>"CODE ADDRESS TYPE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEADMINFEE",
				"FrameName"=>"CODEADMINFEE",
				"FrameDesc"=>"CODE ADMIN FEE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEAGBRANCH",
				"FrameName"=>"CODEAGBRANCH",
				"FrameDesc"=>"CODE AG BRANCH SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEAGDEPT",
				"FrameName"=>"CODEAGDEPT",
				"FrameDesc"=>"CODE AG DEPT SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEAGPAYCENTER",
				"FrameName"=>"CODEAGPAYCENTER",
				"FrameDesc"=>"CODE AG PAY CENTER SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEAGSTATE",
				"FrameName"=>"CODEAGSTATE",
				"FrameDesc"=>"CODE AG STATE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEAKADSCRIPT",
				"FrameName"=>"CODEAKADSCRIPT",
				"FrameDesc"=>"CODE AKAD SCRIPT SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEAPPAUTS",
				"FrameName"=>"CODEAPPAUT",
				"FrameDesc"=>"CODE APPROVAL AUTHORITY SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEAPPSTATUS",
				"FrameName"=>"CODEAPPSTATUS",
				"FrameDesc"=>"CODE APPL STATUS SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEARB",
				"FrameName"=>"CODEARB",
				"FrameDesc"=>"CODE AMANAH RAYA AND BANCA SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEBANK",
				"FrameName"=>"CODEBANK",
				"FrameDesc"=>"CODE BANK SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEBANKRUP",
				"FrameName"=>"CODEBANKRUP",
				"FrameDesc"=>"CODE BANKRUPT SETUP CODE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEBENEBANK",
				"FrameName"=>"CODEBENEBANK",
				"FrameDesc"=>"CODE BENEFICIARY BANK SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEBENEFICIARY",
				"FrameName"=>"CODEBENEFICIARY",
				"FrameDesc"=>"CODE BENEFICIARY PAYMENT SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEBRANCH",
				"FrameName"=>"CODEBRANCH",
				"FrameDesc"=>"BRANCH CODE SETUP",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEBUMI",
				"FrameName"=>"CODEBUMI",
				"FrameDesc"=>"CODE BUMI SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODECEILINGRATE",
				"FrameName"=>"CODECEILINGRATE",
				"FrameDesc"=>"CODE CEILING RATE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODECHECKLIST",
				"FrameName"=>"CODECHECKLIST",
				"FrameDesc"=>"CODE DOCUMENT CHECKLIST SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODECLRZONE",
				"FrameName"=>"CODECLRZONE",
				"FrameDesc"=>"CODECLEARING ZONE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODECMSCANCEL",
				"FrameName"=>"CODECMSCANCEL",
				"FrameDesc"=>"CODE APPLICATION REASON SETUP PAGE",
				"Area"=>"Admin"
			]);


		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODECOLLECTIONTYPE",
				"FrameName"=>"CODECOLLECTIONTYPE",
				"FrameDesc"=>"CODE COLLECTION TYPE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODECOOP",
				"FrameName"=>"CODECOOP",
				"FrameDesc"=>"CODE COOPERATIVE SETUP AGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODECOOPBRANCH",
				"FrameName"=>"CODECOOPBRANCH",
				"FrameDesc"=>"CODE COOPERATIVE BANCH SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODECREDITGRADE",
				"FrameName"=>"CODECREDITGRADE",
				"FrameDesc"=>"CODE LOAN CREDIT GRADE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEDCGROUPS",
				"FrameName"=>"CODEDCGROUPS",
				"FrameDesc"=>"DC GROUP SETUP",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEDEALER",
				"FrameName"=>"CODEDEALER",
				"FrameDesc"=>"CODE THIRD PARTY SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEDEALERBRCH",
				"FrameName"=>"CODEDEALERBRCH",
				"FrameDesc"=>"CODE THIRD PARTY BRANCH SETUP PAGE",
				"Area"=>"Admin"
			]);


		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEDELIMET",
				"FrameName"=>"CODEDELIMET",
				"FrameDesc"=>"CODE DELIVERY METHOD SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEDEPARTMENT",
				"FrameName"=>"CODEDEPARTMENT",
				"FrameDesc"=>"CODE DEPARTMENT",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEDEVIATION",
				"FrameName"=>"CODEDEVIATION",
				"FrameDesc"=>"DEVIATION CODE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEDISPATCHTO",
				"FrameName"=>"CODEDISPATHTO",
				"FrameDesc"=>"CODE DISPATCH TO SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEEMPLOYSECTOR",
				"FrameName"=>"CODEEMPLOYSECTOR",
				"FrameDesc"=>"CODE EMPLOY SECTOR SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEEMPTY",
				"FrameName"=>"CODEEMPTY",
				"FrameDesc"=>"CODE EMPLOYMENT TYPE CODE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEENTITY",
				"FrameName"=>"CODEENTITY",
				"FrameDesc"=>"CODE ENTITY SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEGENDER",
				"FrameName"=>"CODEGENDER",
				"FrameDesc"=>"CODE GENDER SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEGL",
				"FrameName"=>"CODEGL",
				"FrameDesc"=>"CODE GL SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEIDTYPE",
				"FrameName"=>"CODEIDTYPE",
				"FrameDesc"=>"CODE ID TYPE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEINS",
				"FrameName"=>"CODEINS",
				"FrameDesc"=>"CODE INSURANCE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEINTPACKAGE",
				"FrameName"=>"CODEINTPACKAGE",
				"FrameDesc"=>"CODE LOAN INTEREST PACKAGE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEISAVE",
				"FrameName"=>"CODEISAVE",
				"FrameDesc"=>"CODE ISAVE / BANCA SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEJOBSTATUS",
				"FrameName"=>"CODEJOBSTATUS",
				"FrameDesc"=>"CODE JOB STATUS SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODELOANPACKAGE",
				"FrameName"=>"CODELOANPACKAGE",
				"FrameDesc"=>"CODE LOAN PACKAGE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODELOANTENOR",
				"FrameName"=>"CODELOANTENOR",
				"FrameDesc"=>"CODE LOAN TENOR SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEMARITAL",
				"FrameName"=>"CODEMARITAL",
				"FrameDesc"=>"CODE MARITAL UPDATE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEMODEOFPAYMENT",
				"FrameName"=>"CODEMODEOFPAYMENT",
				"FrameDesc"=>"CODE MODE OF PAYMENT SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEMOFFICER",
				"FrameName"=>"CODEMOFFICER",
				"FrameDesc"=>"CODE MARKERTING OFFICER SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEMOPREDEMP",
				"FrameName"=>"CODEMOPREDEMP",
				"FrameDesc"=>"REDEMPTION MODE OF PAYMENT SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODENAOFBUSS",
				"FrameName"=>"CODENAOFBUSS",
				"FrameDesc"=>"CODE NATURE OF BUSINESS",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODENEGARA",
				"FrameName"=>"CODENEGARA",
				"FrameDesc"=>"CODE COUNTRY SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODENEGERI",
				"FrameName"=>"CODENEGERI",
				"FrameDesc"=>"CODE STATE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEOCCUPATION",
				"FrameName"=>"CODEOCCUPATION",
				"FrameDesc"=>"CODE OCCUPATION SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEPAYEETYPE",
				"FrameName"=>"CODEPAYEETYPE",
				"FrameDesc"=>"CODE PAYEE TYPE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEPENDINGREAS",
				"FrameName"=>"CODEPENDINGREAS",
				"FrameDesc"=>"CODE PENDING REASON SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEPOSITION",
				"FrameName"=>"CODEPOSITION",
				"FrameDesc"=>"CODE POSITION SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEPOSTCODE",
				"FrameName"=>"CODEPOSTCODE",
				"FrameDesc"=>"CODE POSTCODE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEPRODUCT",
				"FrameName"=>"CODEPRODUCT",
				"FrameDesc"=>"CODE PRODUCT SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODERACE",
				"FrameName"=>"CODERACE",
				"FrameDesc"=>"CODE RACE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEREGION",
				"FrameName"=>"CODEREGION",
				"FrameDesc"=>"CODE REGION SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEREJECTREAS",
				"FrameName"=>"CODEREJECTREAS",
				"FrameDesc"=>"CODE REJECTION REASON SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODERELATION",
				"FrameName"=>"CODERELATION",
				"FrameDesc"=>"CODE RELATION SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODERELIGION",
				"FrameName"=>"CODERELIGION",
				"FrameDesc"=>"CODE RELIGION SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEREMARKS",
				"FrameName"=>"CODEREMARKS",
				"FrameDesc"=>"CODE REMARK SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEREPAYMENTTYPE",
				"FrameName"=>"CODEREPAYMENTTYPE",
				"FrameDesc"=>"CODE REPAYMENT TYPE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODERESIDENCETY",
				"FrameName"=>"CODERESIDENCETY",
				"FrameDesc"=>"CODE DWELLING TYPE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODESECUPARAM",
				"FrameName"=>"CODESECUPARAM",
				"FrameDesc"=>"CODE SECURITY PARAMETER SETUP PAGE",
				"Area"=>"AdminDSU"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODESETTLEPTY",
				"FrameName"=>"CODESETTLEPTY",
				"FrameDesc"=>"CODE SETTLEMENT PARTY SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODESUBMITTYPE",
				"FrameName"=>"CODESUBMITTYPE",
				"FrameDesc"=>"CODE SUBMISSION TYPE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODETITLE",
				"FrameName"=>"CODETITLE",
				"FrameDesc"=>"CODE TITLE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODETRANSACTION",
				"FrameName"=>"CODETRANSACTION",
				"FrameDesc"=>"CODE TRANSACTION SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODETYPEOFPRODUCT",
				"FrameName"=>"CODETYPEOFPRODUCT",
				"FrameDesc"=>"CODE TYPE OF PRODUCT",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEUSERSTAT",
				"FrameName"=>"CODEUSERSTAT",
				"FrameDesc"=>"CODE USER STATUS SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEVARIANCERATE",
				"FrameName"=>"CODEVARIANCERATE",
				"FrameDesc"=>"CODE VARIANCE RATE SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEWAKALAH",
				"FrameName"=>"CODEWAKALAH",
				"FrameDesc"=>"CODE WAKALAH SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"CODEWORKSEC",
				"FrameName"=>"CODEWORKSEC",
				"FrameDesc"=>"WORK SECTOR CODE PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"DISBURSEMENT",
				"FrameName"=>"DISBURSEMENT",
				"FrameDesc"=>"LOAN DISBURSEMENT PAGE",
				"Area"=>"UserProcess"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"DOCUMENTCHECKLIST",
				"FrameName"=>"DOCUMENTCHECKLIST",
				"FrameDesc"=>"DOCUMENT CHECK LIST PAGE",
				"Area"=>"UserProcess"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"DSUUSERREGISTRATIONREQUEST",
				"FrameName"=>"DSUUSERREGISTRATIONREQUEST",
				"FrameDesc"=>"DSU CREATE NEW SYSTEM USER PAGE",
				"Area"=>"AdminDSU"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"INSPREM",
				"FrameName"=>"INSPREM",
				"FrameDesc"=>"INSURANCE PREMIUM SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"LOANCREATIONPAGE",
				"FrameName"=>"LOANCREATIONPAGE",
				"FrameDesc"=>"LOAN CREATION PAGE",
				"Area"=>"UserProcess"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ONLINECHECKSETUP",
				"FrameName"=>"ONLINECHECKSETUP",
				"FrameDesc"=>"ONLINE CHECK SETUP PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"ROLEMANAGER",
				"FrameName"=>"ROLEMANAGER",
				"FrameDesc"=>"GROUP ROLES SETUP",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"SMSNOTIFICATION",
				"FrameName"=>"SMSNOTIFICATION",
				"FrameDesc"=>"SMS NOTIFICATION PAGE",
				"Area"=>"UserProcess"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"SYSFRAMETAB",
				"FrameName"=>"SYSFRAMETAB",
				"FrameDesc"=>"SYSTEM PAGE REGISTRATION PAGE",
				"Area"=>"Admin"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"SYSFRAMETABDSU",
				"FrameName"=>"SYSFRAMETABDSU",
				"FrameDesc"=>"DSU REGISTER NEW PAGE",
				"Area"=>"AdminDSU"
			]);

		DB::connection('sqlsrv2')
			->table('sysFrameTabs')
		    ->insert([
				"Id"=>"VIEWAUDITTRAILS",
				"FrameName"=>"VIEWAUDITTRAILS",
				"FrameDesc"=>"VIEW AUDIT TRAILS",
				"Area"=>"Admin"
			]);

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);

    }

    /*
    public function CCRISApplAUStatuss(Request $request)
    {
    	
		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'P',
		    	"AUStatDesc"  => 'P - Pending'
		    ]);


		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'A',
		    	"AUStatDesc"  => 'A - Accepted'
		    ]);


		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'T',
		    	"AUStatDesc"  => 'T - Approved with Variation'
		    ]);



		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'R',
		    	"AUStatDesc"  => 'R - Rejected'
		    ]);



		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'W',
		    	"AUStatDesc"  => 'W - Cancel Before Approval'
		    ]);



		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'X',
		    	"AUStatDesc"  => 'X - Cancel By Customer'
		    ]);



		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'D',
		    	"AUStatDesc"  => 'D - Deleted By FI'
		    ]);

	

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }
    */


    public function CCRISApplAUStatuss(Request $request)
    {
    	
		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'P',
		    	"AUStatDesc"  => 'P - Pending'
		    ]);


		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'A',
		    	"AUStatDesc"  => 'A - Accepted'
		    ]);


		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'T',
		    	"AUStatDesc"  => 'T - Approved with Variation'
		    ]);



		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'R',
		    	"AUStatDesc"  => 'R - Rejected'
		    ]);



		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'W',
		    	"AUStatDesc"  => 'W - Cancel Before Approval'
		    ]);



		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'X',
		    	"AUStatDesc"  => 'X - Cancel By Customer'
		    ]);



		DB::connection('sqlsrv2')
			->table('CCRISApplAUStatuss')
		    ->insert([
		    	//'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	"AUStatCode"  => 'D',
		    	"AUStatDesc"  => 'D - Deleted By FI'
		    ]);

	

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }




}
