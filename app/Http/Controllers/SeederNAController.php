<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Ramsey\Uuid\Uuid;

class SeederNAController extends Controller
{
    

    public function NA(Request $request)
    {   

        $today = date("Y-m-d H:i:s");

        DB::connection('sqlsrv2')
            ->table('CodeAcademics')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'AcadCode' => "NA",
                'AcadDesc' => "NOT APPLICABLE"
            ]);




        DB::connection('sqlsrv2')
            ->table('CodeAddTypes')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' => $today,
                'Act' => true,
                'AddTyCode' => '0',
                'AddTyDesc' => "NOT APPLICABLE"
            ]);




        DB::connection('sqlsrv2')
            ->table('CodeAdminFees')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'AdminFeeCode' => 'NA',
                'AdminFeeAmt' => '0.00',
                'AdminFeedesc' => 'NOT APPLICABLE',
                'BenePaymentDesc' => 'NA',
                'AdminFeeGLCode' => 'NA',
                'GSTRate' => '0.00',
                'GSTTaxCode' => 'NA',
                'GSTChgTy' => ''
            ]);




        DB::connection('sqlsrv2')
            ->table('CodeAGBranches')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' => $today,
                'Act' => true,
                'AGBranchCode' => '0000',
                'AGBranchDesc' => 'NOT APPLICABLE',
                'AGBranchStatus' => true
            ]);




        DB::connection('sqlsrv2')
            ->table('CodeAGDepts')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' => $today,
                'Act' => true,
                'AGDeptCode' => '0000',
                'AGDeptDesc' => 'NOT APPLICABLE',
                'AGDeptStatus' => '0'
            ]);




        DB::connection('sqlsrv2')
            ->table('CodeAGStates')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'AGStateCode' => 'X',
                'AGStateDesc' => 'NOT APPLICABLE'
            ]);




        DB::connection('sqlsrv2')
            ->table('CodeAkadScripts')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,                  
                'AkadCode' => '0',
                'AkadDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeAppAuts')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'AppvAutCode' => 'NA',
                'AppvAutDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeAppStatuss')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'ApplStatCode' => 'AAAA',
                'ApplStatDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeARBs')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'ARBCode' => '00',       
                "ARBType"=> 'Z',
                "ARBDesc"=> 'NOT APPLICABLE',
                "ARBAmt"=> '0.00',
                "ARBCommAmt"=> '0.00',
                "ARBTotal"=> '0.00',
                "ARBGLCode"=> '421230',
                "ARBPaymentDesc"=> 'NOT APPLICABLE',
                "ARBCommGLCode"=> '434063',
                "ARBCommPaymentDesc"=> 'NOT APPLICABLE',
                "Banca2Code"=> 'ARB',
                "GSTRate"=> '0.00',
                "GSTTaxCode"=> 'SR',
                "GSTChgTy"=> ''

            ]);



        DB::connection('sqlsrv2')
            ->table('CodeBankrups')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'BankrupCode' => '00',
                'BankrupDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeBanks')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' => $today,
                'Act' => true,
                'BankCode' => 'NA',
                'BankDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeBeneBank')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'BeneBankCode' => 'AAA',
                'BeneBankBame' => 'NOT APPLICABLE',
                'Giro' => 'PROPAY_GIRO',
                'Rentas' => 'PROPAY_RTS',
                'RentasI' => 'NOT SUPPORTED',
                'CashierOrder' => 'PROPAY_BC',
                'InternalFundTransfer' => 'PROPAY_IFT',
                'RentasCode' => ''

            ]);




        DB::connection('sqlsrv2')
            ->table('CodeBumis')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'BumiCode' => 'A',
                'BumiDesc' => 'NOT APPLICABLE',
                'BumiBNMCode' => 'A'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeCeilingRates')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'CeilingCode' => '0',
                'CeilingRate' => '0.0000',
                'CeilingDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeClrZones')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'ClrZoneCode' => 'NA',
                'ClrZoneDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeCmsCancels')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'CMSCancelCode' => '0000',
                'CMSCancelDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeConvertJobPosts')
            ->insert([
                
                'JobDsnCode' => 'NOT APPLICABLE',
                'PostCode' => 'Z'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeCoop')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'Addr1' => '',
                'Addr2' => '',
                'Addr3' => '',
                'Postcode' => '',
                'City' => '',
                'State' => '',
                'CoopCode' => '000',
                'CoopName' => 'NOT APPLICABLE',
                'CoopTel1' => '',
                'CoopTel2' => '',
                'CoopFax' => '',
                'CoopEmail' => '',

                'CoopProfMargin' => '6.0000',
                'CoopProcessFee' => '6.0000',
                'CoopMshipFee' => '7.0000',
                'CoopMiscFee' => '7.0000',

                'CoopLegalFee' => '6.0000',
                'Shn' => '6.0000',
                'Shnm' => '6.0000',
                'ShSol' => '',

                'Deferment' => '',
                'Fd' => '2.0000',
                'CoopPer' => '2.0000',
                'CoopRegNo' => '',

                'CoopAcNo' => '',
                'CoopBankAcNo' => '',
                'CoopCMSMop' => '',
                'CoopNameCMS' => ''
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeCreditGrades')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'CreditCode' => '00',
                'CreditDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeDCGroups')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'DcGpAct' => true,
                'DCGpCode' => '00',
                'DCGPDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeDealerBrchs')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'DBCode' => '000',
                'DBDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeDealers')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'DealerCode' => '0000',
                'DealerDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeDelimets')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'DelimatCode' => 'Z',
                'DelimetDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('codeDepartments')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'DeptCode' => '0000',
                'DeptDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeDispatchTos')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'DispToCode' => 'AA',
                'DispToDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeEmploySector')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'EmpSecCode' => '88888',
                'EmpSecDesc' => 'NOT APPLICABLE',
                'BNMCode' => '88888'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeEmpTys')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'EmpTyCode' => '000',
                'EmpTyDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('codeEntities')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'EntCode' => 'XXX',
                'EntDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeGenders')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'GenderCode' => 'A',
                'GenderDesc' => 'NOT APPLICABLE',
                'BNMCode' => ''
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeIDTypes')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'IDTypeCode' => 'AA',
                'IDTypeDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeInss')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'InsCode' => 'A',
                'InsDesc' => 'NOT APPLICABLE',
                'DisbDesc' => '00'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeISaves')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                "ISaveCode" => '00',
                "ISaveAmt" => '0.00',
                "ISaveDesc" => 'NOT APPLICABLE',
                "BenePaymentDesc" => 'NOT APPLICABLE',
                "BancaCode" => 'HLG',
                "GSTRate"=> '0.00',
                "GSTTaxCode" => 'PTS',
                "GSTChgTy" => ''

            ]);


        DB::connection('sqlsrv2')
            ->table('CodeJobStatuss')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => $status,
                'JStatCode' => 'NA',
                'JStatDesc' => 'NOT APPLICABLE',
                'BNMCode' =>'0'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeMaritals')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'MarCode' => 'A',
                'MarDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeModeOfPayments')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'MopCode' => 'NA',
                'MopDesc' => 'NOT APPLICABLE',
                'MopMinAmt' => '0.01',
                'MopMaxAmt' => '0.00',
                'MopBankAccNo' => '000',
                'MopColName' => '',
                'MopColIC' => ''
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeNaOfBusss')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'NOBCode' => '0000',
                'NOBDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeNegaras')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'CntryCode' => 'XX',
                'CntryDesc' => 'NOT APPLICABLE'
            ]);


        /*DB::connection('sqlsrv2')
            ->table('CodeNegeris')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'CityCode' => '1',
                'CityDesc' => 'NOT APPLICABLE',
                'BNMCode' => $data->BNMCODE
            ]);
        */

        DB::connection('sqlsrv2')
            ->table('CodeOccupations')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'OccuCode' => 'AAA',
                'OccuDesc' => 'NOT APPLICABLE',
                'BNMCode' => ''
            ]);



        DB::connection('sqlsrv2')
            ->table('CodePayeeTypes')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'PayTyCode' => '00',
                'PayTyDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodePendingReass')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'PenReasCode' => 'AA',
                'PenReasDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodePositions')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'PostCode' => 'X',
                'PostDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodePostCodes')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'Street' => 'NOT APPLICABLE',
                'PostCode' =>'00000',
                'PostOffice' => 'NOT APPLICABLE',
                'State' => '1'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeProcess')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Activate' => 'Y',
                'ProcCode' => '0',
                'ProcLevel' => '0',
                'ProcTime' => '1440',
                'ProcDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeProducts')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'ProdCode' => 'A',
                'ProdDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeRaces')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'RaceCode' => 'AAA',
                'RaceDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeRegions')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'RegionCode' => 'AA',
                'RegionDesc' => 'NOT APPLICABLE'
            ]);



        /*DB::connection('sqlsrv2')
            ->table('CodeRejectReass')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'RejReasCode' =>' 00',
                'RegionDesc' => 'NOT APPLICABLE',
                'SMSFlag' => true,
                'BNMRejCode' => '1',
                'BNMStatus' => 'R'
            ]);*/



        DB::connection('sqlsrv2')
            ->table('CodeRelations')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'RelationCode' => '00',
                'RelationDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeReligions')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'RelCode' => '0',
                'RelDesc' =>'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeRemarkss')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'RemarkCode' => '00',
                'RemarkDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeRepaymentTypes')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'RepayCode' => 'AA',
                'Repaydesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeResidencetys')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'RestyCode' => '0',
                'RestyDesc' => 'NOT APPLICABLE'
            ]);



        /*DB::connection('sqlsrv2')
            ->table('CodeSettlePtys')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                
                'SetPtyCode' => $data->SetPtyCode,
                'SetPtyType' => $data->SetPtyType,
                'SetPtyAbbv' => $data->SetPtyAbbrv,
                'SetptyDesc' => $data->SetPtyDesc,
                'SetPtyBank_Name' => $data->codesettlepty_to_CodeBeneBank->Id ?? NULL,
                'SetPtyRedemptCode' => $data->codesettlepty_to_CodeMopRedemps->Id ?? NULL,
                'SetPtybene_Name' => $data->SetPtybene_Name,
                'SetPtyPaymentDesc' => $data->SetPtyPaymentDesc,
                'SetPtyMBSMOP_Code' => $data->codesettlepty_to_CodeTransaction->Id ?? NULL,
                'SetPtyMBSGL_Code' => $data->codesettlepty_to_CodeGLs->Id ?? NULL,
                'SetPtyCMSMOP_Code' => $data->codesettlepty_to_CodeModeOfPayments->Id ?? NULL,
                'Addr1' => $data->SetPtyAdd1,
                'Addr2' => $data->SetPtyAdd2,
                'Addr3' => $data->SetPtyAdd3,
                'SetRegNo' => $data->SetRegNo,
                'SetPtyBankAcc' => $data->SetPtyBankAcc*/ 


        DB::connection('sqlsrv2')
            ->table('CodeSubmitTypes')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'SubTyCode' => 'AAA',
                'SubTyDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeTitles')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'TtlCode' => 'AA',
                'TtlDesc' => 'NOT APPLICABLE'
            ]);



        DB::connection('sqlsrv2')
            ->table('CodeTransactions')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'TransCode' => 'AAA',
                'TransDesc' => 'NOT APPLICABLE'
            ]);


        DB::connection('sqlsrv2')
            ->table('CodeUserStats')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'StatCode' => '0',
                'StatDesc' => 'NOT APPLICABLE'
            ]);


        /*DB::connection('sqlsrv2')
            ->table('CodeVarianceRates')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'Code' => '0',
                'VarianceCode' => '+',
                'VarianceRate' => '0',
                'VarianceDesc' => '0'
            ]);*/



        DB::connection('sqlsrv2')
            ->table('CodeWorkSecs')
            ->insert([
                'Id' => Uuid::uuid4()->tostring(), 
                'CreatedBy'=> 'Bobonn3', 
                'CreatedDate' =>  date("Y-m-d H:i:s"),
                'Act' => true,
                'WorkSecCode' => '0',
                'WorkSecDesc' => 'NOT APPLICABLE'
            ]);



        return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);


    }



}
