<?php

namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use enableForeignKeyCheck;
use disableForeignKeyConstraints;
use Schema;
use DB;
use Response;


use App\Models\Table2\AspNetUserRoles;


class UserController extends Controller
{
    

    public function user(Request $request)
    {
        $id1 = Uuid::uuid4()->tostring();
        //$id2 = Uuid::uuid4()->tostring();

        DB::connection('sqlsrv2')
            ->table('AspNetUsers')
            ->insert([              
                  "Id"=> $id1,  
                  "UserName"=> 'admindsu2',
                  "NormalizedUserName"=> 'ADMINDSU2',
                  "Email"=> '',
                  "NormalizedEmail"=> '',
                  "EmailConfirmed"=> '0',
                  "PasswordHash"=> "AQAAAAEAACcQAAAAEApSZ6Qn1942gVVEOtedSV/BnH7v93E+M0fHMkJABYrqAvj6Pjn+Y4bIw3vXrJbsAA==",
                  "SecurityStamp"=> "CNFWGJW5NMU6XNRDH4P2Z2IEOYSKHV7I", 
                 "ConcurrencyStamp"=> "6cfeace1-4690-4b88-8691-a8988e7cc5ce",
                  "PhoneNumber"=> "",
                  "PhoneNumberConfirmed"=> "0",
                  "TwoFactorEnabled"=> "0",
                 "LockoutEnd"=> "2021-06-29 08:55:45",
                  "LockoutEnabled"=> "1",
                  "AccessFailedCount"=> "0",
                 "Name"=> "zita6",
                 "UserGroupCode"=> "",
                 "UserDeviation"=> "0",
                  "UserApprovalRight"=> "0",
                 "UserMinAppAmt"=> "0.00",
                 "UserAppMaxAmt"=> "0.00",
                "UserAppPassowrd"=> "",
                  "PasswordCreateDate"=> "2021-06-14 08:55:45",
                 "RouteFlag"=> "0",
                 "RouteDate"=> "0001-01-01 00:00:00",
                 "RenewPassword"=> "Y",
                  "Activate"=> "1",
                // "BrCode"=> "5880723d-05c7-4cf1-b81a-60b1b3ba5e1d",
                  "CSUFlag"=> "0",
                  "DCGrpCode"=> "",
                 "SIBSCode"=> "",
                 "DualSign"=> "0",
                 "SuprOvrId"=> "",
                  "UserIdCreatedDate"=> "2021-04-14 10:42:10",
                 "UserIdCreatedBy"=> "itaadmin",
                    "UserStaffId"=> "",
                  "Salt"=> "UZfk2GYhBXhl8Q==",
                  "StafDept"=> "IT",
                 "StaffId"=> "A126",
                 "userRole"=> "ADMIN,ADMINDSU",
                 "CodeProcessId"=> ""
            ]);


            $get_admin = AspNetRoles::where('Name', 'ADMIN')->first();
            $get_admin_dsu = AspNetRoles::where('Name', 'ADMINDSU')->first();

            DB::connection('sqlsrv2')
            ->table('AspNetUserRoles')
            ->insert([         
                  "UserId"=> $id1,
                  "RoleId"=> $get_admin->Id
                        
            ]);


            DB::connection('sqlsrv2')
            ->table('AspNetUserRoles')
            ->insert([    
                  "UserId"=> $id1,
                  "RoleId"=> $get_admin_dsu->Id
                        
            ]);


  

    }
}
