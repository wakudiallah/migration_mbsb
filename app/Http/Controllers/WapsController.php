<?php

namespace App\Http\Controllers;
use DB;
use Response;
use App\Models\Table1\codeAcademic;
use App\Models\Table1\codegl;
use App\Models\Table1\codechecklist;
use App\Models\Table1\codeloanpackage;
use App\Models\Table1\codebeneficiary;
use App\Models\Table1\CodeAGPayCenter;
use App\Models\Table1\codecoopbranch;
use App\Models\Table1\codemofficer;
use App\Models\Table1\codeintpackage;
use App\Models\Table1\codemopredemp;
use App\Models\Table1\codesettlepty;
use App\Models\Table1\CodeWakalah;
use App\Models\Table1\codebranch;
use App\Models\Table1\insprem;
use App\Models\Table1\Online_Check_Setup;
use App\Models\Table1\CCRISTaxonomy;
use App\Models\Table1\CodeAcademics;
use App\Models\Table1\usertab;



use App\Models\Table2\CodeTransaction;
use App\Models\Table2\CodeCheckLists;
use App\Models\Table2\CodeClrZones;
use App\Models\Table2\AspNetRoles;




use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use enableForeignKeyCheck;
use disableForeignKeyConstraints;
use Schema;

use RealRashid\SweetAlert\Facades\Alert;


class WapsController extends Controller
{
    
    public function __construct()
    {
        
        $this->db_origin = config('setup.DB_origin');
        $this->db_dest = config('setup.DB_dest');
        $this->get_years_setting = config('setup.get_years_setting');
        $this->limit_100 = config('setup.limit_100');
        $this->min_two_month = config('setup.min_two_month');
        $this->today = date("Y-m-d H:i:s");
        

    }





    public function alert()
    {

    	$table_name = 'CodeAcademics';

    	alert()->success($table_name,'Data Migration Successfully');

    	
    	return redirect('/waps')->with(['warning' => 'Chassis has been searched before']);
    }





    public function index()
    {

    	$tables = DB::connection('sqlsrv2')->getDoctrineSchemaManager()->listTableNames();

    	$table1 = DB::connection('sqlsrv')
               ->table('INFORMATION_SCHEMA.TABLES')
               ->select('*')
               ->OrderBy('TABLE_NAME', 'ASC')
               ->get();

        $table2 = DB::connection('sqlsrv2')
               ->table('INFORMATION_SCHEMA.TABLES')
               ->OrderBy('TABLE_NAME', 'ASC')
               ->select('*')
               ->get();

        $database1 = DB::connection('sqlsrv')->getDatabaseName();
        $database2 = DB::connection('sqlsrv2')->getDatabaseName();
        $database3 = DB::connection('sqlsrv3')->getDatabaseName();

   

    	return view('waps/waps', compact('tables', 'table1', 'database1', 'database2', 'table2', 'database3'));
    }


    public function migration_status()
    {
    	$datas = DB::connection('sqlsrv3')
    			->table('migrated_statuses')
               ->select('*')
               ->get();

    	return view('waps/migration', compact('datas'));
    }


    public function update_faname()
    {
    	
        $datas = DB::connection('sqlsrv2')
               ->table('INFORMATION_SCHEMA.TABLES')
               ->select('*')
               ->whereIn('TABLE_NAME', ['ApplRefNoTabTemps'])
               //->OrderBy('TABLE_NAME', 'ASC')
               ->get();

    	return view('waps/update_faname', compact('datas'));
    }


    public function migrate_user()
    {

        $datas = DB::connection('sqlsrv2')
               ->table('INFORMATION_SCHEMA.TABLES')
               ->select('*')
               //->whereIn('TABLE_NAME', ['AspNetRoles','AspNetUserClaims','AspNetUserLogins','AspNetUserRoles','AspNetUsers','AspNetUserTokens'])
               ->whereIn('TABLE_NAME', ['AspNetUsers'])
               //->OrderBy('TABLE_NAME', 'ASC')
               ->get();


    	return view('waps/migrate_user', compact('datas'));
    }


    public function migrate_seeder()
    {

        $datas = DB::connection('sqlsrv2')
               ->table('INFORMATION_SCHEMA.TABLES')
               ->select('*')
               //->whereIn('TABLE_NAME', ['AspNetRoles','AspNetUserClaims','AspNetUserLogins','AspNetUserRoles','AspNetUsers','AspNetUserTokens'])
               ->whereIn('TABLE_NAME', [
	               	'CodeNextRoutes',
					'CodeTypeOfProducts',
					'AspNetRoles',
					'CodeLoanTenors',
					'CodeCollectionTypes',
					'CodeSecuParams',
					'CodeAngkasaCodes',
					'sysFrameTabs',
					'CCRISApplAUStatuss'
               ])
               //->OrderBy('TABLE_NAME', 'ASC')
               ->get();


    	return view('waps/migrate_seeder', compact('datas'));
    }


    public function migrate_parameter()
    {

        $datas = DB::connection('sqlsrv2')
               ->table('INFORMATION_SCHEMA.TABLES')
               ->select('*')
               //->whereIn('TABLE_NAME', ['AspNetRoles','AspNetUserClaims','AspNetUserLogins','AspNetUserRoles','AspNetUsers','AspNetUserTokens'])
               ->whereIn('TABLE_NAME', [
               	
		               	'CodeAcademics',
						'CodeAddTypes',
						'CodeAdminFees',
						'CodeAGBranches',
						'CodeAGDepts',
						'CodeAGPayCenters',
						'CodeBanks',
						'CodeAkadScripts',
						'CodeAppAuts',
						'CodeAppStatuss',
						'CodeBankrups',
						'CodeClrZones',
						'CodeCreditGrades',
						'CodeDCGroups',
						'CodeDelimets',
						'codeDepartments',
						'codeDeviations',
						'CodeDispatchTos',
						'CodeEmpTys',
						'CodeInss',
						'CodeMaritals',
						'CodeNaOfBusss',
						'CodeIDTypes',
						'CodeNegaras',
						'CodeNegeris',
						'CodeOccupations',
						'CodePayeeTypes',
						'CodePendingReass',
						'CodeProcess',
						'CodeProducts',
						'CodeRaces',
						'CodeRegions',
						'CodeRelations',
						'CodeReligions',
						'CodeRemarkss',
						'CodeRepaymentTypes',
						'CodeResidencetys',
						'CodeSubmitTypes',
						'CodeTitles',
						'CodeTransactions',
						'CodeUserStats',
						'CodeWorkSecs',
						'CodePositions',
						'CodeGLs',
						'CodeGenders',
						'codeEntities',
						'CodeEmploySector',
						'CodeBumis',
						'CodeAGStates',
						'CodePostCodes',
						'CodeJobStatuss',
						'CodeModeOfPayments',
						'CodeMOfficers',
						'CodeCmsCancels',
						'CodeConvertJobPosts',
						'CodeCeilingRates',
						'CodeCheckLists',
						'CodeRejectReass',
						'CodeVarianceRates',
						'CodeBranches',
						'CodeCoopBranchs',
						'CodeCoop',
						'CodeSettlePtys',
						'CodeBeneBank',
						'CodeBeneficiarys',
						'CodeIntPackages',
						'CodeDealers',
						'CodeDealerBrchs',
						'CodeARBs',
						'CodeCollectionTypes',
						'CodeISaves',
						'CodeLoanPackages',
						'CodeMopRedemps',
						'CodeWakalahs',
						'InsPrems',
						'OnlineCheckSetups',
						'CCRISTaxonomys'

               ])
               //->OrderBy('TABLE_NAME', 'ASC')
               ->get();


    	return view('waps/migrate_parameter', compact('datas'));
    }


    public function migrate_process()
    {

        $datas = DB::connection('sqlsrv2')
               ->table('INFORMATION_SCHEMA.TABLES')
               ->select('*')
               //->whereIn('TABLE_NAME', ['AspNetRoles','AspNetUserClaims','AspNetUserLogins','AspNetUserRoles','AspNetUsers','AspNetUserTokens'])
               ->whereIn('TABLE_NAME', [

					'ApplApproves',
					'ApplCheckLists',
					'ApplWorkFlows',
					'ApplDisburses',
					'ApplKpis',
					'CaafRedemptions',
					'CaafFacilities',
					'Cifs',			
					'Blas',
					'CaafTabs',
					'ApplRefNoTabTemps',		
					'ApplRefNoTabTempsNew',
					'wapsijal',
					'ApplRefNoTabs',
					'ApplWorkFlows',
					'Addresses',
					'CaafUnsExposures',
					'CalAbsentTabs',
					'DSP_CifDetailGSTs',
					'DSP_CifDetailGST_1',
					'Dsp_CifMastDetails',
					'Dsp_CifMastDetail_1',
					'DSP_EmpDetails',
					'DSP_EmpDetail_1',
					'Dsp_GetCifNos',
					'DSP_GetCifNo_1',
					'Guarantors',		
					'SumCurrentApplStats',
					'UserTaskLists',
					'SubmissionTypes',
					'ISms_Incomp',
					'CMSCaafRedemptions',
					'Dsp_Creation_AccNos',
					'Dsp_AA_Facilitys',
					'Dsp_Islamics'


               ])
               //->OrderBy('TABLE_NAME', 'ASC')
               ->get();


    	return view('waps/migrate_process', compact('datas'));
    }

    


    public function CodeCollectionTypes()
    {
    	dd('Use Seeder');
    }




    public function CCRISTaxonomys(Request $request)
    {

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = CCRISTaxonomy::take(3000)
               ->get();
    	

		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('CCRISTaxonomys')
		    ->insert([
		    	
				"Id"  => Uuid::uuid4()->tostring(),
				"CreatedBy" => "Bobonn3",
				"CreatedDate" => date("Y-m-d H:i:s"),
				"Act" => true,

				"AppType"	=> $data->AppType,
				"FacilityType"	=> $data->FacilityType,
				"PurposeOfLoan"	=> $data->PurposeofLoan,
				"SpecialFund"	=> $data->SpecialFund,
				"DPIFinance"	=> $data->DPIFinance,
				"Syndicated"	=> $data->Syndicated,
				"Location"		=> $data->Location,
				"FInConcept"		=> $data->FinConcept,
				"PrioritySector"	=> $data->PrioritySector,
				"MemberBankRakyat"	=> $data->MemberBankRakyat,
				"CorporateStatus"	=> $data->CorporateStatus,
				"IndustrialSector"	=> $data->IndustrialSector,
				"EntityType"	=> $data->EntityType,
				"AppicationStatus"	=> $data->ApplicationStatus,
				"FICode"	=> $data->FICode,
				"ProdId"	=> $data->ProdID,
				"NewCust"	=> $data->NewCust,
				"Registered"	=> $data->Registered,
				"SMEFinance"	=> $data->SMEFinance,
				"AssetPurchase"	 => $data->AssetPurchase
	
		    ]);
		}



		$table_name = 'CCRISTaxonomys';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CCRISTaxonomy')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CCRISTaxonomys')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    


    public function CodeAcademics(Request $request)
    {
    	$today = date("Y-m-d H:i:s");

    	$start = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeAcademic')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeAcademics')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'AcadCode' => $data->AcadCode,
		    	'AcadDesc' => $data->AcadDesc
		    ]);
		}


		$table_name = 'CodeAcademics';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeAcademic')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAcademics')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status,
				    	'created_at' => $start,
				    	'updated_at' => date("Y-m-d H:i:s")
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function OnlineCheckSetups(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = Online_Check_Setup::get();
    	
		foreach($datas as $data){

			if($data->Active == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('OnlineCheckSetups')
		    ->insert([
		    	
				"Id" =>	Uuid::uuid4()->tostring(),
				"CreatedBy" =>	'Bobonn3',
				"CreatedDate" => date("Y-m-d H:i:s"),	
				"Act" =>true,	
				"XML_LOADPGM" =>$data->XML_LOADPGM,
				"User_ID" =>$data->User_ID,
				"User_Group" =>$data->User_Group,
				"Version_No" =>$data->Version_No,
				"Prod_ID" =>$data->Prod_ID,
				"Rpt_Type_1" =>$data->Rpt_Type_1,
				"Rpt_Type_2" =>$data->Rpt_Type_2,
				"Rpt_Type_3" =>$data->Rpt_Type_3,
				"Limit" =>$data->Limit,
				"Date" =>$data->Date_Time,
				"UsrID" =>$data->UsrID,
				"CCRIS_URL" =>$data->CCRIS_URL,
				"CCRIS_Port" =>$data->CCRIS_Port,
				"CTOS_URL" =>$data->CTOS_URL,
				"BIS_URL" =>$data->BIS_URL,
				"NameSpace" =>$data->NameSpace,
				"Usr_Pwd" =>$data->Usr_Pwd,
				"SMSPeriod" =>$data->SMSPeriod,
				"ASSIDQ_URL" =>$data->ASSIDQ_URL,
				"DSP_URL" =>$data->DSP_URL,
				"DSP_Port" =>$data->DSP_Port

		    ]);
		}



		$table_name = 'OnlineCheckSetups';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('Online_Check_Setup')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('OnlineCheckSetups')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function InsPrems(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = insprem::get();
    	
		foreach($datas as $data){

			if($data->Active == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('InsPrems')
		    ->insert([
		    	
		    	"Id"	=> Uuid::uuid4()->tostring(),	
				"CreatedBy"=>'Bobonn3',	
				"CreatedDate"=> date("Y-m-d H:i:s"),
				"Act"=>true,	
				"LoanAmt" =>$data->LoanAmt,
				"LoanAmtMax"=>  $data->LoanAmtMax,
				"PremAmt" =>  $data->PremAmt,
				"Tenor"=>   $data->Tenor


		    ]);
		}



		$table_name = 'InsPrems';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('insprem')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('InsPrems')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeWakalahs(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = CodeWakalah::get();
    	
		foreach($datas as $data){

			if($data->Active == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeWakalahs')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	"WakalahCode" => $data->WakalahCode,
		    	"WakalahAmt" => $data->WakalahAmt,
		    	"WakalahDesc" => $data->WakalahDesc,
		    	"WakalahGLCode" => "HLG",
		    	"GSTRate" => $data->GSTRate,
		    	"GSTTaxCode" => $data->GSTTaxCode,
		    	"GSTChgTy" => $data->GSTChgTy,
		    	"BenePaymentDesc" => "I/S HONG LEONG MSIG TAKAFUL BERHAD"

		    ]);
		}



		$table_name = 'CodeWakalahs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeWakalah')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeWakalahs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


	public function CodeMopRedemps(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = codemopredemp::get();
    	
		foreach($datas as $data){

			if($data->RmopAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeMopRedemps')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,

		    	"RMopCode"  => $data->Rmopcode,
		    	"RMopCMS"  => $data->codemopredemp_to_CodeModeOfPayments->Id ?? NULL,
		    	"RMopDesc" => $data->RmopDesc,
		    	"RMopClrZone" => $data->codemopredemp_to_CodeClrZones->Id ?? NULL,
		    	"RMopDispTo" => $data->RMopDispTo,
		    	"RMp[DispTo" => $data->codemopredemp_to_CodeDispatchTos->Id ?? NULL,
		    	"RMopDeliMet" => $data->codemopredemp_to_CodeDelimets->Id ?? NULL

		    ]);
		}


		$table_name = 'CodeMopRedemps';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codemopredemp')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeMopRedemps')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }
    

    public function CodeLoanPackages(Request $request)
    {	

    	

    	$today = date("Y-m-d H:i:s");

    	// Disable foreign key checks!
		//DB::statement('NOCHECK Constraint All;');

		//DB::disableForeignKeyCheck();

		//shell_exec("NOCHECK Constraint All;");

		//\DB::statement('SET FOREIGN_KEY_CHECKS == 0;');


        $datas = codeloanpackage::get();

        /*$datas = codeloanpackage::Join('CodeProducts', 'CodeProducts.ProdCode', '=', 'codeloanpackage.LnProdCode')
        	->get();*/


		foreach($datas as $data){

			if($data->LnPackACT == 1){
				$status = true;
			}
			else{
				$status = false;
			}


			/*if($data->codeloanpackage_to_CodeProducts->Id != "NULL"){
				$LnProdCode = $data->codeloanpackage_to_CodeProducts->Id;
			}
			else{
				$LnProdCode = "";
			}*/


			if($data->MultiRate == "Y"){

				$MultiRate = 1;
			}else{
				$MultiRate = 0;

			}


			DB::connection('sqlsrv2')
			->table('CodeLoanPackages')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,

		    	"LnPackCode" => $data->LnPackCode,
		    	"LnPackDesc" => $data->LnPackDesc,
		    	"FacilityCode" => $data->FacilityCode,
		    	"BiroIndc" => $data->BiroIndc,
		    	"SIBSCode2" => $data->SIBSCode2,
		    	"MultiRate" => $MultiRate,
		    	"SIBSCode" => $data->SIBSCode,
		    	"LnProdCode" => $data->codeloanpackage_to_CodeProducts->Id ?? NULL


		    ]);
		}

			// Enable foreign key checks!
			//DB::statement('SET FOREIGN_KEY_CHECKS=1;');



		$table_name = 'CodeLoanPackages';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeloanpackage')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeLoanPackages')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeISaves(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodeISave')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->Active == 'Y'){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeISaves')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	"ISaveCode" => $data->ISaveCode,
		    	"ISaveAmt" => $data->ISaveAmt,
		    	"ISaveDesc" => $data->ISaveDesc,
		    	"BenePaymentDesc" => $data->BenePaymentDesc,
		    	"BancaCode" => $data->BancaCode,
		    	"GSTRate"=> $data->GSTRate,
		    	"GSTTaxCode" => $data->GSTTaxCode,
		    	"GSTChgTy" => $data->GSTChgTy

		    ]);
		}


		$table_name = 'CodeISaves';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeISave')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeISaves')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeARBs(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodeARB')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->Active == 'Y'){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeARBs')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ARBCode' => $data->ARBCode,	   
				"ARBType"=> $data->ARBType,
				"ARBDesc"=> $data->ARBDesc,
				"ARBAmt"=> $data->ARBAmt,
				"ARBCommAmt"=> $data->ARBCommAmt,
				"ARBTotal"=> $data->ARBTotal,
				"ARBGLCode"=> $data->ARBGLCode,
				"ARBPaymentDesc"=> $data->ARBPaymentDesc,
				"ARBCommGLCode"=> $data->ARBCommGLCode,
				"ARBCommPaymentDesc"=> $data->ARBCommPaymentDesc,
				"Banca2Code"=> $data->Banca2Code,
				"GSTRate"=> $data->GSTRate,
				"GSTTaxCode"=> $data->GSTTaxCode,
				"GSTChgTy"=> $data->GSTChgTy


		    ]);
		}


		$table_name = 'CodeARBs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeARB')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeARBs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);
		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeDealerBrchs(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codedealerbrch')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeDealerBrchs')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'DBCode' => $data->DBCode,
		    	'DBDesc' => $data->DBDesc
		    ]);
		}


		$table_name = 'CodeDealerBrchs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codedealerbrch')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeDealerBrchs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);
		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeDealers(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codedealer')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeDealers')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'DealerCode' => $data->DealerCode,
		    	'DealerDesc' => $data->DealerDesc
		    ]);
		}


		$table_name = 'CodeDealers';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codedealer')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeDealers')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeAdminFees(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodeAdminFee')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->Active == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeAdminFees')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	'AdminFeeCode' => $data->AdminFeeCode,
		    	'AdminFeeAmt' => $data->AdminFeeAmt,
		    	'AdminFeedesc' => $data->AdminFeeDesc,
		    	'BenePaymentDesc' => $data->BenePaymentDesc,
		    	'AdminFeeGLCode' => $data->AdminFeeGLCode,
		    	'GSTRate' => $data->GSTRate,
		    	'GSTTaxCode' => $data->GSTTaxCode,
		    	'GSTChgTy' => $data->GSTChgTy
		    ]);
		}


		$table_name = 'CodeAdminFees';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeAdminFee')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAdminFees')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeIntPackages(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = codeintpackage::get();
    	

		foreach($datas as $data){

			if($data->IntAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}


			if($data->FBInsType == "Y"){
				$FBInsType = true;
			}
			else{
				$FBInsType = false;
			}


			if($data->InsLifeFlag == "Y"){
				$InsLifeFlag = true;
			}
			else{
				$InsLifeFlag = false;
			}


			if($data->FBInsLife == "Y"){
				$FBInsLife = true;
			}
			else{
				$FBInsLife = false;
			}


			if($data->PFeeFlag == "Y"){
				$PFeeFlag = true;
			}
			else{
				$PFeeFlag = false;
			}


			if($data->FBProcFee == "Y"){
				$FBProcFee = true;
			}
			else{
				$FBProcFee = false;
			}


			if($data->OAFlag == "Y"){
				$OAFlag = true;
			}
			else{
				$OAFlag = false;
			}


			if($data->FBOA == "Y"){
				$FBOA = true;
			}
			else{
				$FBOA = false;
			}


			if($data->FBSecDep == "Y"){
				$FBSecDep = true;
			}
			else{
				$FBSecDep = false;
			}


			if($data->ARBFlag == "Y"){
				$ARBFlag = true;
			}
			else{
				$ARBFlag = false;
			}


			if($data->FBARB == "Y"){
				$FBARB = true;
			}
			else{
				$FBARB = false;
			}


			if($data->LOLGFlag == "Y"){
				$LOLGFlag = true;
			}
			else{
				$LOLGFlag = false;
			}

			if($data->FBLOLG == "Y"){
				$FBLOLG = true;
			}
			else{
				$FBLOLG = false;
			}

			if($data->SDFlag == "Y"){
				$SDFlag = true;
			}
			else{
				$SDFlag = false;
			}

			if($data->FBStamp == "Y"){
				$FBStamp = true;
			}
			else{
				$FBStamp = false;
			}

			if($data->LGFlag == "Y"){
				$LGFlag = true;
			}
			else{
				$LGFlag = false;
			}

			if($data->FBLettGua == "Y"){
				$FBLettGua = true;
			}
			else{
				$FBLettGua = false;
			}

			if($data->MDFlag == "Y"){
				$MDFlag = true;
			}
			else{
				$MDFlag = false;
			}

			if($data->FBMemoDep == "Y"){
				$FBMemoDep = true;
			}
			else{
				$FBMemoDep = false;
			}

			if($data->EntitleFlag == "Y"){
				$EntitleFlag = true;
			}
			else{
				$EntitleFlag = false;
			}

			if($data->SOFlag == "Y"){
				$SOFlag = true;
			}
			else{
				$SOFlag = false;
			}

			if($data->FBSetOff == "Y"){
				$FBSetOff = true;
			}
			else{
				$FBSetOff = false;
			}

			if($data->WadiahFlag == "Y"){
				$WadiahFlag = true;
			}
			else{
				$WadiahFlag = false;
			}

			if($data->FBWadiah == "Y"){
				$FBWadiah = true;
			}
			else{
				$FBWadiah = false;
			}

			if($data->TemanFlag == "Y"){
				$TemanFlag = true;
			}
			else{
				$TemanFlag = false;
			}

			if($data->FBTeman == "Y"){
				$FBTeman = true;
			}
			else{
				$FBTeman = false;
			}

			if($data->IBGFlag == "Y"){
				$IBGFlag = true;
			}
			else{
				$IBGFlag = false;
			}

			if($data->FBIBG == "Y"){
				$FBIBG = true;
			}
			else{
				$FBIBG = false;
			}

			if($data->DSRFlag == "Y"){
				$DSRFlag = true;
			}
			else{
				$DSRFlag = false;
			}


			DB::connection('sqlsrv2')
			->table('CodeIntPackages')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
				'LnPackId'=> $data->codeintpackage_to_CodeLoanPackages->Id ?? NULL,
				'MinYr'=> $data->MinYr,
				'MaxYr'=> $data->MaxYr,
				'LnLimit'=> $data->LnLimit,
				'IntRate'=> $data->IntRate,
				'MinAge'=> $data->MinAge,
				'MaxAge'=> $data->MaxAge,
				'InsDesc'=> $data->InsDesc,
				'FBInsType'=> $FBInsType,
				'GSTRateIns'=> $data->GSTRate_Ins,
				'GSTTaxCodeIns'=> $data->GSTTaxCode_Ins,
				'GSTChgTyIns'=> $data->GSTChgTy_Ins,
				'LnInsType'=> $data->LnInsType,
				'LnInsLife'=> $data->LnInsLife,
				'InsLifeFlag'=> $InsLifeFlag,
				'FBInsLife'=> $FBInsLife,
				'LnProcessFee'=> $data->LnProcessFee,
				'PFeeFlag'=> $PFeeFlag,
				'FBProcFee'=> $FBProcFee,
				'LnAOSearch'=> $data->LnOASearch,
				'OAFlag'=> $OAFlag,
				'FBOA'=> $FBOA,
				'LnSecDep'=> $data->LnSecDep,
				'FBSecDep'=> $FBSecDep,
				'LnARB'=> $data->LnARB,
				'ARBFlag'=> $ARBFlag,
				'FBArb'=> $FBARB,
				'LnStampLOLG'=> $data->LnStampLOLG,
				'LOLGFlag'=> $LOLGFlag,
				'FBLOLG'=> $FBLOLG,
				'LnStampDuty'=> $data->LnStampDuty,
				'SDFlag'=> $SDFlag,
				'FBStamp'=> $FBStamp,
				'LnLetterGua'=> $data->LnLetterGua,
				'LGFlag'=> $LGFlag,
				'FBLettGua'=> $FBLettGua,
				'LnMemoDep'=> $data->LnMemoDep,
				'MDFlag'=> $MDFlag,
				'FBMemoDep'=> $FBMemoDep,
				'Entitle'=> $data->Entitle,
				'EntitleFlag'=> $EntitleFlag,
				'SOFlag'=> $SOFlag,
				'FBSetOff'=> $FBSetOff,
				'Wadiah'=> $data->Wadiah,
				'WadiahFlag'=> $WadiahFlag,
				'FBWadiah'=> $FBWadiah,
				'GSTRateWadiah'=> $data->GSTRate_Wadiah,
				'Teman'=> $data->Teman,
				'TemanFlag'=> $TemanFlag,
				'FBTeman'=> $FBTeman,
				'IBGFee'=> $data->IBGFee,
				'IBGFlag'=> $IBGFlag,
				'FBIBG'=> $FBIBG,
				'GSTRateIBGFee'=> $data->GSTRate_IBGFee,
				'GSTTaxIBGFee'=> $data->GSTTaxCode_IBGFee,
				'TypeWaslah'=> $data->TypeWaslah,
				'FinanceConcept'=> $data->FinanceConcept,
				'BaseRateCode'=> $data->BaseRateCode,
				'MinRate'=> $data->MinRate,
				'TypeScript'=> $data->TypeScript,
				'DSRFlag'=> $DSRFlag,
				'ManualPM'=> $data->ManualPM,
				'MultInst'=> $data->MultInst,
				'BeneGLCode2'	=> $data->BeneGLCode2,
				'GSTRateProcessFee'	=> $data->GSTRate_ProcessFee,
				'GSTTaxCodeProcessFee' => $data->GSTTaxCode_ProcessFee,
				'GSTChgTyProcessFee'=> $data->GSTChgTy_ProcessFee,
				'GSTRateOA'=> $data->GSTRate_OA,
				'GSTTaxCodeOA'=> $data->GSTTaxCode_OA,
				'GSTChgTyOA'=> $data->GSTChgTy_OA,
				'GSTRateStampLOLG'=> $data->GSTRate_StampLOLG,
				'GSTTaxStampLOLG'=> $data->GSTTaxCode_StampLOLG,
				'GSTChgStampLOLG'=> $data->GSTChgTy_StampLOLG,
				'GSTRateStamp'=> $data->GSTRate_StampDuty,
				'GSTTaxStampDuty'=> $data->GSTTaxCode_StampDuty,
				'GSTChgStampDuty'=> $data->GSTChgTy_StampDuty,
				'GSTRateSetOff'=> $data->GSTRate_SetOff,
				'GSTTaxSetOff'=> $data->GSTTaxCode_SetOff,
				'GSTChgSetOff'=> $data->GSTChgTy_SetOff,
				'GSTTaxWadiah'=> $data->GSTTaxCode_Wadiah,
				'GSTChgWadiah'=> $data->GSTChgTy_Wadiah,
				'GSTRateTeman'=> $data->GSTRate_Teman,
				'GSTTaxTeman'=> $data->GSTTaxCode_Teman,
				'GSTChgTeman'=> $data->GSTChgTy_Teman,
				'GSTChgIBGFee'=> $data->GSTChgTy_IBGFee,
				'LnSetOff'=> $data->LnSetOff


		    ]);
		}


		$table_name = 'CodeIntPackages';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeintpackage')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeIntPackages')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeBeneficiarys(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = codebeneficiary::get();

      	/*$datas = DB::connection('sqlsrv')
      			->table('codebeneficiary')
			    ->join('sqlsrv2.CodeBeneBank', 'sqlsrv2.CodeBeneBank.BeneBankCode', '=', 'sqlsrv.codebeneficiary.BeneCode')
			    ->select('*')
			    ->get();	*/


		foreach($datas as $data){

			$zone = CodeClrZones::where('ClrZoneCode', $data->BeneClrZone)->first();


			if($data->BeneAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeBeneficiarys')
		    ->insert([
		    	'BeneId' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
				'Addr1' => $data->BeneAdd1,
				'Addr2' => $data->BeneAdd2,
				'Addr3' => $data->BeneAdd3,
				/*'Postcode' => $data->
				'City' => $data->
				'State' => $data->*/
				'BeneCode' => $data->BeneCode,
				'BeneRegNo' => $data->BeneRegNo,
				'BeneDesc' => $data->BeneDesc,
				'BeneBank' => $data->codebeneficiary_to_CodeBeneBank->Id ?? NULL,
				'BeneACNo' => $data->BeneACNo,
				'BenePayeeName' => $data->BenePayeeName,
				'BenePaymentDesc' => $data->BenePaymentDesc,
				'BeneCharges' => $data->BeneCharges,
				'BeneMOPCode' => $data->codebeneficiary_to_CodeModeOfPayments->Id ?? NULL,
				'BeneGLCode' => $data->codebeneficiary_to_CodeGLs->Id ?? NULL,
				'BeneGLCode2' => $data->BeneGLCode2,
				'BeneDeliMet'=> $data->codebeneficiary_to_CodeDelimets->Id ?? NULL,
				'BeneDispTo'=> $data->codebeneficiary_to_CodeDispatchTos->Id ?? NULL,
				'BenePayTab'=> $data->BenePayTab,
				'BeneClearingZone'=> $zone->codebeneficiary_to_CodeClrZones->Id ?? NULL 
				


		    ]);
		}


		$table_name = 'CodeBeneficiarys';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codebeneficiary')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeBeneficiarys')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeBeneBank(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codebenebank')
               ->select('*')
               ->get();   	

		foreach($datas as $data){
			if($data->BeneBankACT == 1){
				$status = true;
			}
			else{
				$status = false;
			}


			DB::connection('sqlsrv2')
			->table('CodeBeneBank')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
				'BeneBankCode' => $data->BeneBank_Code,
				'BeneBankBame' => $data->BeneBank_Name,
				'Giro' => $data->Giro,
				'Rentas' => $data->Rentas,
				'RentasI' => $data->RentasI,
				'CashierOrder' => $data->Cashier_Order,
				'InternalFundTransfer' => $data->Internal_Fund_Transfer,
				'RentasCode' => $data->RentasCode

		    ]);
		}

		$table_name = 'CodeBeneBank';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codebenebank')
    					->count();

    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('codebenebank')
    					->count();

    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeSecuParams(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codesecu_param')
               ->select('*')
               ->get();
    	
		foreach($datas as $data){

			if($data->Strong_Pwd == 'Y'){
				$StrongPwd = 1;
			}else{
				$StrongPwd = 0;
			}

			DB::connection('sqlsrv2')
			->table('CodeSecuParams')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,

		    	"SessionTimeout"  => $data->Session_Timeout,
		    	"PwdExpDays"  => $data->Pwd_Exp_Days,
		    	"PwdHist"  => $data->Pwd_Hist,
		    	"PwdInactive"  => $data->Pwd_Inactive,
		    	"StrongPwd"  => $StrongPwd,
		    	"PwdMinLength"  => $data->Pwd_MinLength,
		    	"PwdMaxLength"  => $data->Pwd_MaxLength,

		    	"FailedTry"  => '3',
		    	"LockedDuration"  => '1000',
		    	"RoundRobin"  => '1'

		    	//"Max_Age","Min_Age",
		    ]);
		}




		$table_name = 'CodeSecuParams';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codesecu_param')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeSecuParams')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeSettlePtys(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = codesettlepty::get();


		foreach($datas as $data){

			if($data->SetPtyACT == 1){
				$status = true;
			}
			else{
				$status = false;
			}

			DB::connection('sqlsrv2')
			->table('CodeSettlePtys')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	
		    	'SetPtyCode' => $data->SetPtyCode,
				'SetPtyType' => $data->SetPtyType,
				'SetPtyAbbv' => $data->SetPtyAbbrv,
				'SetptyDesc' => $data->SetPtyDesc,
				'SetPtyBank_Name' => $data->codesettlepty_to_CodeBeneBank->Id ?? NULL,
				'SetPtyRedemptCode' => $data->codesettlepty_to_CodeMopRedemps->Id ?? NULL,
				'SetPtybene_Name' => $data->SetPtybene_Name,
				'SetPtyPaymentDesc' => $data->SetPtyPaymentDesc,
				'SetPtyMBSMOP_Code' => $data->codesettlepty_to_CodeTransaction->Id ?? NULL,
				'SetPtyMBSGL_Code' => $data->codesettlepty_to_CodeGLs->Id ?? NULL,
				'SetPtyCMSMOP_Code' => $data->codesettlepty_to_CodeModeOfPayments->Id ?? NULL,
				'Addr1' => $data->SetPtyAdd1,
				'Addr2' => $data->SetPtyAdd2,
				'Addr3' => $data->SetPtyAdd3,
				'SetRegNo' => $data->SetRegNo,
				'SetPtyBankAcc' => $data->SetPtyBankAcc

				/*'Postcode' => $data->AcadCode,
		    	'City' => $data->AcadCode,
		    	'State' => $data->AcadCode,*/

		    ]);
		}


		$table_name = 'CodeSettlePtys';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codesettlepty')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeSettlePtys')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeCoop(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codecoop')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeCoop')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Addr1' => $data->CoopAdd1,
		    	'Addr2' => $data->CoopAdd2,
		    	'Addr3' => $data->CoopAdd3,
		    	'Postcode' => $data->CoopPostcode,
		    	'City' => $data->CoopCity,
		    	'State' => $data->CoopState,
		    	'CoopCode' => $data->CoopCode,
		    	'CoopName' => $data->CoopName,
		    	'CoopTel1' => $data->CoopTel1,
		    	'CoopTel2' => $data->CoopTel2,
		    	'CoopFax' => $data->CoopFax,
		    	'CoopEmail' => $data->CoopEmail,

		    	'CoopProfMargin' => $data->CoopProfMargin,
		    	'CoopProcessFee' => $data->CoopProcessFee,
		    	'CoopMshipFee' => $data->CoopMshipFee,
		    	'CoopMiscFee' => $data->CoopMiscFee,

		    	'CoopLegalFee' => $data->CoopLegalFee,
		    	'Shn' => $data->Shm,
		    	'Shnm' => $data->Shnm,
		    	'ShSol' => $data->ShSol,

		    	'Deferment' => $data->Deferment,
		    	'Fd' => $data->Fd,
		    	'CoopPer' => $data->COOPPer,
		    	'CoopRegNo' => $data->COOPRegno,

		    	'CoopAcNo' => $data->COOPAcno,
		    	'CoopBankAcNo' => $data->COOPBankACNo,
		    	'CoopCMSMop' => $data->COOPCMSMOP,
		    	'CoopNameCMS' => $data->COOPNAMECMS
		    ]);
		}


		$table_name = 'CodeCoop';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codecoop')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeCoop')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeCoopBranchs(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = codecoopbranch::get();    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeCoopBranchs')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'CoopId' => $data->codecoopbranch_to_CodeCoop->Id ?? NULL,
		    	'BrchCode' => $data->BrchCode,
		    	'BrchDesc' => $data->BrchDesc
		    ]);
		}


		$table_name = 'CodeCoopBranchs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codecoopbranch')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeCoopBranchs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeBranches(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = codebranch::get();
    	
		foreach($datas as $data){

			if($data->BrAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeBranches')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'BrCode'=> $data->BrCode, 
		    	'BrName' => $data->BrName,
		    	'BrLoc' => $data->BrLoc,
		    	'BrState' => $data->BrState,
		    	'BrRegion' => $data->codebranch_to_CodeRegions->Id ?? NULL,

		    	'BrBOF'=> $data->BrBOF, 
		    	'BrACNo' =>  $data->BrACNo,
		    	'BrZone' => $data->BrZone,
		    	'FICode' => $data->FICode,

		    	'ControlUnit'=> $data->ControlUnit, 
		    	'Addr1' =>  $data->BrAdd1,
		    	'Addr2' => $data->BrAdd2,
		    	'Addr3' => $data->BrAdd3,

		    	/*'City' => $data->BrRegion,
		    	'Postcode' => $data->BrRegion,
		    	'State' => $data->DeliMet_Code,*/

		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status
		    ]);
		}

		$table_name = 'CodeBranches';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codebranch')
    					->count();

    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeBranches')
    					->count();

    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeVarianceRates(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodeVarianceRate')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			if($data->Active == 'Y'){
				$status = true;
			}
			else{
				$status = false;
			}

			DB::connection('sqlsrv2')
			->table('CodeVarianceRates')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Code' => $data->Code,
		    	'VarianceCode' => $data->VarianceCode,
		    	'VarianceRate' => $data->VarianceRate,
		    	'VarianceDesc' => $data->VarianceDesc
		    ]);
		}


		$table_name = 'CodeVarianceRates';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeVarianceRate')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeVarianceRates')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeRejectReass(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('coderejectreas')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->SMSFlag == 'Y'){
				$status = true;
			}
			else{
				$status = false;
			}

			DB::connection('sqlsrv2')
			->table('CodeRejectReass')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'RejReasCode' => $data->RejReasCode,
		    	'RegionDesc' => $data->RejReasDesc,
		    	'SMSFlag' => $status,
		    	'BNMRejCode' => $data->BNMREJCODE,
		    	'BNMStatus' => $data->BNMSTATUS
		    ]);
		}


		$table_name = 'CodeRejectReass';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('coderejectreas')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeRejectReass')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeCheckLists(Request $request)
    {
    	$today = date("Y-m-d H:i:s");

    	$datas = codechecklist::get();
    	
		foreach($datas as $data){

			DB::connection('sqlsrv2')
			->table('CodeCheckLists')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'CLCode' => $data->CLCode,
		    	'CLType' => $data->CLType,
		    	'CLMandatory' => $data->CLMandatory,
		    	'CLBiro' => $data->codechecklist_to_CodeCollectionTypes->Id,
		    	'CLSection' => $data->CLSection,
		    	'CLDesc' => $data->CDesc
		    ]);
		}


		$table_name = 'CodeCheckLists';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codechecklist')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeCheckLists')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);
		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeCeilingRates(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodeCeilingRate')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeCeilingRates')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'CeilingCode' => $data->CeilingCode,
		    	'CeilingRate' => $data->CeilingRate,
		    	'CeilingDesc' => $data->CeilingDesc
		    ]);
		}


		$table_name = 'CodeCeilingRates';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeCeilingRate')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeCeilingRates')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeConvertJobPosts(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodeConvertJobPost')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeConvertJobPosts')
		    ->insert([
		    	
		    	'JobDsnCode' => $data->JobDsgnCode,
		    	'PostCode' => $data->PostCode
		    ]);
		}


		$table_name = 'CodeConvertJobPosts';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeConvertJobPost')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeConvertJobPosts')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeCmsCancels(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codecmscancel')
               ->select('*')
               ->get();    	

		foreach($datas as $data){

			if($data->CMSCancelACT == 0){
				$status = true;
			}
			else{
				$status = false;
			}

			DB::connection('sqlsrv2')
			->table('CodeCmsCancels')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'CMSCancelCode' => $data->CMSCancel_Code,
		    	'CMSCancelDesc' => $data->CMSCancel_Desc
		    ]);
		}

		$table_name = 'CodeCmsCancels';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codecmscancel')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeCmsCancels')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeMOfficers(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = codemofficer::get();
    	

		foreach($datas as $data){

			if($data->Activate == 'Y'){
				$status = true;
			}
			else{
				$status = false;
			}

			if($data->MoAQSFlag == 'Y'){
				$statusMoAQSFlag = true;
			}
			else{
				$statusMoAQSFlag = false;
			}

			DB::connection('sqlsrv2')
			->table('CodeMOfficers')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	'OffID' => $data->OffID,
		    	'OffName' => $data->OffName,
		    	'OffBr' => $data->codemofficer_to_CodeBranches->Id ?? NULL,
		    	'OffDept' => $data->codemofficer_to_codeDepartments->Id ?? NULL,
		    	'OffBank' => $data->codemofficer_to_CodeBanks->Id ?? NULL,
		    	'OffAcNo' => $data->OffAcNo,
		    	'OffEntity' => $data->codemofficer_to_codeEntities->Id ?? NULL,
		    	'MoAQSFlag' => $statusMoAQSFlag
		    ]);
		}


		$table_name = 'CodeMOfficers';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codemofficer')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeMOfficers')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeModeOfPayments(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codemodeofpayment')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			if($data->MopAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeModeOfPayments')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'MopCode' => $data->Mopcode,
		    	'MopDesc' => $data->MopDesc,
		    	'MopMinAmt' => $data->MopMinAmt,
		    	'MopMaxAmt' => $data->MopMaxAmt,
		    	'MopBankAccNo' => $data->MopBnkACNo,
		    	'MopColName' => $data->MOPColName,
		    	'MopColIC' => $data->MOPColIC
		    ]);
		}


		$table_name = 'CodeModeOfPayments';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codemodeofpayment')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeModeOfPayments')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeJobStatuss(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codejobstatus')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->JStatAct == 'Y'){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeJobStatuss')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	'JStatCode' => $data->JStatCode,
		    	'JStatDesc' => $data->JStatDesc,
		    	'BNMCode' => $data->BNMCode
		    ]);
		}


		$table_name = 'CodeJobStatuss';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codejobstatus')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeJobStatuss')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodePostCodes(Request $request)
    {
    	
    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodePostcode')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodePostCodes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'Street' => $data->Street,
		    	'PostCode' => $data->Postcode,
		    	'PostOffice' => $data->PostOffice,
		    	'State' => $data->State
		    ]);
		}


		$table_name = 'CodePostCodes';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodePostcode')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodePostCodes')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');


		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeAGStates(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeAGState')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			if($data->AGStateStatus == 'Y'){
				$status = true;
			}
			else{
				$status = false;
			}

			DB::connection('sqlsrv2')
			->table('CodeAGStates')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'AGStateCode' => $data->AGStateCode,
		    	'AGStateDesc' => $data->AGStateDesc
		    ]);
		}


		$table_name = 'CodeAGStates';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeAGState')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAGStates')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeBumis(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeBumi')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeBumis')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'BumiCode' => $data->BumiCode,
		    	'BumiDesc' => $data->BumiDesc,
		    	'BumiBNMCode' => $data->BumiBNMCode
		    ]);
		}


		$table_name = 'CodeBumis';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeBumi')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeBumis')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeEmploySector(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeEmploySector')
               ->select('*')
               ->get();
    	
		foreach($datas as $data){

			if($data->EmpSecAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeEmploySector')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	'EmpSecCode' => $data->EmpSecCode,
		    	'EmpSecDesc' => $data->EmpSecDesc,
		    	'BNMCode' => $data->BNMCode
		    ]);
		}


		$table_name = 'CodeEmploySector';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeEmploySector')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeEmploySector')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function codeEntities(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeentity')
               ->select('*')
               ->get();
    	
		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('codeEntities')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'EntCode' => $data->EntCode,
		    	'EntDesc' => $data->EntDesc
		    ]);
		}

		$table_name = 'codeEntities';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeentity')
    					->count();

    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('codeEntities')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeGenders(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeGender')
               ->select('*')
               ->get();
    	
		foreach($datas as $data){

			if($data->GenderAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeGenders')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'GenderCode' => $data->GenderCode,
		    	'GenderDesc' => $data->GenderDesc,
		    	'BNMCode' => $data->BNMCode
		    ]);
		}


		$table_name = 'CodeGenders';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeGender')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeGenders')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeGLs(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	/*$datas = DB::connection('sqlsrv')
    			->table('codegl')
               ->select('*')
               ->get();

   
      		/*$datas = codegl::Join('CodeTransaction', 'CodeTransaction.TransCode', '=', 'codegl.TransType')        		
        		->select(['CodeTransaction.*', 'codegl.GLCode', 'codegl.GLDesc'])
        		->get();*/


        		$datas = codegl::get();


        		/*$datas = CodeTransaction::Join('codegl', 'codegl.TransType', '=', 'CodeTransaction.TransCode')        
        		->get();*/
      		

        		/*$data= DB::table('sqlsrv.codegl')
              ->leftJoin('sqlsrv.CodeTransaction', 'sqlsrv.CodeTransaction.TransCode', '=', 'sqlsrv2.codegl.TransType')
              ->select(['sqlsrv.codegl.*','sqlsrv2.CodeTransaction.Id'])
              ->get();*/


        		/*$query = DB::table('sqlsrv.codegl as dt1')->leftjoin('sqlsrv2.CodeTransactions as dt2', 'dt2.TransCode', '=', 'dt1.TransType');        
		$datas = $query->select('sqlsrv2.CodeTransactions.Id')->get();*/
        		

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeGLs')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'GLCode' => $data->GLCode,
		    	'TransType' => $data->data_codegl->Id,
		    	'GLDesc' => $data->GLDesc
		    ]);
		}


		$table_name = 'CodeGLs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codegl')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeGLs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodePositions(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeposition')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodePositions')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'PostCode' => $data->PostCode,
		    	'PostDesc' => $data->PostDesc
		    ]);
		}


		$table_name = 'CodePositions';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeposition')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodePositions')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeWorkSecs(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeWorkSec')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeWorkSecs')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'WorkSecCode' => $data->WorkSecCode,
		    	'WorkSecDesc' => $data->WorkSecDesc
		    ]);
		}


		$table_name = 'CodeWorkSecs';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeWorkSec')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeWorkSecs')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeUserStats(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeuserstat')
               ->select('*')
               ->get();
    	
		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeUserStats')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'StatCode' => $data->StatCode,
		    	'StatDesc' => $data->StatDesc
		    ]);
		}

		$table_name = 'CodeUserStats';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeuserstat')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeUserStats')
    					->count();

    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeTransactions(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codetransaction')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeTransactions')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TransCode' => $data->TransCode,
		    	'TransDesc' => $data->TransDesc
		    ]);
		}


		$table_name = 'CodeTransactions';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codetransaction')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeTransactions')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeTitles(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeTitle')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeTitles')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'TtlCode' => $data->TtlCode,
		    	'TtlDesc' => $data->TtlDesc
		    ]);
		}


		$table_name = 'CodeTitles';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeTitle')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeTitles')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeSubmitTypes(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codesubmittype')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeSubmitTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'SubTyCode' => $data->SubTyCode,
		    	'SubTyDesc' => $data->SubTyDesc
		    ]);
		}


		$table_name = 'CodeSubmitTypes';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codesubmittype')
    					->count();

    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeSubmitTypes')
    					->count();

    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeResidencetys(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('coderesidencety')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeResidencetys')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'RestyCode' => $data->ResTyCode,
		    	'RestyDesc' => $data->ResTyDesc
		    ]);
		}


		$table_name = 'CodeResidencetys';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('coderesidencety')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeResidencetys')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeRepaymentTypes(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeRepaymentType')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeRepaymentTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'RepayCode' => $data->RepayCode,
		    	'Repaydesc' => $data->RepayDesc
		    ]);
		}


		$table_name = 'CodeRepaymentTypes';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeRepaymentType')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeRepaymentTypes')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeRemarkss(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('coderemarks')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeRemarkss')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'RemarkCode' => $data->RemarksCode,
		    	'RemarkDesc' => $data->RemarksDesc
		    ]);
		}


		$table_name = 'CodeRemarkss';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('coderemarks')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeRemarkss')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeReligions(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codereligion')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeReligions')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'RelCode' => $data->RelCode,
		    	'RelDesc' => $data->RelDesc
		    ]);
		}


		$table_name = 'CodeReligions';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codereligion')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeReligions')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeRelations(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('coderelation')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeRelations')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'RelationCode' => $data->RelationCode,
		    	'RelationDesc' => $data->RelationDesc
		    ]);
		}


		$table_name = 'CodeRelations';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('coderelation')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeRelations')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeRegions(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('coderegion')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeRegions')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'RegionCode' => $data->RegionCode,
		    	'RegionDesc' => $data->RegionDesc
		    ]);
		}


		$table_name = 'CodeRegions';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('coderegion')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeRegions')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeRaces(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('coderace')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->RaceAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeRaces')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'RaceCode' => $data->RaceCode,
		    	'RaceDesc' => $data->RaceDesc
		    ]);
		}


		$table_name = 'CodeRaces';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('coderace')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeRaces')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeProducts(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeproduct')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeProducts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ProdCode' => $data->ProdCode,
		    	'ProdDesc' => $data->ProdDesc
		    ]);
		}


		$table_name = 'CodeProducts';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeproduct')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeProducts')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeProcess(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeprocess')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeProcess')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Activate' => 'Y',
		    	'ProcCode' => $data->ProcCode,
		    	'ProcLevel' => $data->ProcLev,
		    	'ProcTime' => $data->ProcTime,
		    	'ProcDesc' => $data->ProcDesc
		    ]);
		}


		$table_name = 'CodeProcess';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeprocess')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeProcess')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodePendingReass(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codependingreas')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodePendingReass')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'PenReasCode' => $data->PenReasCode,
		    	'PenReasDesc' => $data->PendReasDesc
		    ]);
		}


		$table_name = 'CodePendingReass';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codependingreas')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodePendingReass')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodePayeeTypes(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codepayeety')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodePayeeTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'PayTyCode' => $data->PayTyCode,
		    	'PayTyDesc' => $data->PayTyDesc
		    ]);
		}


		$table_name = 'CodePayeeTypes';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codepayeety')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodePayeeTypes')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeOccupations(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeOccupation')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeOccupations')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'OccuCode' => $data->OccuCode,
		    	'OccuDesc' => $data->OccuDesc,
		    	'BNMCode' => $data->BNMCode
		    ]);
		}


		$table_name = 'CodeOccupations';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeOccupation')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeOccupations')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeNegeris(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codenegeri')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeNegeris')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'CityCode' => $data->NegCode,
		    	'CityDesc' => $data->NegDesc,
		    	'BNMCode' => $data->BNMCODE
		    ]);
		}


		$table_name = 'CodeNegeris';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codenegeri')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeNegeris')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeNegaras(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codenegara')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeNegaras')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'CntryCode' => $data->CtryCode,
		    	'CntryDesc' => $data->CtryDesc
		    ]);
		}


		$table_name = 'CodeNegaras';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codenegara')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeNegaras')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeMaritals(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codemarital')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			if($data->MarAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}

			DB::connection('sqlsrv2')
			->table('CodeMaritals')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'MarCode' => $data->MarCode,
		    	'MarDesc' => $data->MarDesc
		    ]);
		}


		$table_name = 'CodeMaritals';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codemarital')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeMaritals')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeNaOfBusss(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codenaofbuss')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->NOBAct == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeNaOfBusss')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	'NOBCode' => $data->NOBCode,
		    	'NOBDesc' => $data->NOBDesc
		    ]);
		}


		$table_name = 'CodeNaOfBusss';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codenaofbuss')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeNaOfBusss')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeInss(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeIns')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeInss')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'InsCode' => $data->InsCode,
		    	'InsDesc' => $data->InsDesc,
		    	'DisbDesc' => $data->DisbDesc
		    ]);
		}


		$table_name = 'CodeInss';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeIns')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeInss')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeIDTypes(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodeIDType')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeIDTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'IDTypeCode' => $data->IDTyCode,
		    	'IDTypeDesc' => $data->IDTyDesc
		    ]);
		}


		$table_name = 'CodeIDTypes';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeIDType')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeIDTypes')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    

    public function CodeEmpTys(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodeEmpTy')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->EmpTyAct == 'Y'){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeEmpTys')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	'EmpTyCode' => $data->EmpTyCode,
		    	'EmpTyDesc' => $data->EmpTyDesc
		    ]);
		}


		$table_name = 'CodeEmpTys';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeEmpTy')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeEmpTys')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeDispatchTos(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codedispatchto')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->DispToACT == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeDispatchTos')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'DispToCode' => $data->DispTo_code,
		    	'DispToDesc' => $data->DispTo_Desc
		    ]);
		}


		$table_name = 'CodeDispatchTos';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codedispatchto')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeDispatchTos')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function codeDeviations(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codedeviation')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('codeDeviations')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'DevCode' => $data->DevCode,
		    	'DevDesc' => $data->DevDesc
		    ]);
		}


		$table_name = 'codeDeviations';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codedeviation')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('codeDeviations')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function codeDepartments(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codedepartment')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('codeDepartments')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'DeptCode' => $data->DeptCode,
		    	'DeptDesc' => $data->DeptDesc
		    ]);
		}


		$table_name = 'codeDepartments';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codedepartment')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('codeDepartments')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeDCGroups(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeDCGroup')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeDCGroups')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'DcGpAct' => true,
		    	'DCGpCode' => $data->DCGpCode,
		    	'DCGPDesc' => $data->DCGpDesc
		    ]);
		}


		$table_name = 'CodeDCGroups';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeDCGroup')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeDCGroups')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeDelimets(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codedelimet')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){

			if($data->DeliMetACT == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			DB::connection('sqlsrv2')
			->table('CodeDelimets')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'DelimatCode' => $data->DeliMet_Code,
		    	'DelimetDesc' => $data->DeliMet_desc
		    ]);
		}


		$table_name = 'CodeDelimets';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codedelimet')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeDelimets')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeCreditGrades(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('CodeCreditGrade')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeCreditGrades')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'CreditCode' => $data->CreditCode,
		    	'CreditDesc' => $data->CreditDesc
		    ]);
		}


		$table_name = 'CodeCreditGrades';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeCreditGrade')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeCreditGrades')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeClrZones(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeclrzone')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			if($data->ClrZoneACT == 1){

				$status = true;
			}else{
				$status = false;
			}


			DB::connection('sqlsrv2')
			->table('CodeClrZones')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => $status,
		    	'ClrZoneCode' => $data->ClrZone_Code,
		    	'ClrZoneDesc' => $data->ClrZone_Desc
		    ]);
		}


		$table_name = 'CodeClrZones';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeclrzone')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeClrZones')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);


		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeBankrups(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeBankrup')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeBankrups')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'BankrupCode' => $data->BankrupCode,
		    	'BankrupDesc' => $data->BankrupDesc
		    ]);
		}


		$table_name = 'CodeBankrups';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeBankrup')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeBankrups')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

    public function CodeAppStatuss(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeappstatusfix')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeAppStatuss')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'ApplStatCode' => $data->AppStatCode,
		    	'ApplStatDesc' => $data->AppStatDesc
		    ]);
		}


		$table_name = 'CodeAppStatuss';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeappstatusfix')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAppStatuss')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

     public function CodeAppAuts(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeappvaut')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeAppAuts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,
		    	'AppvAutCode' => $data->AppvAutCode,
		    	'AppvAutDesc' => $data->AppvAutDesc
		    ]);
		}


		$table_name = 'CodeAppAuts';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeappvaut')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAppAuts')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }
    


    public function CodeAkadScripts(Request $request)
    {

    	$today = date("Y-m-d H:i:s");

    	$datas = DB::connection('sqlsrv')
    			->table('codeAkadScript')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeAkadScripts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' =>  date("Y-m-d H:i:s"),
		    	'Act' => true,		    		
		    	'AkadCode' => $data->AkadCode,
		    	'AkadDesc' => $data->AkadDesc
		    ]);
		}


		$table_name = 'CodeAkadScripts';


    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeAkadScript')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAkadScripts')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}


		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');


		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeAddTypes(Request $request)
    {

    	$today = date("Y-m-d H:i:s");

    	$datas = DB::connection('sqlsrv')
    			->table('codeaddtype')
                ->select('*')
                ->get();
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeAddTypes')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' => $today,
		    	'Act' => true,
		    	'AddTyCode' => $data->AddTyCode,
		    	'AddTyDesc' => $data->AddTyDesc
		    ]);
		}


		$table_name = 'CodeAddTypes';


    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeaddtype')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAddTypes')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}


		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);
		
		alert()->success($table_name,'Data Migration Successfully');


		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeBanks(Request $request)
    {

    	$today = date("Y-m-d H:i:s");

    	$datas = DB::connection('sqlsrv')
    			->table('codebank')
                ->select('*')
                ->get();
    	
              
		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeBanks')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' => $today,
		    	'Act' => true,
		    	'BankCode' => $data->BankCode,
		    	'BankDesc' => $data->BankDesc
		    ]);
		}
		

		$table_name = 'CodeBanks';


    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codebank')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeBanks')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}


		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);
		

		alert()->success($table_name,'Data Migration Successfully');


		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }


    public function CodeAGBranches(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeAGBranch')
               ->select('*')
               ->get(); 
    	

		foreach($datas as $data){
			DB::connection('sqlsrv2')
			->table('CodeAGBranches')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' => $today,
		    	'Act' => true,
		    	'AGBranchCode' => $data->AGBranchCode,
		    	'AGBranchDesc' => $data->AGBranchDesc,
		    	'AGBranchStatus' => true
		    ]);
		}


		$table_name = 'CodeAGBranches';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeAGBranch')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAGBranches')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }
   

    public function CodeAGDepts(Request $request)
    {
    	$today = date("Y-m-d H:i:s");
    	
    	$datas = DB::connection('sqlsrv')
    			->table('codeAGDept')
               ->select('*')
               ->get();
    	

		foreach($datas as $data){
			if($data->AGDeptStatus == 1){
				$status = true;
			}
			else{
				$status = false;
			}
			
			DB::connection('sqlsrv2')
			->table('CodeAGDepts')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' => $today,
		    	'Act' => true,
		    	'AGDeptCode' => $data->AGDeptCode,
		    	'AGDeptDesc' => $data->AGDeptDesc,
		    	'AGDeptStatus' => $status
		    ]);
		}


		$table_name = 'CodeAGDepts';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('codeAGDept')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAGDepts')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



    public function CodeAGPayCenters(Request $request)
    {
    	

    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	//Schema::disableForeignKeyConstraints();
    	$datas = CodeAGPayCenter::get();


		foreach($datas as $data){


			DB::connection('sqlsrv2')
			->table('CodeAGPayCenters')
		    ->insert([
		    	'Id' => Uuid::uuid4()->tostring(), 
		    	'CreatedBy'=> 'Bobonn3', 
		    	'CreatedDate' => $today,
		    	'Act' => true,
		    	'Addr1' => $data->Addr1,
		    	'Addr2' => $data->Addr2,
		    	'Addr3' => $data->Addr3,
		    	//'Postcode' =>'',
		    	//'City' => '',
		    	//'State' => '',
		    	'Code' => $data->Code,
		    	'AGPayCenterCode' => $data->AGPayCentreCode,
		    	'AGDepartCode' => $data->CodeAGPayCenter_to_CodeAGDepts->Id ?? NULL,

		    	'AGBranchCode' => $data->CodeAGPayCenter_to_CodeAGBranches->Id ?? NULL,
		    	'Description' => $data->Description,
		    	'ReferTo' => $data->ReferTo,
		    	'CodeTitleReferTo' => $data->CodeTitleReferTo,
		    	'Designation' => $data->CodeDesignation
		    	
		    ]);
		}


		$table_name = 'CodeAGPayCenters';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('CodeAGPayCenter')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('CodeAGPayCenters')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }

   
    public function AspNetUsers(Request $request)
    {
    	
    	ini_set('max_execution_time', 300000);

    	$today = date("Y-m-d H:i:s");

    	  	
    	$datas = usertab::leftjoin($this->db_dest.'.dbo.CodeDCGroups as db2','usertab.DCGpCode','=','db2.DCGpCode')
    			->leftjoin($this->db_dest.'.dbo.CodeBranches as db3','usertab.BrCode','=','db3.BrCode')
    			->join('usrgrouptab', 'usrgrouptab.UsrGpCode', '=', 'usertab.UsrGpCode')
    			->select(//'applrefnotabtemp.Userid as CreatedBy',
						'*',
						'db2.Id as id_CodeDCGroups',
						'db3.Id as id_CodeBranches')
    			->where('usertab.Activate', 'Y')
    			//->where('usertab.UsrName', 'MUHAMAD SAHROLAZLI BIN RAMLI')
    			->get();


		foreach($datas as $data){

			/*Role*/
			if($data->UsrGpCode == "000018"){
				$group = "DOC CHECKLIST B";
			}elseif($data->UsrGpCode == "CSU"){
				$group = "CUSTOMER SERVICE";
			}elseif($data->UsrGpCode == "EXTR"){
				$group = "EXTERNAL REDEMPTION";
			}elseif($data->UsrGpCode == "JOB103"){
				$group = "JOB & SALARY CONFIRMATION";
			}elseif($data->UsrGpCode == "000019"){
				$group = "DISBURSEMENT AUTHORIZER";
			}elseif($data->UsrGpCode == "000016"){
				$group = "DOC CHECKLIST";
			}elseif($data->UsrGpCode == "000015"){
				$group = "SETTLEMENT UNIT";
			}elseif($data->UsrGpCode == "000014"){
				$group = "LOAN CONFIRMATION";
			}elseif($data->UsrGpCode == "000012"){
				$group = "CR REVIEW";
			}elseif($data->UsrGpCode == "000010"){
				$group = "BRANCH LIAISON";
			}elseif($data->UsrGpCode == "ADMIN3"){
				$group = "ADMIN3";
			}elseif($data->UsrGpCode == "ADMIN2"){
				$group = "ADMIN2";
			}elseif($data->UsrGpCode == "ADMIN2"){
				$group = "ADMIN2";
			}elseif($data->UsrGpCode == "ADMIN1"){
				$group = "ADMIN1";
			}elseif($data->UsrGpCode == "000013"){
				$group = "APPR AUTHORITIES";
			}elseif($data->UsrGpCode == "ENQ"){
				$group = "ENQUIRY";
			}elseif($data->UsrGpCode == "CHKPRC"){
				$group = "PROCESSOR";
			}elseif($data->UsrGpCode == "000017"){
				$group = "DISBURSEMENT MAKER";
			}


			if($data->Activate == "Y"){
					$Activate = True;
			}else{
					$Activate = False;
			}

			if($data->DualSign == "Y"){
				$DualSign = True;
			}else{
					$DualSign = False;
			
			}



			$get_group = AspNetRoles::where('Name', $group)
    					->first();


			$uuid = Uuid::uuid4()->tostring();


			$name_upper = strtoupper($data->UsrName);
			$id_upper = strtoupper($data->UsrID);
			$name_lower = strtolower($data->UsrName);

			$name_lower_under = str_replace(' ', '_', $name_lower);


			$i = 1;
			DB::connection('sqlsrv2')
			->table('AspNetUsers')
		    ->insert([
		    	
		    	'Id' => $uuid,
				'UserName'	 => $data->UsrID,
				'NormalizedUserName'	=> $id_upper,
				//'Email'	 => $data->system : NULL
				//'NormalizedEmail'	 => $data->system : NULL
				'EmailConfirmed'	 => FALSE,
				'PasswordHash'	 => "AQAAAAEAACcQAAAAEPL8VW6h+CiAYRAxfavrMmUI7+cwAank0XcolIyw9LrR6a4TmKfZxxjrQ6P6psf0cg==",
				'SecurityStamp'	 => "OSYFDZN3P77CB5LR7ZGVXTGH5MBVNXUU",
				'ConcurrencyStamp'	 => "aa58512c-ee16-4479-afe2-1d0872c5208d",
				//'PhoneNumber'	 => $data->system : NULL
				'PhoneNumberConfirmed'	 => FALSE,
				'TwoFactorEnabled'	 => FALSE,
				//'LockoutEnd'	 =>  $this->today,
				'LockoutEnabled'	 => TRUE,
				'AccessFailedCount'	 => FALSE,
				'Name'	 => $name_upper,
				//'UserGroupCode'	 => $data->system : NULL
				'UserDeviation'	 => FALSE,
				'UserApprovalRight'	 => FALSE,
				'UserMinAppAmt'	 => $data->UsrMinAmt,
				'UserAppMaxAmt'	 => $data->UsrMaxAmt,
				'UserAppPassowrd'	 => "AQAAAAEAACcQAAAAEPL8VW6h+CiAYRAxfavrMmUI7+cwAank0XcolIyw9LrR6a4TmKfZxxjrQ6P6psf0cg==",
				'PasswordCreateDate'	 => $this->today,
				'RouteFlag'	 => $data->RouteFlag,
				'RouteDate'	 => $data->RouteDate,
				'RenewPassword'	 => $data->RenPwd,
				'Activate'	 => $Activate,
				'BrCode'	 => $data->id_CodeBranches,
				'CSUFlag'	 => $data->CSUFlag,
				'DCGrpCode'	 => $data->id_CodeDCGroups,
				'SIBSCode'	 => $data->SIBSCode,
				'DualSign'	 => $DualSign,
				'SuprOvrId'	 => $data->SuprOvrID,
				'UserIdCreatedDate'	 => $this->today,
				'UserIdCreatedBy'	 => "Bobonn1",
				//'UserStaffId'	 => $data->system : NULL
				'Salt'	 => "Qr8VtvwJEPyG2g==",
				//'StafDept'	 => $data->system : NULL
				'StaffId'	 => "System".$i++,
				'userRole'	 => $get_group->Name,
				//'CodeProcessId'	 => $data->UsrProcLev

		    	
		    ]);

		    DB::connection('sqlsrv2')
			->table('AspNetUserRoles')
		    ->insert([

		    	'UserId' => $uuid,
		    	'RoleId' => $get_group->Id

		    ]);
		    
		}


		$table_name = 'AspNetUsers';

    	$count_data_origin = DB::connection('sqlsrv')
    					->table('usertab')
    					->count();


    	$count_data_destination = DB::connection('sqlsrv2')
    					->table('AspNetUsers')
    					->count();


    	if($count_data_origin == $count_data_destination){
    		$status = 1;
    	}else{
    		$status = 0;
    	}

		$insert_data = DB::connection('sqlsrv3')
						->table('migrated_statuses')
						->insert([
				    	'table' => $table_name, 
				    	'total_row_origin'=> $count_data_origin, 
				    	'total_row_destination' =>  $count_data_destination,
				    	'status' => $status
				    ]);

		alert()->success($table_name,'Data Migration Successfully');

		return redirect()->back()->with(['success' => 'Report ID Updated Successfully']);
    }



}
