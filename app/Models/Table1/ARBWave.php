<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $MakerID
 * @property string $MakerDt
 * @property string $ARBCode
 * @property string $ARBAmt
 * @property string $ARBComm
 */
class ARBWave extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'ARBWave';

    /**
     * @var array
     */
    protected $fillable = ['ApplRefNo', 'MakerID', 'MakerDt', 'ARBCode', 'ARBAmt', 'ARBComm'];

}
