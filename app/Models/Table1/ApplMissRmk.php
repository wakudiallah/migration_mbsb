<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $NoID
 * @property string $Applrefno
 * @property string $Remark
 * @property string $UsrID
 * @property string $CanDtTm
 */
class ApplMissRmk extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'ApplMissRmk';

    /**
     * @var array
     */
    protected $fillable = ['NoID', 'Applrefno', 'Remark', 'UsrID', 'CanDtTm'];

}
