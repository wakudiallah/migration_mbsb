<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AppType
 * @property string $FacilityType
 * @property string $PurposeofLoan
 * @property string $AssetPurchase
 * @property string $SpecialFund
 * @property string $DPIFinance
 * @property string $Syndicated
 * @property string $Location
 * @property string $FinConcept
 * @property string $PrioritySector
 * @property string $MemberBankRakyat
 * @property string $CorporateStatus
 * @property string $IndustrialSector
 * @property string $EntityType
 * @property string $ApplicationStatus
 * @property string $ReportRequestType
 * @property string $FICode
 * @property string $ProdID
 * @property string $NewCust
 * @property string $Registered
 * @property string $SMEFinance
 */
class CCRISTaxonomy extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'CCRISTaxonomy';

    

}
