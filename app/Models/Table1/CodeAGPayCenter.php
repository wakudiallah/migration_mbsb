<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Code
 * @property string $AGPayCentreCode
 * @property string $AGBranchCode
 * @property string $AGDepartCode
 * @property string $Description
 * @property string $Status
 * @property string $ReferTo
 * @property string $Addr1
 * @property string $Addr2
 * @property string $Addr3
 * @property string $Addr4
 * @property string $CodeTitleReferTo
 * @property string $CodeDesignation
 */
class CodeAGPayCenter extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'CodeAGPayCenter';

   

    /**
     * @var array
     */
    protected $fillable = ['AGPayCentreCode', 'AGBranchCode', 'AGDepartCode', 'Description', 'Status', 'ReferTo', 'Addr1', 'Addr2', 'Addr3', 'Addr4', 'CodeTitleReferTo', 'CodeDesignation'];


    public function CodeAGPayCenter_to_CodeAGDepts() {
        return $this->belongsTo('App\Models\Table2\CodeAGDepts' , 'AGDepartCode', 'AGDeptCode'); 
    }


    public function CodeAGPayCenter_to_CodeAGBranches() {
        return $this->belongsTo('App\Models\Table2\CodeAGBranches' , 'AGBranchCode', 'AGBranchCode'); 
    }


    
    




}
