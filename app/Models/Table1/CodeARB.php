<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ARBCode
 * @property string $ARBType
 * @property string $ARBDesc
 * @property float $ARBAmt
 * @property float $ARBCommAmt
 * @property float $ARBTotal
 * @property string $ARBGLCode
 * @property string $ARBPaymentDesc
 * @property string $ARBCommGLCode
 * @property string $ARBCommPaymentDesc
 * @property string $Banca2Code
 * @property float $GSTRate
 * @property string $GSTTaxCode
 * @property string $GSTChgTy
 * @property string $Active
 */
class CodeARB extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'CodeARB';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'ARBCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['ARBType', 'ARBDesc', 'ARBAmt', 'ARBCommAmt', 'ARBTotal', 'ARBGLCode', 'ARBPaymentDesc', 'ARBCommGLCode', 'ARBCommPaymentDesc', 'Banca2Code', 'GSTRate', 'GSTTaxCode', 'GSTChgTy', 'Active'];

}
