<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property boolean $Act
 * @property string $AcadCode
 * @property string $AcadDesc
 */
class CodeAcademics extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'CodeAcademics';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    

}
