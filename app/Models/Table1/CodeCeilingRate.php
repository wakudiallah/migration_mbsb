<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $CeilingCode
 * @property float $CeilingRate
 * @property string $CeilingDesc
 */
class CodeCeilingRate extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'CodeCeilingRate';

    /**
     * @var array
     */
    protected $fillable = ['CeilingCode', 'CeilingRate', 'CeilingDesc'];

}
