<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $JobDsgnCode
 * @property string $PostCode
 */
class CodeConvertJobPost extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'CodeConvertJobPost';

    /**
     * @var array
     */
    protected $fillable = ['JobDsgnCode', 'PostCode'];

}
