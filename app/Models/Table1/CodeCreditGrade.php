<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $CreditCode
 * @property string $CreditDesc
 */
class CodeCreditGrade extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'CodeCreditGrade';

    /**
     * @var array
     */
    protected $fillable = ['CreditCode', 'CreditDesc'];

}
