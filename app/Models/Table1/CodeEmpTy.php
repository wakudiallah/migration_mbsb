<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $EmpTyCode
 * @property string $EmpTyDesc
 * @property string $EmpTyAct
 */
class CodeEmpTy extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'CodeEmpTy';

    /**
     * @var array
     */
    protected $fillable = ['EmpTyCode', 'EmpTyDesc', 'EmpTyAct'];

}
