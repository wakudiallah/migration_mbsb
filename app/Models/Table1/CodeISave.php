<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ISaveCode
 * @property float $ISaveAmt
 * @property string $ISaveDesc
 * @property string $BenePaymentDesc
 * @property string $BancaCode
 * @property float $GSTRate
 * @property string $GSTTaxCode
 * @property string $GSTChgTy
 * @property string $Active
 */
class CodeISave extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'CodeISave';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'ISaveCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['ISaveAmt', 'ISaveDesc', 'BenePaymentDesc', 'BancaCode', 'GSTRate', 'GSTTaxCode', 'GSTChgTy', 'Active'];

}
