<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Street
 * @property string $Postcode
 * @property string $PostOffice
 * @property string $State
 */
class CodePostcode extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'CodePostcode';

    /**
     * @var array
     */
    protected $fillable = ['Street', 'Postcode', 'PostOffice', 'State'];

}
