<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Code
 * @property string $VarianceCode
 * @property float $VarianceRate
 * @property string $VarianceDesc
 * @property string $Active
 */
class CodeVarianceRate extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'CodeVarianceRate';

    /**
     * @var array
     */
    protected $fillable = ['Code', 'VarianceCode', 'VarianceRate', 'VarianceDesc', 'Active'];

}
