<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $WakalahCode
 * @property float $WakalahAmt
 * @property string $WakalahDesc
 * @property string $BenePaymentDesc
 * @property string $WakalahGLCode
 * @property string $Active
 * @property float $GSTRate
 * @property string $GSTTaxCode
 * @property string $GSTChgTy
 */
class CodeWakalah extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'CodeWakalah';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $primaryKey = 'WakalahCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['WakalahAmt', 'WakalahDesc', 'BenePaymentDesc', 'WakalahGLCode', 'Active', 'GSTRate', 'GSTTaxCode', 'GSTChgTy'];


    public function CodeWakalah_to_codegl() {
        return $this->belongsTo('App\Models\Table2\CodeGLs', 'WakalahGLCode','GLCode'); 
    }


    public function CodeWakalah_to_CodeBeneficiarys() {
        return $this->belongsTo('App\Models\Table2\CodeBeneficiarys', 'BenePaymentDesc','BeneDesc'); 
    }


}
