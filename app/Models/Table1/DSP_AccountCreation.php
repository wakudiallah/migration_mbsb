<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class DSP_AccountCreation extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'DSP_AccountCreation';


    
    public function DSP_AccountCreation_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }


    /**
     * The primary key for the model.
     * 
     * @var string
     */

    //protected $primaryKey = 'No';


    /**
     * @var array
     */

    /*protected $fillable = ['ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'HDRNUM', 'ClientDt', 'ClientTm', 'UserID', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCOBCifNo', 'SeqNo', 'LoanAccNo', 'DataSend', 'DataReturn', 'DateSend', 'DateReceive'];*/


}
