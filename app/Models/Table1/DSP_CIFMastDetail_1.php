<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class DSP_CIFMastDetail_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'DSP_CIFMastDetail_1';


    public function DSP_CIFMastDetail_1_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }

    
    /**
     * @var array
     */
    /*protected $fillable = ['No', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'HDRNUM', 'ClientDt', 'ClientTm', 'UserID', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCOBCifNo', 'CustIDNo', 'Individual', 'ShortName', 'VIP', 'Insider', 'InquiryCode', 'MotherMaidenName', 'DateOfBirth', 'Country', 'Nationality', 'Bumi', 'Residence', 'Gender', 'Race', 'ReligionCode', 'LanguageIdentifier', 'MaritalStatus', 'MaritalStatusDate', 'Occupation', 'CustomerStatus', 'CustomerStatusDate', 'DeceasedStatus', 'DeceaseDate', 'CostCenter', 'HoldMailCode', 'PromotionMailCode', 'CombinedCycle', 'AddressSeq', 'StartRelationship', 'ReviewDate', 'CreationDate', 'Retention', 'SpecialInfoCode1', 'SpecialInfoCode2', 'SpecialInfoCode3', 'SpecialInfoCode4', 'SpecialInfoCode5', 'SpecialInfoCode6', 'SpecialInfoCode7', 'SpecialInfoCode8', 'SpecialInfoCode1Flag', 'SpecialInfoCode2Flag', 'SpecialInfoCode3Flag', 'SpecialInfoCode4Flag', 'SpecialInfoCode5Flag', 'SpecialInfoCode6Flag', 'SpecialInfoCode7Flag', 'SpecialInfoCode8Flag', 'CustomerInfoCode1', 'CustomerInfoCode2', 'CustomerInfoCode3', 'CustomerInfoCode4', 'CustomerInfoCode5', 'CustomerInfoCode6', 'CustomerInfoCode7', 'CustomerInfoCode8', 'CustomerInfCode1Flag', 'CustomerInfCode2Flag', 'CustomerInfCode3Flag', 'CustomerInfCode4Flag', 'CustomerInfCode5Flag', 'CustomerInfCode6Flag', 'CustomerInfCode7Flag', 'CustomerInfCode8Flag', 'Consitution', 'Placeofbirth', 'CustomerSegmentationCode', 'NumberofEmployees', 'HighRiskFlag', 'HighRiskCode', 'StaffFlag', 'StaffIDNumber', 'CustomerAge', 'Durationofstayatpresentaddress', 'HouseOwnershipCode', 'TypeofDwelling', 'Numberofdependents', 'BusinessPremisesOwnershipcode', 'TypeofBusinessPremises', 'BusinessOperatingHours', 'IDNumber', 'IDType', 'Salutation', 'AddressLine1', 'AddressLine2', 'AddressLine3', 'AddressLine4', 'City', 'PostalCode', 'HousePhone', 'MobilePhone', 'EmailAddress', 'DATEOFBIRTH2', 'STARTOPERATIONHOURS', 'ENDOPERATIONHOURS', 'BranchNumber', 'OfficePhone', 'ContactName', 'Fax', 'ContactName1', 'ContactName2', 'CreationDate2', 'Year', 'AnnualTurnover', 'Sector', 'NoofFullTimeEmp', 'TypeofClient', 'Customername1', 'DataSend', 'DataReturn', 'DateSend', 'DateReceive'];*/

}
