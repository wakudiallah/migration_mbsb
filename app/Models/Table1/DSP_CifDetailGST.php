<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class DSP_CifDetailGST extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'DSP_CifDetailGST';

    
    public function DSP_CifDetailGST_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }


}
