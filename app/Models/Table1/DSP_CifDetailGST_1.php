<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class DSP_CifDetailGST_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'DSP_CifDetailGST_1';



    public function DSP_CifDetailGST_1_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }




    /**
     * @var array
     */
    /*protected $fillable = ['No', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'HDRNUM', 'ClientDt', 'ClientTm', 'UserID', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCOBCifNo', 'CustIDNo', 'ID1', 'CustomerIDNo2', 'IDTypeCode2', 'IDIssueCountry2', 'Shortname', 'CustomerIDNo1', 'IDTypeCode1', 'IDIssueCountry1', 'CustomerIDNoNOB2', 'IDTypeCodeNOB2', 'IDIssueCountryNOB2', 'Branchnumber', 'Customernumber', 'Salutation', 'Customername1', 'Customername2', 'Class1', 'DOB', 'Individual', 'Countryofcitizenship', 'RaceCountry', 'ResidenceBusOper', 'Gender', 'Marital', 'Residentcode', 'Businesstype', 'ID2', 'Addresssequence1', 'Addresstype1', 'Addressformat1', 'Foreignaddress1', 'Addressline11', 'Addressline12', 'Addressline13', 'Citystatezip1', 'Postalcode1', 'State1', 'Country1', 'Addressseq2', 'Addresstype2', 'Addressformat2', 'Foreignaddress2', 'Addressline21', 'Addressline22', 'Addressline23', 'Citystatezip2', 'Postalcode2', 'State2', 'Country2', 'ContactSeq1', 'ContactType1', 'ContactDetail1', 'ContactName1', 'ContactSeq2', 'ContactType2', 'ContactDetail2', 'ContactName2', 'Costcenter', 'CustomerSegmentationCode', 'NumberofEmployees', 'HighRiskFlag', 'HighRiskCode', 'StaffFlag', 'StaffIDNumber', 'Religioncode', 'CustomerAge', 'Insidercode', 'DurationStay', 'HouseOwnershipCode', 'TypeofDwelling', 'Numberofdependents', 'BusinessPremisesOwnershipCode', 'TypeofBusinessPremises', 'BusinessOperatingHours', 'STARTOPERATIONHOURS', 'ENDOPERATIONHOURS', 'BNMSectorCode', 'BNMStateCode', 'BNMBumiCode', 'VIPcustomercode', 'BlacklistedFlag', 'Sourcecode', 'Reasoncode', 'Listeddescription', 'GSTRegNo', 'GSTRegFlag', 'DataSend', 'DataReturn', 'DateSend', 'DateReceive'];*/

}
