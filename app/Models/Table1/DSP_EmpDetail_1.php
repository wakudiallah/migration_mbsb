<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class DSP_EmpDetail_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'DSP_EmpDetail_1';


    public function DSP_EmpDetail_1_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }

    /**
     * @var array
     */
    /*protected $fillable = ['No', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'HDRNUM', 'ClientDt', 'ClientTm', 'UserID', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCOBCifNo', 'EmpHisSeq', 'EmpStat', 'EmpStDt', 'OccuCode', 'JobDsgnCode', 'TyEmp', 'EmpIndCode', 'EmployerCode', 'EmployerRate', 'EmployerName', 'IncomeBracketCode', 'Income', 'OthIncome', 'Expenditure', 'EmployeeID', 'Remark1', 'Remark2', 'Remark3', 'EmploymentEndDt1', 'EmploymentEndDt2', 'EmployerAdd1', 'EmployerAdd2', 'EmployerAdd3', 'EmployerAdd4', 'EmployerAdd5', 'EmployerCity', 'ForeignAdd', 'City', 'State', 'Postalcode', 'Country', 'Dept', 'BusinessType', 'EmpSec', 'CustPayClass', 'OffNo', 'FaxNo', 'HpNo', 'HRPhoneNo', 'Remark4', 'MonthlyIncome', 'MonthlyOtherIncome', 'MonthlyExpenditure', 'DateSend', 'DateReceive']; */

}
