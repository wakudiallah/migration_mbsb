<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class DSP_GetCifNo_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'DSP_GetCifNo_1';


    public function DSP_GetCifNo_1_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }

    /**
     * @var array
     */
    /*protected $fillable = ['No', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'HDRNUM', 'ClientDt', 'ClientTm', 'UserID', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCOBCifNo', 'CustIDNo', 'CustIDTy', 'IDNo', 'IDTypeCode', 'AliasFlag', 'CustomerName1', 'FormattedIdNo', 'Country', 'BranchNo', 'CifNo', 'CustomerName2', 'ShortName', 'Individual', 'ElectronicAddDesc', 'City', 'ZipCode', 'CollateralName', 'Address1', 'Address2', 'StaffIDNo', 'IDMNUUSR', 'DataSend', 'DataReturn', 'DateSend', 'DateReceive'];*/

}
