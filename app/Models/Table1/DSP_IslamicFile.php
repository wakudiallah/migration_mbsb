<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class DSP_IslamicFile extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'DSP_IslamicFile';


    public function DSP_IslamicFile_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }

    /**
     * @var array
     */
    /*protected $fillable = ['No', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'HDRNUM', 'ClientDt', 'ClientTm', 'UserID', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCOBCifNo', 'SeqNo', 'DataSend', 'DataReturn', 'DateSend', 'DateReceive'];*/

}
