<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $No
 * @property string $ApplRefNo
 * @property string $ScenarioNo
 * @property string $ActionCode
 * @property string $TransactionCode
 * @property string $HDRNUM
 * @property string $ClientDt
 * @property string $ClientTm
 * @property string $UserID
 * @property string $UserGroup
 * @property string $Branch
 * @property string $Status
 * @property string $ErrorCode
 * @property string $ErrorReason
 * @property string $MiCOBCifNo
 * @property string $SeqNo
 * @property string $DataSend
 * @property string $DataReturn
 * @property string $DateSend
 * @property string $DateReceive
 */
class DSP_IslamicFile_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'DSP_IslamicFile_1';

    /**
     * @var array
     */
    protected $fillable = ['No', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'HDRNUM', 'ClientDt', 'ClientTm', 'UserID', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCOBCifNo', 'SeqNo', 'DataSend', 'DataReturn', 'DateSend', 'DateReceive'];

}
