<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $XML_LOADPGM
 * @property string $User_ID
 * @property string $User_Group
 * @property string $Version_No
 * @property string $Prod_ID
 * @property string $Rpt_Type_1
 * @property string $Rpt_Type_2
 * @property string $Rpt_Type_3
 * @property float $Limit
 * @property string $Date_Time
 * @property string $UsrID
 * @property string $CCRIS_URL
 * @property string $CTOS_URL
 * @property string $BIS_URL
 * @property int $id
 * @property string $NameSpace
 * @property string $Usr_Pwd
 * @property string $SMSPeriod
 * @property string $ASSIDQ_URL
 * @property string $CCRIS_Port
 * @property string $DSP_URL
 * @property string $DSP_Port
 */
class Online_Check_Setup extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'Online_Check_Setup';

    /**
     * @var array
     
    protected $fillable = ['XML_LOADPGM', 'User_ID', 'User_Group', 'Version_No', 'Prod_ID', 'Rpt_Type_1', 'Rpt_Type_2', 'Rpt_Type_3', 'Limit', 'Date_Time', 'UsrID', 'CCRIS_URL', 'CTOS_URL', 'BIS_URL', 'id', 'NameSpace', 'Usr_Pwd', 'SMSPeriod', 'ASSIDQ_URL', 'CCRIS_Port', 'DSP_URL', 'DSP_Port'];*/

}
