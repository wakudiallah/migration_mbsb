<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

class address extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'address';

    /**
     * @var array
     */
    

    public function address_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','AcID', 'AcId'); 
    }


}
