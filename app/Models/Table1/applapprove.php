<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class applapprove extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'applapprove';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    //protected $primaryKey = 'ApplRefNo';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    //protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    //public $incrementing = false;

    /**
     * @var array
     */
    /** protected $fillable = ['AppvAutCode', 'AppvAmt', 'AppvTenure', 'AppvRate', 'AppvInst', 'AppStatCode', 'DecisionBy', 'RejReasCode', 'OfferDt', 'AcceptDt', 'AppvRemark', 'MBSFlag', 'UploadDt', 'DocCOM', 'AppvDeferment', 'AppvAdInst', 'Appvtpc', 'Appvshm', 'LMSFlag', 'CSUFlag', 'ApplRefNo2', 'AppvAmt2', 'AppvRate2', 'AppvInst2', 'DecisionBy2', 'DualSignID']; */

    public function applapprove_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }

}
