<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class appldisburse extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'appldisburse';


    public function appldisburse_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }


     /*public function applworkflow_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }*/

    /**
     * @var array
     */
    /*protected $fillable = ['CoOpAmt', 'FDAmt', 'AMFBAmt', 'EmergLoan', 'OSBalAMF', 'OSBal', 'LOAmt', 'ActAmt', 'GLTransAmt', 'InterBankAmt', 'ACNo', 'ChequeNo', 'SavingAC', 'TransDtTm', 'TransBrCode', 'TransCodeCO', 'TransCodeATT', 'TransCodeRCT', 'TransCodeGTT', 'TransUsrID', 'GLTransAmt2', 'TransAmt', 'TransAmtRef', 'TransAmtDesc', 'AuthUsrID', 'TransAmtCO', 'TransAmtATT', 'TransAmtRCT', 'TransAmtGTT', 'COdrNo', 'COdrDate', 'PayeeTy', 'PayeeName', 'GLACCO', 'GLACATT', 'GLACRCT', 'GLACGTT', 'HoldDisb', 'MBSFlag', 'UploadDt', 'SvrChg', 'Inc', 'postage', 'CCCharge', 'ReqDis', 'Netloan', 'StampFee', 'DisbDeferment', 'DisbAdInst', 'Disbtpc', 'Disbtpcno', 'Disbshm', 'TranscodeGTT1', 'TranscodeGTT2', 'TransAmtGTT1', 'TransAmtGTT2', 'GLACGTT1', 'GLACGTT2', 'CancelTransact', 'ReleaseDt', 'LMSFlag', 'NetBal', 'GLCNetBal', 'NetBalDesc', 'NetBalRef', 'TransCodeStamp', 'GLCStamp', 'StampDuty', 'StampDesc', 'StampRef', 'GLCStamp2', 'TransCodeSD', 'GLCSD', 'SecDep', 'SecDepDesc', 'SecDepRef', 'GLCSD2', 'TransCodeProcFee', 'GLCProcFee', 'ProcFee', 'ProcFeeDesc', 'ProcFeeRef', 'GLCProcFee2', 'TransCodeIns', 'GLCIns', 'Insurans', 'InsDesc', 'InsRef', 'GLCIns2', 'TransCodeMDTA', 'GLCMDTA', 'MDTA', 'MDTADesc', 'MDTARef', 'GLCMDTA2', 'TransCodeIBGFee', 'GLCIBGFee', 'IBGFee', 'IBGDesc', 'IBGRef', 'GLCIBGFee2', 'TransCodeOA', 'GLCOA', 'OASearch', 'OADesc', 'OARef', 'GLCOA2', 'TransCodeARB', 'GLCARB', 'ARB', 'ARBDesc', 'ARBRef', 'GLCARB2', 'TransCodeCommARB', 'GLCCommARB', 'CommARB', 'CommARBDesc', 'CommARBRef', 'GLCCommARB2', 'TranscodeWadiah', 'GLCWadiah', 'Wadiah', 'WadiahDesc', 'WadiahRef', 'GLCWadiah2', 'TranscodeTeman', 'GLCTeman', 'Teman', 'TemanDesc', 'TemanRef', 'GLCTeman2', 'payMode', 'payBank', 'ARBCode', 'WadiahFlag', 'WadBranch', 'TemanFlag', 'ReimbFee', 'ISaveFlag', 'TranscodeISave', 'GLCISave', 'ISave', 'ISaveDesc', 'ISaveRef', 'GLCISave2', 'WakalahFlag', 'TranscodeWakalah', 'GLCWakalah', 'Wakalah', 'WakalahDesc', 'WakalahRef', 'GLCWakalah2', 'LiveFlag', 'CSUFlag', 'ApplRefNo2', 'LoanAccNo2', 'Netloan2', 'TransAmt2', 'NetBal2', 'AdminFeeFlag', 'TranscodeAdminFee', 'GLCAdminFee', 'AdminFee', 'AdminFeeDesc', 'AdminFeeRef', 'GLCAdminFee2', 'RepayTy', 'GProcFee', 'GStampDuty', 'GLOLG', 'GInsurance', 'GOtherFee', 'GInsLife', 'GOA', 'GARB', 'GLetterGua', 'GMemoDep', 'GSetOff', 'GWadiah', 'GTeman', 'GSecDep', 'GMDTA', 'GIBGFee', 'GOASearch', 'GCommARB', 'GISave', 'GWakalah', 'GAdminFee', 'GTaxCodeProcFee', 'GTaxCodeStampDuty', 'GTaxCodeInsurance', 'GTaxCodeARB', 'GTaxCodeWadiah', 'GTaxCodeTeman', 'GTaxCodeSecDep', 'GTaxCodeMDTA', 'GTaxCodeIBGFee', 'GTaxCodeOASearch', 'GTaxCodeCommARB', 'GTaxCodeISave', 'GTaxCodeWakalah', 'GTaxCodeAdminFee', 'InsCode'];*/

}
