<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class applkpi extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'applkpi';

    /**
     * The primary key for the model.
     * 
     * @var string
     */

    public function applkpi_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }
    

}
