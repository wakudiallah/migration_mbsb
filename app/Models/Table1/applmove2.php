<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Applrefno
 * @property string $Usrid
 * @property string $UsrGrp
 * @property string $SubmitDt
 * @property string $StRouteDt
 * @property string $StprocDt
 * @property string $StProcTm
 * @property string $EndProcDt
 * @property string $EndProctm
 * @property string $TotProcTm
 * @property string $TotProcDay
 * @property string $ApplStat
 * @property string $NxProc
 * @property string $NxProcGrp
 * @property string $ProcCnt
 * @property string $PendingStat
 */
class applmove2 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'applmove2';

    /**
     * @var array
     */
    protected $fillable = ['Applrefno', 'Usrid', 'UsrGrp', 'SubmitDt', 'StRouteDt', 'StprocDt', 'StProcTm', 'EndProcDt', 'EndProctm', 'TotProcTm', 'TotProcDay', 'ApplStat', 'NxProc', 'NxProcGrp', 'ProcCnt', 'PendingStat'];

}
