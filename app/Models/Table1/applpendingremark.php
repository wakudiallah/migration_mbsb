<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $NoID
 * @property string $ApplRefNo
 * @property string $RmrkDt
 * @property string $RmrkTm
 * @property string $UsrID
 * @property string $RmrkDesc
 * @property string $CSUFlag
 */
class applpendingremark extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'applpendingremark';

    /**
     * @var array
     */
    protected $connection = 'sqlsrv';
    protected $fillable = ['NoID', 'ApplRefNo', 'RmrkDt', 'RmrkTm', 'UsrID', 'RmrkDesc', 'CSUFlag'];

}
