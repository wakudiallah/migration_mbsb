<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class applrefnotab extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'applrefnotab';

    /**
     * The primary key for the model.
     * 
    */



    public function applrefnotab_one_to_many_applrefnotemp() {
        return $this->hasMany('App\Models\Table1\applrefnotabtemp', 'ApplRefNo','ApplRefNo');    
    }


    public function applrefnotab_to_bla()
    {
        return $this->belongsTo('App\Models\Table1\bla', 'AcID', 'AcID');
    }   


    public function applrefnotab_to_cif()
    {
        return $this->belongsTo('App\Models\Table1\cif', 'AcID', 'AcID');
    }


    public function applrefnotab_to_submissiontype()
    {
        return $this->belongsTo('App\Models\Table1\submissiontype', 'AcID', 'AcID');
    }  


    public function applrefnotab_to_DSP_EmpDetail()
    {
        return $this->belongsTo('App\Models\Table1\DSP_EmpDetail', 'ApplRefNo', 'ApplRefNo');
    }






    public function applrefnotab_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }




}
