<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AGDeptCode
 * @property string $AGDeptDesc
 * @property string $AGDeptStatus
 */
class applrefnotabtemp extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'applrefnotabtemp';


    public function applrefnotabtemp_to_ApplRefNoTabTemps()
    {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps', 'CustIDNo', 'CustIdno');
    }

  

}
