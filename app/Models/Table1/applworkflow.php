<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class applworkflow extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'applworkflow';

    /**
     * @var array
     */
    /*protected $fillable = ['PrevUsrID', 'StProcTm', 'StProcDt', 'EndProcTm', 'EndProcDt', 'TotProcTm', 'NetTm', 'Rmrk', 'AppStatus', 'NextProccess', 'DualSignID', 'PenReasCode', 'CSUFlag', 'BrCode', 'PickFlag', 'Usrid_Curr'];*/


    public function applworkflow_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }

}
