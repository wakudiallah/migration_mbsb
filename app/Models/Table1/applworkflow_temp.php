<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $StRouteDt
 * @property string $StRouteTm
 * @property string $UsrID
 * @property string $CSUFlag
 */
class applworkflow_temp extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'applworkflow_temp';

    /**
     * @var array
     */
    protected $fillable = ['ApplRefNo', 'StRouteDt', 'StRouteTm', 'UsrID', 'CSUFlag'];

}
