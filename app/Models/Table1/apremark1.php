<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Applrefno
 * @property string $Rmk
 */
class apremark1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'apremark1';

    /**
     * @var array
     */
    protected $fillable = ['Applrefno', 'Rmk'];

}
