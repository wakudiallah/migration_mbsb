<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class bla extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'bla';


    public function bla_to_applrefnotab()
    {
        return $this->belongsTo('App\Models\Table1\applrefnotab', 'AcID', 'AcID');
    }


    public function bla_to_ApplRefNoTabTemps()
    {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps', 'AcID', 'AcId');
    }


    

}
