<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class caafredemption extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'caafredemption';



    public function caafredemption_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }

   

}
