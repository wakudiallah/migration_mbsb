<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $CustIDNo
 * @property string $Facility
 * @property float $BorrowerVal
 * @property float $GuarantorVal
 */
class caafunsexposure extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'caafunsexposure';

    
    public function caafunsexposure_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }


    
    

}
