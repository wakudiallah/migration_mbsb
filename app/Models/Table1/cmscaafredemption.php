<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;



class cmscaafredemption extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'cmscaafredemption';


    public function cmscaafredemption_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }

    /**
     * @var array
     */
    /*protected $fillable = ['RmtID', 'ApplRefNo', 'SetPtyCode', 'DeducVal', 'OSLoanAmt', 'LnACNo', 'UpdateBy', 'UpdateDt', 'DtSubmit', 'DtObtReceipt', 'VerifyBy', 'Rmk', 'ExpDt', 'GLCCoop', 'CoopDesc', 'CoopRef', 'AGTFlag', 'BankName', 'BankCode', 'BankAccNo', 'BankBusRegNo'];*/


}
