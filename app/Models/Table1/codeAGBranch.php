<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AGBranchCode
 * @property string $AGBranchDesc
 * @property string $AGBranchStatus
 */
class codeAGBranch extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'codeAGBranch';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'AGBranchCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['AGBranchDesc', 'AGBranchStatus'];

}
