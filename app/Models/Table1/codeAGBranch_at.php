<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AGBranchCode
 * @property string $AGBranchDesc
 * @property string $AGBranchCodeAT
 * @property string $AGBranchDescAT
 * @property string $UsrID
 * @property string $ProcDtTM
 */
class codeAGBranch_at extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'codeAGBranch_at';

    /**
     * @var array
     */
    protected $fillable = ['AGBranchCode', 'AGBranchDesc', 'AGBranchCodeAT', 'AGBranchDescAT', 'UsrID', 'ProcDtTM'];

}
