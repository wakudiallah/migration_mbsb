<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AGDeptCode
 * @property string $AGDeptDesc
 * @property string $AGDeptStatus
 */
class codeAGDept extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'codeAGDept';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'AGDeptCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['AGDeptDesc', 'AGDeptStatus'];

}
