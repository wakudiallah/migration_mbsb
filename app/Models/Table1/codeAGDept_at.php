<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AGDeptCode
 * @property string $AGDeptDesc
 * @property string $AGDeptCodeAT
 * @property string $AGDeptDescAT
 * @property string $UsrID
 * @property string $ProcDtTM
 */
class codeAGDept_at extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'codeAGDept_at';

    /**
     * @var array
     */
    protected $fillable = ['AGDeptCode', 'AGDeptDesc', 'AGDeptCodeAT', 'AGDeptDescAT', 'UsrID', 'ProcDtTM'];

}
