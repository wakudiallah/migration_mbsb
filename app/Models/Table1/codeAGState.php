<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AGStateCode
 * @property string $AGStateDesc
 * @property string $AGStateStatus
 */
class codeAGState extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'codeAGState';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'AGStateCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['AGStateDesc', 'AGStateStatus'];

}
