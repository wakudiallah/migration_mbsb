<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AcadCode
 * @property string $AcadDesc
 */
class Models/Table1/codeAcademic extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'codeAcademic';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'AcadCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['AcadDesc'];

}
