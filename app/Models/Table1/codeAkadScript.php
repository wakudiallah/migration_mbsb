<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AkadCode
 * @property string $AkadDesc
 * @property string $Active
 */
class codeAkadScript extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codeAkadScript';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'AkadCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['AkadDesc', 'Active'];

}
