<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $BumiCode
 * @property string $BumiDesc
 * @property string $BumiBNMCode
 */
class codeBumi extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'codeBumi';

    /**
     * @var array
     */
    protected $fillable = ['BumiCode', 'BumiDesc', 'BumiBNMCode'];

}
