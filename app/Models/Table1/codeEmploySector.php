<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $EmpSecCode
 * @property string $EmpSecDesc
 * @property string $BNMCode
 * @property string $EmpSecAct
 */
class codeEmploySector extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codeEmploySector';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'EmpSecCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['EmpSecDesc', 'BNMCode', 'EmpSecAct'];

}
