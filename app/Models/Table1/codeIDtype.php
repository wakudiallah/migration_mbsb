<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $IDTyCode
 * @property string $IDTyDesc
 */
class codeIDtype extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'codeIDtype';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $primaryKey = 'IDTyCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['IDTyDesc'];

}
