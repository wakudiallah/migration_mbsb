<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $InsCode
 * @property string $InsDesc
 * @property string $DisbDesc
 */
class codeIns extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'codeIns';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $primaryKey = 'InsCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['InsDesc', 'DisbDesc'];

}
