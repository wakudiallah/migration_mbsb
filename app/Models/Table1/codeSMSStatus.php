<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $SMSCode
 * @property string $SMSDesc
 */
class codeSMSStatus extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codeSMSStatus';

    /**
     * @var array
     */
    protected $fillable = ['SMSCode', 'SMSDesc'];

}
