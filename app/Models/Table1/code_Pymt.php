<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $PymtID
 * @property string $PymtDesc
 * @property string $PymtTransCode
 * @property string $PymtGLCode
 * @property string $PymtAct
 */
class code_Pymt extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'code_Pymt';

    /**
     * @var array
     */
    protected $fillable = ['PymtID', 'PymtDesc', 'PymtTransCode', 'PymtGLCode', 'PymtAct'];

}
