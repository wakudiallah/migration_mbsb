<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AppStatCode
 * @property string $AppStatDesc
 * @property string $ProcLev
 * @property string $ProcDesc
 */
class codeappstatus extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'codeappstatus';

    /**
     * @var array
     */
    protected $fillable = ['AppStatCode', 'AppStatDesc', 'ProcLev', 'ProcDesc'];

}
