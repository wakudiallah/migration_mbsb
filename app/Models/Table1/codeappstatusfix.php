<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AppStatCode
 * @property string $AppStatDesc
 */
class codeappstatusfix extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    
    protected $connection = 'sqlsrv';
    protected $table = 'codeappstatusfix';

    /**
     * @var array
     */
    protected $fillable = ['AppStatCode', 'AppStatDesc'];

}
