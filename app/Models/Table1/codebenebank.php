<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $BeneBank_Code
 * @property string $BeneBank_Name
 * @property string $Giro
 * @property string $Rentas
 * @property string $RentasI
 * @property string $Cashier_Order
 * @property string $Internal_Fund_Transfer
 * @property string $BeneBankACT
 * @property string $RentasCode
 */
class codebenebank extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codebenebank';

    /**
     * @var array
     */
    protected $fillable = ['BeneBank_Code', 'BeneBank_Name', 'Giro', 'Rentas', 'RentasI', 'Cashier_Order', 'Internal_Fund_Transfer', 'BeneBankACT', 'RentasCode'];

}
