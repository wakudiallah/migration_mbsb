<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $BeneBank_Code
 * @property string $BeneBank_Name
 * @property string $Giro
 * @property string $Rentas
 * @property string $RentasI
 * @property string $Cashier_Order
 * @property string $Internal_Fund_Transfer
 * @property string $BeneBankACT
 * @property string $BeneBank_CodeAT
 * @property string $BeneBank_NameAT
 * @property string $GiroAT
 * @property string $RentasAT
 * @property string $RentasIAT
 * @property string $Cashier_OrderAT
 * @property string $Internal_Fund_TransferAT
 * @property string $BeneBankACTAT
 * @property string $UsrID
 * @property string $ProcDtTm
 */
class codebenebank_at extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'codebenebank_at';

    /**
     * @var array
     */
    protected $fillable = ['BeneBank_Code', 'BeneBank_Name', 'Giro', 'Rentas', 'RentasI', 'Cashier_Order', 'Internal_Fund_Transfer', 'BeneBankACT', 'BeneBank_CodeAT', 'BeneBank_NameAT', 'GiroAT', 'RentasAT', 'RentasIAT', 'Cashier_OrderAT', 'Internal_Fund_TransferAT', 'BeneBankACTAT', 'UsrID', 'ProcDtTm'];

}
