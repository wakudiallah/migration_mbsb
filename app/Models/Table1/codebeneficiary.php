<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class codebeneficiary extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'codebeneficiary';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'BeneID';

    /**
     * @var array
     */
    protected $fillable = ['BeneCode', 'BeneDesc', 'BeneAct', 'BeneBank', 'BeneRegNo', 'BeneACNo', 'BenePayeeName', 'BenePaymentDesc', 'BeneCharges', 'BeneMOPCode', 'BeneMOPDesc', 'BeneTransCode', 'BeneTransDesc', 'BeneGLCode', 'BeneGLCode2', 'BeneGLDesc', 'BenePaymentTab', 'BeneClrZone', 'BeneDeliMet', 'BeneDispTo', 'BenePayTab', 'BeneAdd1', 'BeneAdd2', 'BeneAdd3', 'BeneAdd4', 'BenePkd'];


     public function codebeneficiary_to_CodeModeOfPayments() {
        return $this->belongsTo('App\Models\Table2\CodeModeOfPayments', 'BeneMOPCode','Mopcode'); 
    }

    public function codebeneficiary_to_CodeGLs() {
        return $this->belongsTo('App\Models\Table2\CodeGLs','BeneGLCode', 'GLCode'); 
    }


    public function codebeneficiary_to_CodeBeneBank() {
        return $this->belongsTo('App\Models\Table2\CodeBeneBank', 'BeneBank','BeneBankCode'); 
    }

    public function codebeneficiary_to_CodeDelimets() {
        return $this->belongsTo('App\Models\Table2\CodeDelimets', 'BeneDeliMet','DelimatCode'); 
    }

    public function codebeneficiary_to_CodeDispatchTos() {
        return $this->belongsTo('App\Models\Table2\CodeDispatchTos', 'BeneDispTo','DispToCode'); 
    }


    public function codebeneficiary_to_CodeClrZones() {
        return $this->belongsTo('App\Models\Table2\CodeClrZones', 'BeneClrZone','ClrZoneCode'); 
    }

    

}
