<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $BrCode
 * @property string $BrName
 * @property string $BrLoc
 * @property string $BrState
 * @property string $BrRegion
 * @property string $BrBOF
 * @property string $BrACNo
 * @property string $BrZone
 * @property string $BrAdd1
 * @property string $BrAdd2
 * @property string $BrAdd3
 * @property string $BrAdd4
 * @property string $Brpkd
 * @property string $FICode
 * @property string $BrAct
 * @property string $ControlUnit
 */
class codebranch extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'codebranch';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'BrCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['BrName', 'BrLoc', 'BrState', 'BrRegion', 'BrBOF', 'BrACNo', 'BrZone', 'BrAdd1', 'BrAdd2', 'BrAdd3', 'BrAdd4', 'Brpkd', 'FICode', 'BrAct', 'ControlUnit'];




    public function codebranch_to_CodeRegions() {
        return $this->belongsTo('App\Models\Table2\CodeRegions', 'BrRegion','RegionCode'); 
    }
    

}
