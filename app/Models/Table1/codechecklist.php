<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $CLCode
 * @property string $CLType
 * @property string $CLMandatory
 * @property string $CLSection
 * @property string $CLBiro
 * @property string $CDesc
 */
class codechecklist extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'codechecklist';

    /**
     * @var array
     */
    protected $fillable = ['CLMandatory', 'CLSection', 'CLBiro', 'CDesc'];


    public function codechecklist_to_CodeCollectionTypes() {
        return $this->belongsTo('App\Models\Table2\CodeCollectionType', 'CLBiro','ColCode'); 
    }

}
