<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ClrZone_Code
 * @property string $ClrZone_Desc
 * @property string $ClrZoneACT
 */
class codeclrzone extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codeclrzone';

    /**
     * @var array
     */
    protected $fillable = ['ClrZone_Code', 'ClrZone_Desc', 'ClrZoneACT'];

}
