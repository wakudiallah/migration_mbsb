<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $CMSCancel_Code
 * @property string $CMSCancel_Desc
 * @property string $CMSCancelACT
 */
class codecmscancel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codecmscancel';

    /**
     * @var array
     */
    protected $fillable = ['CMSCancel_Code', 'CMSCancel_Desc', 'CMSCancelACT'];

}
