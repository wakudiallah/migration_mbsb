<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $CoopCode
 * @property string $CoopName
 * @property string $CoopAdd1
 * @property string $CoopAdd2
 * @property string $CoopAdd3
 * @property string $CoopCity
 * @property string $CoopState
 * @property string $CoopPostcode
 * @property string $CoopTel1
 * @property string $CoopTel2
 * @property string $CoopFax
 * @property string $CoopEmail
 * @property float $CoopProfMargin
 * @property float $CoopProcessFee
 * @property float $CoopMshipFee
 * @property float $CoopMiscFee
 * @property float $CoopLegalFee
 * @property float $Shm
 * @property float $Shnm
 * @property string $ShSol
 * @property string $Deferment
 * @property float $Fd
 * @property float $COOPPer
 * @property string $COOPRegno
 * @property string $COOPAcno
 * @property string $COOPBankACNo
 * @property string $COOPCMSMOP
 * @property string $COOPNAMECMS
 */
class codecoop extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'codecoop';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'CoopCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['CoopName', 'CoopAdd1', 'CoopAdd2', 'CoopAdd3', 'CoopCity', 'CoopState', 'CoopPostcode', 'CoopTel1', 'CoopTel2', 'CoopFax', 'CoopEmail', 'CoopProfMargin', 'CoopProcessFee', 'CoopMshipFee', 'CoopMiscFee', 'CoopLegalFee', 'Shm', 'Shnm', 'ShSol', 'Deferment', 'Fd', 'COOPPer', 'COOPRegno', 'COOPAcno', 'COOPBankACNo', 'COOPCMSMOP', 'COOPNAMECMS'];

}
