<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $CoopCode
 * @property string $BrchCode
 * @property string $BrchDesc
 */
class codecoopbranch extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'codecoopbranch';

    /**
     * @var array
     */
    protected $fillable = ['BrchDesc'];


    public function codecoopbranch_to_CodeCoop() {
        return $this->belongsTo('App\Models\Table2\CodeCoop', 'CoopCode', 'CoopCode'); 
    }

}
