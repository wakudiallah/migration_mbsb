<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $DeliMet_Code
 * @property string $DeliMet_desc
 * @property string $DeliMetACT
 */
class codedelimet extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'codedelimet';

    /**
     * @var array
     */
    protected $fillable = ['DeliMet_Code', 'DeliMet_desc', 'DeliMetACT'];

}
