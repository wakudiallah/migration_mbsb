<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $DispTo_code
 * @property string $DispTo_Desc
 * @property string $DispToACT
 */
class codedispatchto extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codedispatchto';

    /**
     * @var array
     */
    protected $fillable = ['DispTo_code', 'DispTo_Desc', 'DispToACT'];

}
