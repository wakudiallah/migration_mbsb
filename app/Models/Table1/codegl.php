<?php


namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AcadCode
 * @property string $AcadDesc
 */
class codegl extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'codegl';



    protected $fillable = ["GLCode","TransType","GLDesc"];


    public function data_codegl() {
        return $this->belongsTo('App\Models\Table2\CodeTransaction', 'TransType','TransCode'); 
    }


    /*public function supplier() {
        return $this->belongsTo('App\Model\ParameterSupplier', 'supplier_id','id'); 
    }*/




}
