<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Incvcode
 * @property string $IncvDesc
 * @property string $IncvAct
 * @property float $IncvMinLnAmt
 * @property float $IncvMaxLnAmt
 * @property string $IncvMode
 * @property float $IncvPayment
 * @property string $IncvProductCode
 * @property string $IncvproductDesc
 * @property string $RbMO
 */
class codeincentive extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codeincentive';

    /**
     * @var array
     */
    protected $fillable = ['Incvcode', 'IncvDesc', 'IncvAct', 'IncvMinLnAmt', 'IncvMaxLnAmt', 'IncvMode', 'IncvPayment', 'IncvProductCode', 'IncvproductDesc', 'RbMO'];

}
