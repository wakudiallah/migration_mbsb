<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class codeintpackage extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codeintpackage';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'LnPackCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['LnPackDesc', 'MinYr', 'MaxYr', 'IntRate', 'LnLimit', 'IntAct', 'LnProcessFee', 'PFeeFlag', 'LnInsType', 'LnInsLife', 'InsLifeFlag', 'LnSecDep', 'LnOASearch', 'OAFlag', 'LnARB', 'ARBFlag', 'LnStampDuty', 'SDFlag', 'LnStampLOLG', 'LOLGFlag', 'LnLetterGua', 'LGFlag', 'LnMemoDep', 'MDFlag', 'LnSetOff', 'SOFlag', 'Entitle', 'EntitleFlag', 'Wadiah', 'WadiahFlag', 'Teman', 'TemanFlag', 'MultInst', 'InsDesc', 'MinAge', 'MaxAge', 'BeneGLCode2', 'FBInsType', 'FBProcFee', 'FBInsLife', 'FBSecDep', 'FBOA', 'FBARB', 'FBStamp', 'FBLOLG', 'FBLettGua', 'FBMemoDep', 'FBSetOff', 'FBWadiah', 'FBTeman', 'IBGFee', 'IBGFlag', 'FBIBG', 'TypeWaslah', 'FBISave', 'FinanceConcept', 'MultiRate', 'IntRate2', 'DSRFlag', 'TypeScript', 'GSTRate', 'GSTTaxCode', 'GSTChgTy', 'GSTRate_ProcessFee', 'GSTTaxCode_ProcessFee', 'GSTChgTy_ProcessFee', 'GSTRate_StampDuty', 'GSTTaxCode_StampDuty', 'GSTChgTy_StampDuty', 'GSTRate_StampLOLG', 'GSTTaxCode_StampLOLG', 'GSTChgTy_StampLOLG', 'GSTRate_SetOff', 'GSTTaxCode_SetOff', 'GSTChgTy_SetOff', 'GSTRate_IBGFee', 'GSTTaxCode_IBGFee', 'GSTChgTy_IBGFee', 'GSTRate_Ins', 'GSTTaxCode_Ins', 'GSTChgTy_Ins', 'GSTRate_OA', 'GSTTaxCode_OA', 'GSTChgTy_OA', 'GSTRate_Wadiah', 'GSTTaxCode_Wadiah', 'GSTChgTy_Wadiah', 'GSTRate_Teman', 'GSTTaxCode_Teman', 'GSTChgTy_Teman', 'MinRate', 'ManualPM', 'BaseRateCode', 'MiCOBBaseRate', 'MiCOBTyPrice'];



    public function codeintpackage_to_CodeLoanPackages() {
        return $this->belongsTo('App\Models\Table2\CodeLoanPackages', 'LnPackCode','LnPackCode'); 
    }



}
