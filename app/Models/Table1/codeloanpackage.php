<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $LnPackCode
 * @property string $LnProdCode
 * @property string $LnPackDesc
 * @property string $LnPackACT
 * @property string $SIBSCode
 * @property string $FacilityCode
 * @property string $BiroIndc
 * @property string $SIBSCode2
 * @property string $MultiRate
 */
class codeloanpackage extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codeloanpackage';

    /**
     * The primary key for the model.
     * 
     

    /**
     * @var array
     */
    protected $fillable = ['LnProdCode', 'LnPackDesc', 'LnPackACT', 'SIBSCode', 'FacilityCode', 'BiroIndc', 'SIBSCode2', 'MultiRate'];



    public function codeloanpackage_to_CodeProducts() {
        return $this->belongsTo('App\Models\Table2\CodeProducts' , 'LnProdCode', 'ProdCode'); 
    }







}
