<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Mopcode
 * @property string $MopDesc
 * @property string $MopAct
 * @property float $MopMinAmt
 * @property float $MopMaxAmt
 * @property string $MopBnkACNo
 * @property string $MOPColName
 * @property string $MOPColIC
 */
class codemodeofpayment extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codemodeofpayment';

    /**
     * @var array
     */
    protected $fillable = ['Mopcode', 'MopDesc', 'MopAct', 'MopMinAmt', 'MopMaxAmt', 'MopBnkACNo', 'MOPColName', 'MOPColIC'];

}
