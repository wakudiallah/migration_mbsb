<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $OffID
 * @property string $OffName
 * @property string $OffBr
 * @property string $OffDept
 * @property string $OffBank
 * @property string $OffAcNo
 * @property string $OffEntity
 * @property string $CSUFlag
 * @property string $MoAQSFlag
 * @property string $Activate
 */
class codemofficer extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codemofficer';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'OffID';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['OffName', 'OffBr', 'OffDept', 'OffBank', 'OffAcNo', 'OffEntity', 'CSUFlag', 'MoAQSFlag', 'Activate'];

    public function codemofficer_to_CodeBranches() {
        return $this->belongsTo('App\Models\Table2\CodeBranches', 'OffBr','BrCode'); 
    }


    public function codemofficer_to_codeDepartments() {
        return $this->belongsTo('App\Models\Table2\codeDepartments', 'OffDept','DeptCode'); 
    }

    public function codemofficer_to_CodeBanks() {
        return $this->belongsTo('App\Models\Table2\CodeBanks', 'OffBank','BankCode'); 
    }

    public function codemofficer_to_codeEntities() {
        return $this->belongsTo('App\Models\Table2\codeEntities', 'OffEntity','EntCode'); 
    }



}
