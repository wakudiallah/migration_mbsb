<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Rmopcode
 * @property string $RmopDesc
 * @property string $RmopAct
 * @property string $RMopCMS
 * @property string $RMopClrZone
 * @property string $RMopDeliMet
 * @property string $RMopDispTo
 * @property string $RMopColName
 * @property string $RMopColIC
 */
class codemopredemp extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'codemopredemp';

    /**
     * @var array
     */
    protected $fillable = ['Rmopcode', 'RmopDesc', 'RmopAct', 'RMopCMS', 'RMopClrZone', 'RMopDeliMet', 'RMopDispTo', 'RMopColName', 'RMopColIC'];


    public function codemopredemp_to_CodeModeOfPayments() {
        return $this->belongsTo('App\Models\Table2\CodeModeOfPayments', 'RMopCMS', 'Mopcode'); 
    }

    public function codemopredemp_to_CodeDelimets() {
        return $this->belongsTo('App\Models\Table2\CodeDelimets', 'RMopDeliMet', 'DelimatCode'); 
    }


    public function codemopredemp_to_CodeDispatchTos() {
        return $this->belongsTo('App\Models\Table2\CodeDispatchTos', 'DispToCode', 'DelimatCode'); 
    }


    public function codemopredemp_to_CodeClrZones() {
        return $this->belongsTo('App\Models\Table2\CodeClrZones', 'RMopClrZone', 'ClrZoneCode'); 
    }



}
