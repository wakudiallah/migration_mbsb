<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $NOBCode
 * @property string $NOBDesc
 * @property string $NOBAct
 */
class codenaofbuss extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codenaofbuss';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'NOBCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['NOBDesc', 'NOBAct'];

}
