<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $NegCode
 * @property string $NegDesc
 * @property string $BNMCODE
 */
class codenegeri extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codenegeri';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'NegCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['NegDesc', 'BNMCODE'];

}
