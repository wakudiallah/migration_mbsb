<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $PenReasCode
 * @property string $PendReasDesc
 */
class codependingreas extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $primaryKey = 'PenReasCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['PendReasDesc'];

}
