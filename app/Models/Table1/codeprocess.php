<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ProcCode
 * @property integer $ProcLev
 * @property string $ProcDesc
 * @property int $ProcTime
 */
class codeprocess extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codeprocess';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'ProcLev';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['ProcCode', 'ProcDesc', 'ProcTime'];

}
