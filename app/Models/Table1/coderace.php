<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $RaceCode
 * @property string $RaceDesc
 * @property string $RaceAct
 */
class coderace extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'coderace';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'RaceCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['RaceDesc', 'RaceAct'];

}
