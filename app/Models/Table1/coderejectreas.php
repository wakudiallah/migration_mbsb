<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $RejReasCode
 * @property string $RejReasDesc
 * @property string $SMSFlag
 * @property string $BNMREJCODE
 * @property string $BNMSTATUS
 */
class coderejectreas extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $primaryKey = 'RejReasCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['RejReasDesc', 'SMSFlag', 'BNMREJCODE', 'BNMSTATUS'];

}
