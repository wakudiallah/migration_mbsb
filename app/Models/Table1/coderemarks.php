<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $RemarksCode
 * @property string $RemarksDesc
 */
class coderemarks extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $primaryKey = 'RemarksCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['RemarksDesc'];

}
