<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;


class codesettlepty extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'codesettlepty';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'SetPtyCode';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['SetPtyType', 'SetPtyAbbrv', 'SetPtyDesc', 'SetPtyBank_Name', 'SetPtyRedemptCode', 'SetPtybene_Name', 'SetPtyPaymentDesc', 'SetPtyMBSMOP_Code', 'SetPtyMBSGL_Code', 'SetPtyCMSMOP_Code', 'SetPtyACT', 'SetPtyAdd1', 'SetPtyAdd2', 'SetPtyAdd3', 'SetPtyAdd4', 'SetptyPkd', 'SetRegNo', 'SetPtyBankAcc', 'CSUFlag'];



    public function codesettlepty_to_CodeModeOfPayments() {
        return $this->belongsTo('App\Models\Table2\CodeModeOfPayments', 'Mopcode','SetPtyCMSMOP_Code'); 
    }


    public function codesettlepty_to_CodeGLs() {
        return $this->belongsTo('App\Models\Table2\CodeGLs', 'SetPtyMBSGL_Code', 'GLCode'); 
    }


    public function codesettlepty_to_CodeTransaction() {
        return $this->belongsTo('App\Models\Table2\CodeTransaction', 'SetPtyMBSGL_Code', 'TransCode'); 
    }


    public function codesettlepty_to_CodeBeneBank() {
        return $this->belongsTo('App\Models\Table2\CodeBeneBank', 'SetPtyBank_Name', 'BeneBankCode'); 
    }


    public function codesettlepty_to_CodeMopRedemps() {
        return $this->belongsTo('App\Models\Table2\CodeMopRedemps', 'SetPtyRedemptCode', 'RMopCode'); 
    }





}
