<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Applrefno
 * @property string $MsgSend
 */
class iSMS_InComp extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'iSMS_InComp';

    /**
     * @var array
     */

    public function iSMS_InComp_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','Applrefno', 'ApplRefNo'); 
    }
   

}
