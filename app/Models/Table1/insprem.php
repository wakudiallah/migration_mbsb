<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property float $LoanAmt
 * @property float $PremAmt
 * @property float $LoanAmtMax
 * @property string $Tenor
 */
class insprem extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'insprem';

    

}
