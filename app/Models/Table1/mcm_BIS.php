<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $No
 * @property string $ApplRefNo
 * @property string $TransactionCode
 * @property string $DateTime
 * @property string $OriginatorCode
 * @property string $ReferenceNo
 * @property string $ResponseDescription
 * @property string $TagCode
 * @property string $TagDescription
 * @property string $EffectiveDate
 * @property string $ExpiryDate
 * @property string $Remarks
 * @property string $LastUpdateBy
 * @property string $LastUpdateDateTime
 * @property string $BISIndc
 * @property string $Status
 * @property string $DateSend
 * @property string $DateReceive
 */
class mcm_BIS extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'mcm_BIS';

    /**
     * The primary key for the model.
     * 
     * @var string
     
    protected $primaryKey = 'No'; */

    /**
     * @var array
     
    protected $fillable = ['ApplRefNo', 'TransactionCode', 'DateTime', 'OriginatorCode', 'ReferenceNo', 'ResponseDescription', 'TagCode', 'TagDescription', 'EffectiveDate', 'ExpiryDate', 'Remarks', 'LastUpdateBy', 'LastUpdateDateTime', 'BISIndc', 'Status', 'DateSend', 'DateReceive'];
    */

}
