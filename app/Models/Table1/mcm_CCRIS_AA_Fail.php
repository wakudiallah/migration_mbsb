<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $RefNo
 * @property string $ICNO
 * @property string $CUSTNAME
 * @property string $Envelope
 * @property string $Flag
 * @property string $FailDateTime
 * @property string $ResendDateTime
 * @property string $ERRLOG
 */
class mcm_CCRIS_AA_Fail extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'mcm_CCRIS_AA_Fail';

    /**
     * @var array
     
    protected $fillable = ['RefNo', 'ICNO', 'CUSTNAME', 'Envelope', 'Flag', 'FailDateTime', 'ResendDateTime', 'ERRLOG'];*/

}
