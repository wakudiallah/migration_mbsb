<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $No
 * @property string $ApplRefNo
 * @property string $TransactionCode
 * @property string $DateTime
 * @property string $OriginatorCode
 * @property string $ReferenceNo
 * @property string $ResponseDescription
 * @property string $UserID
 * @property string $UserGroup
 * @property string $VersionNo
 * @property string $RespTimestamp
 * @property string $StatusCode
 * @property string $ReservedField
 * @property string $AppKey
 * @property string $RefNo
 * @property string $Errors
 * @property string $Status
 * @property string $DateSend
 * @property string $DateReceive
 * @property string $StpReqID
 * @property string $OriginReqIDNo
 */
class mcm_ccrisAA_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisAA_1';

    /**
     * @var array
    
    protected $fillable = ['No', 'ApplRefNo', 'TransactionCode', 'DateTime', 'OriginatorCode', 'ReferenceNo', 'ResponseDescription', 'UserID', 'UserGroup', 'VersionNo', 'RespTimestamp', 'StatusCode', 'ReservedField', 'AppKey', 'RefNo', 'Errors', 'Status', 'DateSend', 'DateReceive', 'StpReqID', 'OriginReqIDNo']; */

}
