<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $No
 * @property string $ApplRefNo
 * @property string $StpReqID
 * @property string $OriginReqIDNo
 * @property string $BnmEntitySeqNo
 * @property string $BnmEntityName
 * @property string $BnmEntityIDNo
 * @property string $BnmEntityDateBR
 * @property string $BnmEntityNationality
 * @property string $BnmEntityGroup
 * @property string $BnmEntityRegistered
 * @property string $BnmEntityKey
 * @property string $DateSend
 * @property string $SendFlag
 * @property string $ConfirmFlag
 * @property string $BnmEntityMember
 * @property string $BnmEntityCorporateStatus
 * @property string $BnmEntityIndustrialSector
 * @property string $BnmEntityResidencyStatus
 * @property string $BnmEntityWarning
 * @property string $BnmEntityIDNo2
 * @property string $BnmEntityIncome
 * @property string $BnmEntityGender
 * @property string $BnmEntityOccupation
 * @property string $BnmEntityEmpName
 * @property string $BnmEntityEmpSec
 * @property string $BnmEntityEmpTyp
 * @property string $BnmEntityPostcode
 * @property string $BnmEntityState
 * @property string $BnmEntityCountry
 * @property string $BnmEntityEDBID
 * @property string $BnmEntityVerifiedFlag
 * @property string $BnmEntityStatus
 * @property string $BnmEntityAssignedNumber
 */
class mcm_ccrisAA_MultipleEntities extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisAA_MultipleEntities';

    /**
     * The primary key for the model.
     * 
     * @var string
 
    protected $primaryKey = 'No';*/

    /**
     * @var array
     
    protected $fillable = ['ApplRefNo', 'StpReqID', 'OriginReqIDNo', 'BnmEntitySeqNo', 'BnmEntityName', 'BnmEntityIDNo', 'BnmEntityDateBR', 'BnmEntityNationality', 'BnmEntityGroup', 'BnmEntityRegistered', 'BnmEntityKey', 'DateSend', 'SendFlag', 'ConfirmFlag', 'BnmEntityMember', 'BnmEntityCorporateStatus', 'BnmEntityIndustrialSector', 'BnmEntityResidencyStatus', 'BnmEntityWarning', 'BnmEntityIDNo2', 'BnmEntityIncome', 'BnmEntityGender', 'BnmEntityOccupation', 'BnmEntityEmpName', 'BnmEntityEmpSec', 'BnmEntityEmpTyp', 'BnmEntityPostcode', 'BnmEntityState', 'BnmEntityCountry', 'BnmEntityEDBID', 'BnmEntityVerifiedFlag', 'BnmEntityStatus', 'BnmEntityAssignedNumber'];*/

}
