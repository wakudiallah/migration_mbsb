<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $No
 * @property string $ApplRefNo
 * @property string $ApplRefNo1
 * @property string $TransactionCode
 * @property string $DateTime
 * @property string $OriginatorCode
 * @property string $ReferenceNo
 * @property string $ResponseDescription
 * @property string $UserID
 * @property string $UserGroup
 * @property string $VersionNo
 * @property string $RespTimestamp
 * @property string $StatusCode
 * @property string $ReservedField
 * @property string $RefNo
 * @property string $Errors
 * @property string $Status
 * @property string $DateSend
 * @property string $DateReceive
 */
class mcm_ccrisAU extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisAU';

    /**
     * The primary key for the model.
     * 
     * @var string
    
    protected $primaryKey = 'No'; */

    /**
     * @var array
    
    protected $fillable = ['ApplRefNo', 'ApplRefNo1', 'TransactionCode', 'DateTime', 'OriginatorCode', 'ReferenceNo', 'ResponseDescription', 'UserID', 'UserGroup', 'VersionNo', 'RespTimestamp', 'StatusCode', 'ReservedField', 'RefNo', 'Errors', 'Status', 'DateSend', 'DateReceive'];*/

}
