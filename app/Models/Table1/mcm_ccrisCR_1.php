<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $No
 * @property string $ApplRefNo
 * @property string $TransactionCode
 * @property string $DateTime
 * @property string $OriginatorCode
 * @property string $ReferenceNo
 * @property string $ResponseDescription
 * @property string $UserID
 * @property string $UserGroup
 * @property string $VersionNo
 * @property string $RespTimestamp
 * @property string $ReservedField
 * @property string $RefNo
 * @property string $IDNo
 * @property string $IDNo2
 * @property string $IDICName
 * @property string $IDICGroup
 * @property string $IDICDateBR
 * @property string $IDICNationality
 * @property string $EntWarnings
 * @property string $Status
 * @property string $DateSend
 * @property string $DateReceive
 * @property string $ReportType
 * @property string $EntKey
 * @property string $StatusCode
 * @property string $Errors
 */
class mcm_ccrisCR_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisCR_1';

    /**
     * @var array
    

    protected $fillable = ['No', 'ApplRefNo', 'TransactionCode', 'DateTime', 'OriginatorCode', 'ReferenceNo', 'ResponseDescription', 'UserID', 'UserGroup', 'VersionNo', 'RespTimestamp', 'ReservedField', 'RefNo', 'IDNo', 'IDNo2', 'IDICName', 'IDICGroup', 'IDICDateBR', 'IDICNationality', 'EntWarnings', 'Status', 'DateSend', 'DateReceive', 'ReportType', 'EntKey', 'StatusCode', 'Errors'];*/

}
