<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $StatusCode
 * @property string $Errors
 * @property string $Messages
 * @property string $Data_FromYear
 * @property string $Data_ToYear
 * @property string $Data_M1
 * @property string $Data_M2
 * @property string $Data_M3
 * @property string $Data_M4
 * @property string $Data_M5
 * @property string $Data_M6
 * @property string $Data_M7
 * @property string $Data_M8
 * @property string $Data_M9
 * @property string $Data_M10
 * @property string $Data_M11
 * @property string $Data_M12
 * @property string $LimTot
 * @property string $BalTot
 * @property string $SpecialName
 * @property string $RptRetrieveFromBNM
 * @property string $RptRetrievalDate
 */
class mcm_ccrisCR_DetailRpt_Data extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisCR_DetailRpt_Data';

    /**
     * @var array
     
    protected $fillable = ['ApplRefNo', 'StatusCode', 'Errors', 'Messages', 'Data_FromYear', 'Data_ToYear', 'Data_M1', 'Data_M2', 'Data_M3', 'Data_M4', 'Data_M5', 'Data_M6', 'Data_M7', 'Data_M8', 'Data_M9', 'Data_M10', 'Data_M11', 'Data_M12', 'LimTot', 'BalTot', 'SpecialName', 'RptRetrieveFromBNM', 'RptRetrievalDate']; */

}
