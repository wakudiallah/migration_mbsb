<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $RowNo
 * @property string $MdtaID
 * @property string $Date
 * @property string $STS
 * @property string $LenderType
 * @property string $MyFgn
 * @property string $Capacity
 * @property string $Facility
 * @property string $Balance
 * @property string $BalanceDate
 * @property string $Limit
 * @property string $RepayTerm
 * @property string $ColType
 * @property string $M1
 * @property string $M2
 * @property string $M3
 * @property string $M4
 * @property string $M5
 * @property string $M6
 * @property string $M7
 * @property string $M8
 * @property string $M9
 * @property string $M10
 * @property string $M11
 * @property string $M12
 * @property string $Legal
 * @property string $LegalDate
 */
class mcm_ccrisCR_DetailRpt_SpecialAttn extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisCR_DetailRpt_SpecialAttn';

    /**
     * @var array
     
    protected $fillable = ['ApplRefNo', 'RowNo', 'MdtaID', 'Date', 'STS', 'LenderType', 'MyFgn', 'Capacity', 'Facility', 'Balance', 'BalanceDate', 'Limit', 'RepayTerm', 'ColType', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12', 'Legal', 'LegalDate'];*/

}
