<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $StatusCode
 * @property string $Errors
 * @property string $Messages
 * @property string $EntWarnings
 * @property string $AppCnt
 * @property string $AppAmt
 * @property string $PndCnt
 * @property string $PndAmt
 * @property string $AccOut
 * @property string $AccTot
 * @property string $AccFec
 * @property string $CfgTot
 * @property string $CfgFec
 * @property string $Tot
 * @property string $Fec
 * @property string $Legal
 * @property string $SpecAttn
 * @property string $RptRetrieveFromBNM
 * @property string $RptRetrievalDate
 */
class mcm_ccrisCR_SummaryRpt_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisCR_SummaryRpt_1';

    /**
     * @var array
    protected $fillable = ['ApplRefNo', 'StatusCode', 'Errors', 'Messages', 'EntWarnings', 'AppCnt', 'AppAmt', 'PndCnt', 'PndAmt', 'AccOut', 'AccTot', 'AccFec', 'CfgTot', 'CfgFec', 'Tot', 'Fec', 'Legal', 'SpecAttn', 'RptRetrieveFromBNM', 'RptRetrievalDate'];     */

}
