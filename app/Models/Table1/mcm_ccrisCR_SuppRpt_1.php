<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $StatusCode
 * @property string $Errors
 * @property string $Messages
 * @property string $SpecialName
 * @property string $RptRetrieveFromBNM
 * @property string $RptRetrievalDate
 */
class mcm_ccrisCR_SuppRpt_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisCR_SuppRpt_1';

    /**
     * @var array
     
    protected $fillable = ['ApplRefNo', 'StatusCode', 'Errors', 'Messages', 'SpecialName', 'RptRetrieveFromBNM', 'RptRetrievalDate'];*/

}
