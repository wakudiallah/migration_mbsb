<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $Entity
 * @property string $Address1
 * @property string $Address2
 * @property string $Address3
 * @property string $Address4
 * @property string $Address5
 * @property string $Address6
 * @property string $LongAddress
 * @property string $State_Code
 * @property string $ISO_Country
 * @property string $Post_Code
 * @property string $Org_Code
 * @property string $Branch
 * @property string $Application
 * @property string $Create_Date
 */
class mcm_ccrisCR_SuppRpt_Address extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisCR_SuppRpt_Address';

    /**
     * @var array
    protected $fillable = ['ApplRefNo', 'Entity', 'Address1', 'Address2', 'Address3', 'Address4', 'Address5', 'Address6', 'LongAddress', 'State_Code', 'ISO_Country', 'Post_Code', 'Org_Code', 'Branch', 'Application', 'Create_Date'];*/

}
