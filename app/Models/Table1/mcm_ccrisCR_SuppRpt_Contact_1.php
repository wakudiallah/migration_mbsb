<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $Contact_Entity
 * @property string $Contact_Type
 * @property string $ISD_Code
 * @property string $Prefix
 * @property string $Phone_no
 * @property string $Contact_Org_code
 * @property string $Contact_Branch
 * @property string $Contact_Application
 * @property string $Contact_Create_Date
 */
class mcm_ccrisCR_SuppRpt_Contact_1 extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisCR_SuppRpt_Contact_1';

    /**
     * @var array
     
    protected $fillable = ['ApplRefNo', 'Contact_Entity', 'Contact_Type', 'ISD_Code', 'Prefix', 'Phone_no', 'Contact_Org_code', 'Contact_Branch', 'Contact_Application', 'Contact_Create_Date'];*/

}
