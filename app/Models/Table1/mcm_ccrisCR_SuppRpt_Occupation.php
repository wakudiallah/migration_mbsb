<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $Occ_Entity
 * @property string $Occ_type
 * @property string $Occupation
 * @property string $Employer
 * @property string $Grs_income
 * @property string $Freq_code
 * @property string $Currency
 * @property string $from_Date
 * @property string $to_Date
 * @property string $Occ_Org_code
 * @property string $Occ_Branch
 * @property string $Occ_Application
 * @property string $Occ_Create_Date
 */
class mcm_ccrisCR_SuppRpt_Occupation extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'mcm_ccrisCR_SuppRpt_Occupation';

    /**
     * @var array
    protected $fillable = ['ApplRefNo', 'Occ_Entity', 'Occ_type', 'Occupation', 'Employer', 'Grs_income', 'Freq_code', 'Currency', 'from_Date', 'to_Date', 'Occ_Org_code', 'Occ_Branch', 'Occ_Application', 'Occ_Create_Date']; */

}
