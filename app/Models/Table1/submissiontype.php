<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $AcID
 * @property string $CustNewIDNo
 * @property float $LoanAmt
 * @property string $SubType
 */
class submissiontype extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv';
    protected $table = 'submissiontype';

    /**
     * The primary key for the model.
     * 
     * @var string
     */


    public function submissiontype_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','AcID', 'AcId'); 
    }


}


