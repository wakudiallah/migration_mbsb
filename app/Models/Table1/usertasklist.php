<?php

namespace App\Models\Table1;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplRefNo
 * @property string $UsrID
 * @property string $StRouteTm
 * @property string $StRouteDt
 */
class usertasklist extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */


    protected $connection = 'sqlsrv';
    protected $table = 'usertasklist';



    public function usertasklist_to_ApplRefNoTabTemps() {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps','ApplRefNo', 'ApplRefNo'); 
    }

   

}
