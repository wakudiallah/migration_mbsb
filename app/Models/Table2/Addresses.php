<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;


class Addresses extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'Addresses';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'NewIdtype', 'NewIdno', 'AddNo', 'AddType', 'AddOship', 'Add1', 'Add2', 'Add3', 'AddCity', 'AddState', 'AddCtry', 'AddPcode', 'AddTel1', 'AddExt1', 'AddTel2', 'AddExt2', 'AddTel3', 'AddExt3', 'Lmsflag', 'UploadDt', 'ApplRefNoTabTempId'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applRefNoTabTemp()
    {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemp', 'ApplRefNoTabTempId', 'Id');
    }
}
