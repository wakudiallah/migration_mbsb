<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;


class AdminTaskLists extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv2';
    protected $table = 'AdminTaskLists';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['CreatedBy', 'CreatedDate', 'Act', 'TaskFor', 'NewUserId', 'NewUserName', 'TaskStatus', 'StartRouteDate', 'StartProcessDate', 'EndProcessDate', 'TotProcTime', 'Remarks', 'NewFullName'];

}
