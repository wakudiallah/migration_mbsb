<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $ApplId
 * @property string $ApplRefNo
 * @property string $LoanAccNo
 * @property float $Osbal
 * @property float $Loamt
 * @property float $ActAmt
 * @property float $GltransAmt
 * @property string $Acno
 * @property string $TransDtTm
 * @property string $TransBrCode
 * @property string $TransCodeCo
 * @property string $TransCodeAtt
 * @property string $TransCodeRct
 * @property string $TransCodeGtt
 * @property string $TransUsrId
 * @property string $GltransAmt2
 * @property float $TransAmt
 * @property string $TransAmtRef
 * @property string $TransAmtDesc
 * @property string $AuthUsrId
 * @property float $TransAmtCo
 * @property float $TransAmtAtt
 * @property float $TransAmtRct
 * @property float $TransAmtGtt
 * @property string $CodrDate
 * @property string $PayeeTy
 * @property string $PayeeName
 * @property string $Glacco
 * @property string $Glacatt
 * @property string $Glacrct
 * @property string $Glacgtt
 * @property string $HoldDisb
 * @property string $UploadDt
 * @property float $Inc
 * @property float $Postage
 * @property float $Cccharge
 * @property string $ReqDis
 * @property float $Netloan
 * @property string $StampFee
 * @property string $DisbDeferment
 * @property string $DisbAdInst
 * @property string $Disbtpc
 * @property string $Disbtpcno
 * @property string $Disbshm
 * @property string $TranscodeGtt1
 * @property string $TranscodeGtt2
 * @property float $TransAmtGtt1
 * @property float $TransAmtGtt2
 * @property string $Glacgtt1
 * @property string $Glacgtt2
 * @property string $CancelTransact
 * @property string $ReleaseDt
 * @property string $Lmsflag
 * @property float $NetBal
 * @property string $GlcnetBal
 * @property string $NetBalDesc
 * @property string $NetBalRef
 * @property string $TransCodeStamp
 * @property string $Glcstamp
 * @property float $StampDuty
 * @property string $StampDesc
 * @property string $StampRef
 * @property string $Glcstamp2
 * @property string $TransCodeSd
 * @property string $Glcsd
 * @property float $SecDep
 * @property string $SecDepDesc
 * @property string $SecDepRef
 * @property string $Glcsd2
 * @property string $TransCodeProcFee
 * @property string $GlcprocFee
 * @property float $ProcFee
 * @property string $ProcFeeDesc
 * @property string $ProcFeeRef
 * @property string $GlcprocFee2
 * @property string $TransCodeIns
 * @property string $Glcins
 * @property float $Insurans
 * @property string $InsDesc
 * @property string $InsRef
 * @property string $Glcins2
 * @property string $TransCodeMdta
 * @property string $Glcmdta
 * @property float $Mdta
 * @property string $Mdtadesc
 * @property string $Mdtaref
 * @property string $Glcmdta2
 * @property string $TransCodeIbgfee
 * @property string $Glcibgfee
 * @property float $Ibgfee
 * @property string $Ibgdesc
 * @property string $Ibgref
 * @property string $Glcibgfee2
 * @property string $TransCodeOa
 * @property string $Glcoa
 * @property float $Oasearch
 * @property string $Oadesc
 * @property string $Oaref
 * @property string $Glcoa2
 * @property string $TransCodeArb
 * @property string $Glcarb
 * @property float $Arb
 * @property string $Arbdesc
 * @property string $Arbref
 * @property string $Glcarb2
 * @property string $TransCodeCommArb
 * @property string $GlccommArb
 * @property float $CommArb
 * @property string $CommArbdesc
 * @property string $CommArbref
 * @property string $GlccommArb2
 * @property string $TranscodeWadiah
 * @property string $Glcwadiah
 * @property float $Wadiah
 * @property string $WadiahDesc
 * @property string $WadiahRef
 * @property string $Glcwadiah2
 * @property string $TranscodeTeman
 * @property string $Glcteman
 * @property float $Teman
 * @property string $TemanDesc
 * @property string $TemanRef
 * @property string $Glcteman2
 * @property string $PayMode
 * @property string $PayBank
 * @property string $Arbcode
 * @property string $WadiahFlag
 * @property string $WadBranch
 * @property string $TemanFlag
 * @property string $ReimbFee
 * @property string $IsaveFlag
 * @property string $TranscodeIsave
 * @property string $Glcisave
 * @property float $Isave
 * @property string $IsaveDesc
 * @property string $IsaveRef
 * @property string $Glcisave2
 * @property string $WakalahFlag
 * @property string $TranscodeWakalah
 * @property string $Glcwakalah
 * @property float $Wakalah
 * @property string $WakalahDesc
 * @property string $WakalahRef
 * @property string $Glcwakalah2
 * @property string $LiveFlag
 * @property string $Csuflag
 * @property string $ApplRefNo2
 * @property string $LoanAccNo2
 * @property float $Netloan2
 * @property float $TransAmt2
 * @property float $NetBal2
 * @property string $AdminFeeFlag
 * @property string $TranscodeAdminFee
 * @property string $GlcadminFee
 * @property float $AdminFee
 * @property string $AdminFeeDesc
 * @property string $AdminFeeRef
 * @property string $GlcadminFee2
 * @property string $RepayTy
 * @property float $GprocFee
 * @property float $GstampDuty
 * @property float $Glolg
 * @property float $Ginsurance
 * @property float $GotherFee
 * @property float $GinsLife
 * @property float $Goa
 * @property float $Garb
 * @property float $GletterGua
 * @property float $GmemoDep
 * @property float $GsetOff
 * @property float $Gwadiah
 * @property float $Gteman
 * @property float $GsecDep
 * @property float $Gmdta
 * @property float $Gibgfee
 * @property float $Goasearch
 * @property float $GcommArb
 * @property float $Gisave
 * @property float $Gwakalah
 * @property float $GadminFee
 * @property string $GtaxCodeProcFee
 * @property string $GtaxCodeStampDuty
 * @property string $GtaxCodeInsurance
 * @property string $GtaxCodeArb
 * @property string $GtaxCodeWadiah
 * @property string $GtaxCodeTeman
 * @property string $GtaxCodeSecDep
 * @property string $GtaxCodeMdta
 * @property string $GtaxCodeIbgfee
 * @property string $GtaxCodeOasearch
 * @property string $GtaxCodeCommArb
 * @property string $GtaxCodeIsave
 * @property string $GtaxCodeWakalah
 * @property string $GtaxCodeAdminFee
 * @property string $InsCode
 */
class ApplDisburses extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'ApplDisburses';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'ApplRefNo', 'LoanAccNo', 'Osbal', 'Loamt', 'ActAmt', 'GltransAmt', 'Acno', 'TransDtTm', 'TransBrCode', 'TransCodeCo', 'TransCodeAtt', 'TransCodeRct', 'TransCodeGtt', 'TransUsrId', 'GltransAmt2', 'TransAmt', 'TransAmtRef', 'TransAmtDesc', 'AuthUsrId', 'TransAmtCo', 'TransAmtAtt', 'TransAmtRct', 'TransAmtGtt', 'CodrDate', 'PayeeTy', 'PayeeName', 'Glacco', 'Glacatt', 'Glacrct', 'Glacgtt', 'HoldDisb', 'UploadDt', 'Inc', 'Postage', 'Cccharge', 'ReqDis', 'Netloan', 'StampFee', 'DisbDeferment', 'DisbAdInst', 'Disbtpc', 'Disbtpcno', 'Disbshm', 'TranscodeGtt1', 'TranscodeGtt2', 'TransAmtGtt1', 'TransAmtGtt2', 'Glacgtt1', 'Glacgtt2', 'CancelTransact', 'ReleaseDt', 'Lmsflag', 'NetBal', 'GlcnetBal', 'NetBalDesc', 'NetBalRef', 'TransCodeStamp', 'Glcstamp', 'StampDuty', 'StampDesc', 'StampRef', 'Glcstamp2', 'TransCodeSd', 'Glcsd', 'SecDep', 'SecDepDesc', 'SecDepRef', 'Glcsd2', 'TransCodeProcFee', 'GlcprocFee', 'ProcFee', 'ProcFeeDesc', 'ProcFeeRef', 'GlcprocFee2', 'TransCodeIns', 'Glcins', 'Insurans', 'InsDesc', 'InsRef', 'Glcins2', 'TransCodeMdta', 'Glcmdta', 'Mdta', 'Mdtadesc', 'Mdtaref', 'Glcmdta2', 'TransCodeIbgfee', 'Glcibgfee', 'Ibgfee', 'Ibgdesc', 'Ibgref', 'Glcibgfee2', 'TransCodeOa', 'Glcoa', 'Oasearch', 'Oadesc', 'Oaref', 'Glcoa2', 'TransCodeArb', 'Glcarb', 'Arb', 'Arbdesc', 'Arbref', 'Glcarb2', 'TransCodeCommArb', 'GlccommArb', 'CommArb', 'CommArbdesc', 'CommArbref', 'GlccommArb2', 'TranscodeWadiah', 'Glcwadiah', 'Wadiah', 'WadiahDesc', 'WadiahRef', 'Glcwadiah2', 'TranscodeTeman', 'Glcteman', 'Teman', 'TemanDesc', 'TemanRef', 'Glcteman2', 'PayMode', 'PayBank', 'Arbcode', 'WadiahFlag', 'WadBranch', 'TemanFlag', 'ReimbFee', 'IsaveFlag', 'TranscodeIsave', 'Glcisave', 'Isave', 'IsaveDesc', 'IsaveRef', 'Glcisave2', 'WakalahFlag', 'TranscodeWakalah', 'Glcwakalah', 'Wakalah', 'WakalahDesc', 'WakalahRef', 'Glcwakalah2', 'LiveFlag', 'Csuflag', 'ApplRefNo2', 'LoanAccNo2', 'Netloan2', 'TransAmt2', 'NetBal2', 'AdminFeeFlag', 'TranscodeAdminFee', 'GlcadminFee', 'AdminFee', 'AdminFeeDesc', 'AdminFeeRef', 'GlcadminFee2', 'RepayTy', 'GprocFee', 'GstampDuty', 'Glolg', 'Ginsurance', 'GotherFee', 'GinsLife', 'Goa', 'Garb', 'GletterGua', 'GmemoDep', 'GsetOff', 'Gwadiah', 'Gteman', 'GsecDep', 'Gmdta', 'Gibgfee', 'Goasearch', 'GcommArb', 'Gisave', 'Gwakalah', 'GadminFee', 'GtaxCodeProcFee', 'GtaxCodeStampDuty', 'GtaxCodeInsurance', 'GtaxCodeArb', 'GtaxCodeWadiah', 'GtaxCodeTeman', 'GtaxCodeSecDep', 'GtaxCodeMdta', 'GtaxCodeIbgfee', 'GtaxCodeOasearch', 'GtaxCodeCommArb', 'GtaxCodeIsave', 'GtaxCodeWakalah', 'GtaxCodeAdminFee', 'InsCode'];

}
