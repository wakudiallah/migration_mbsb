<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $ApplId
 * @property string $CustId
 * @property string $OfficerRole
 * @property string $RemarkBy
 * @property string $Remark
 * @property string $RemarkDate
 * @property string $TypeOfRemark
 */
class ApplGlobalRemarks extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'ApplGlobalRemarks';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'CustId', 'OfficerRole', 'RemarkBy', 'Remark', 'RemarkDate', 'TypeOfRemark'];

}
