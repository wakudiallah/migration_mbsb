<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

class ApplKpis extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'ApplKpis';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    public function ApplKpis_to_ApplRefNoTabTemps()
    {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemps', 'ApplId', 'ApplRefNo');
    }

}
