<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property boolean $Act
 * @property string $AcadCode
 * @property string $AcadDesc
 */
class ApplRefNoTabTempsNew extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv2';
    protected $table = 'ApplRefNoTabTempsNew';

    /**
     * The primary key for the model.
     * 
     * @var string
     */


    public function ApplRefNoTabTemps_to_address()
    {
        return $this->belongsTo('App\Models\Table1\address', 'AcId', 'AcID');
    }

    public function ApplRefNoTabTemps_to_address_hasmany() {
        return $this->hasMany('App\Models\Table1\address', 'AcId', 'AcID');    
    }


    public function ApplRefNoTabTemps_to_applworkflow_hasmany() {
        return $this->belongsTo('App\Models\Table1\applworkflow','ApplRefNo', 'ApplRefNo'); 
    }

  

}

