<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;


class ApplRefNoTabs extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'ApplRefNoTabs';

    /**
     * The primary key for the model.
     * 
     * @var string
     */

    //protected $primaryKey = 'CountId';

    /**
     * @var array
     */

    /*protected $fillable = ['ApplId', 'DocCom', 'DocComB', 'LnPackCode', 'Akad', 'Oa', 'Bl', 'Arb', 'Wadiah', 'WadBranch', 'Teman', 'Csuflag', 'Isave', 'PayMode', 'PayBank', 'AcNo', 'PhoneChk', 'PhoneChkResult', 'SmsreqDt', 'SmsreplyDt', 'Smsresult', 'SmsresendFlag', 'ModePayment', 'TradeFlag', 'TradeDt', 'TradeResult', 'TradeSystemNo', 'TypeAkad', 'UsrResponse', 'CallBy', 'ManualAkadRmk', 'SmsRmk', 'ManualAkad', 'TypeComodity', 'Wakalah', 'Smsflag', 'SendBy', 'LiveFlag', 'SmsinComFlag', 'SmsinComDt', 'SmsinComStatus', 'SmsinComSendBy', 'SmsrejFlag', 'SmsrejDt', 'SmsrejStatus', 'SmsrejSendBy', 'AdminFee', 'CeilingRate', 'VarianceRate', 'JobConfirm', 'CcrisOldCase', 'CcrisSumChk', 'CcrisDetChk', 'SectionTy', 'DspCif', 'DspBlacklist', 'DspAafacility', 'DspIslamicFile', 'DspAccountCreation', 'DspStagPayment', 'DspDisb', 'DspDisbBulk', 'MiCobcifNo', 'DspCifSp', 'DspBlacklistSp', 'MiCobcifNoSp', 'DspDisbEc', 'JournalSeq', 'BrchProc', 'MiCobflag', 'MiCobname', 'MiCobidty', 'MiCobidno', 'MiCobmntFlag', 'MiCobmntUsrId', 'MiCobmntDt', 'InsCode', 'AkadScript', 'CcrisAppStatus', 'CertNo1', 'CertNo2', 'CertNo3', 'Ccris_Chk', 'CodeARBId', 'CodeAdminFeeId', 'CodeAkadScriptId', 'CodeBeneBankId', 'CodeBranchId', 'CodeCeilingRateId', 'CodeISaveId', 'CodeInsId', 'CodeLoanPackageId', 'CodeModeOfPaymentMOPId', 'CodeModeOfPaymentPMId', 'CodeVarianceRateId', 'SubTy', 'ApplRefNoTabTempId'];*/


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    





    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeAdminFee()
    {
        return $this->belongsTo('App\Models\Table7\CodeAdminFee', 'CodeAdminFeeId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeAkadScript()
    {
        return $this->belongsTo('App\Models\Table7\CodeAkadScript', 'CodeAkadScriptId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeBeneBank()
    {
        return $this->belongsTo('App\Models\Table7\CodeBeneBank', 'CodeBeneBankId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeBranch()
    {
        return $this->belongsTo('App\Models\Table7\CodeBranch', 'CodeBranchId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeCeilingRate()
    {
        return $this->belongsTo('App\Models\Table7\CodeCeilingRate', 'CodeCeilingRateId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeISafe()
    {
        return $this->belongsTo('App\Models\Table7\CodeISafe', 'CodeISaveId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeInss()
    {
        return $this->belongsTo('App\Models\Table7\CodeInss', 'CodeInsId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeLoanPackage()
    {
        return $this->belongsTo('App\Models\Table7\CodeLoanPackage', 'CodeLoanPackageId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeModeOfPayment()
    {
        return $this->belongsTo('App\Models\Table7\CodeModeOfPayment', 'CodeModeOfPaymentMOPId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeModeOfPayment()
    {
        return $this->belongsTo('App\Models\Table7\CodeModeOfPayment', 'CodeModeOfPaymentPMId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeVarianceRate()
    {
        return $this->belongsTo('App\Models\Table7\CodeVarianceRate', 'CodeVarianceRateId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applRefNoTabTemp()
    {
        return $this->belongsTo('App\Models\Table7\ApplRefNoTabTemp', 'ApplRefNoTabTempId', 'Id');
    }
}
