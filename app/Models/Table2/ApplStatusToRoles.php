<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplStatCode
 * @property string $RoleCode
 * @property int $Id1
 */
class ApplStatusToRoles extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'ApplStatusToRoles';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id1';

    /**
     * @var array
     */
    protected $fillable = ['ApplStatCode', 'RoleCode'];

}
