<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $ApplId
 * @property string $UserId
 * @property string $StRouteDtTm
 * @property string $PrevUserId
 * @property string $StProcessDtTm
 * @property string $EndProcessDtTm
 * @property integer $TotProcessTm
 * @property integer $NetProcessTm
 * @property string $Remark
 * @property string $AppStatus
 * @property string $NextProcess
 * @property string $DualSign
 * @property string $PenReasCode
 * @property string $CSUFlag
 * @property string $BrCode
 * @property string $PickFlag
 * @property string $UserId_Curr
 * @property string $Role
 */
class ApplWorkFlows extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'ApplWorkFlows';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'UserId', 'StRouteDtTm', 'PrevUserId', 'StProcessDtTm', 'EndProcessDtTm', 'TotProcessTm', 'NetProcessTm', 'Remark', 'AppStatus', 'NextProcess', 'DualSign', 'PenReasCode', 'CSUFlag', 'BrCode', 'PickFlag', 'UserId_Curr', 'Role'];

}
