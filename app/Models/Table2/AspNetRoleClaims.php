<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $RoleId
 * @property string $ClaimType
 * @property string $ClaimValue
 * @property AspNetRole $aspNetRole
 */
class AspNetRoleClaims extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'AspNetRoleClaims';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['RoleId', 'ClaimType', 'ClaimValue'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspNetRole()
    {
        return $this->belongsTo('App\Models\Table11\AspNetRole', 'RoleId', 'Id');
    }
}
