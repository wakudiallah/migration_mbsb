<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $Name
 * @property string $NormalizedName
 * @property string $ConcurrencyStamp
 * @property AspNetRoleClaim[] $aspNetRoleClaims
 * @property AspNetUser[] $aspNetUsers
 * @property AspNetUser[] $aspNetUsers
 */
class AspNetRoles extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'AspNetRoles';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['Name', 'NormalizedName', 'ConcurrencyStamp'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function aspNetRoleClaims()
    {
        return $this->hasMany('App\Models\Table12\AspNetRoleClaim', 'RoleId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function aspNetUsers()
    {
        return $this->belongsToMany('App\Models\Table12\AspNetUser', 'AspNetUserRoles', 'RoleId', 'UserId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    /*public function aspNetUsers()
    {
        return $this->hasMany('App\Models\Table12\AspNetUser', 'UserGroupCode', 'Id');
    }*/
}
