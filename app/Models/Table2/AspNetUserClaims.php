<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $UserId
 * @property string $ClaimType
 * @property string $ClaimValue
 * @property AspNetUser $aspNetUser
 */
class AspNetUserClaims extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'AspNetUserClaims';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['UserId', 'ClaimType', 'ClaimValue'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspNetUser()
    {
        return $this->belongsTo('App\Models\Table13\AspNetUser', 'UserId', 'Id');
    }
}
