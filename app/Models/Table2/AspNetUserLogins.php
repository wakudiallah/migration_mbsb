<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $LoginProvider
 * @property string $ProviderKey
 * @property string $ProviderDisplayName
 * @property string $UserId
 * @property AspNetUser $aspNetUser
 */
class AspNetUserLogins extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'AspNetUserLogins';

    /**
     * @var array
     */
    protected $fillable = ['ProviderDisplayName', 'UserId'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspNetUser()
    {
        return $this->belongsTo('App\Models\Table2\AspNetUser', 'UserId', 'Id');
    }
}
