<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $UserId
 * @property string $RoleId
 * @property AspNetUser $aspNetUser
 * @property AspNetRole $aspNetRole
 */
class AspNetUserRoles extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'AspNetUserRoles';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspNetUser()
    {
        return $this->belongsTo('App\Models\Table2\AspNetUser', 'UserId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspNetRole()
    {
        return $this->belongsTo('App\Models\Table2\AspNetRole', 'RoleId', 'Id');
    }
}
