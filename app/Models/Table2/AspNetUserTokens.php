<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $UserId
 * @property string $LoginProvider
 * @property string $Name
 * @property string $Value
 * @property AspNetUser $aspNetUser
 */
class AspNetUserTokens extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'AspNetUserTokens';

    /**
     * @var array
     */
    protected $fillable = ['Value'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspNetUser()
    {
        return $this->belongsTo('App\Models\Table2\AspNetUser', 'UserId', 'Id');
    }
}
