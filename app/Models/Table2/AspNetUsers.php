<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $UserName
 * @property string $NormalizedUserName
 * @property string $Email
 * @property string $NormalizedEmail
 * @property boolean $EmailConfirmed
 * @property string $PasswordHash
 * @property string $SecurityStamp
 * @property string $ConcurrencyStamp
 * @property string $PhoneNumber
 * @property boolean $PhoneNumberConfirmed
 * @property boolean $TwoFactorEnabled
 * @property string $LockoutEnd
 * @property boolean $LockoutEnabled
 * @property int $AccessFailedCount
 * @property string $Name
 * @property string $UserGroupCode
 * @property boolean $UserDeviation
 * @property boolean $UserApprovalRight
 * @property float $UserMinAppAmt
 * @property float $UserAppMaxAmt
 * @property string $UserAppPassowrd
 * @property string $PasswordCreateDate
 * @property int $RouteFlag
 * @property string $RouteDate
 * @property string $RenewPassword
 * @property boolean $Activate
 * @property string $BrCode
 * @property int $CSUFlag
 * @property string $DCGrpCode
 * @property string $SIBSCode
 * @property boolean $DualSign
 * @property string $SuprOvrId
 * @property string $UserIdCreatedDate
 * @property string $UserIdCreatedBy
 * @property string $UserStaffId
 * @property string $Salt
 * @property string $StafDept
 * @property string $StaffId
 * @property string $userRole
 * @property string $CodeProcessId
 * @property AspNetRole $aspNetRole
 * @property CodeBranch $codeBranch
 * @property CodeDCGroup $codeDCGroup
 * @property CodeProcess $codeProcess
 * @property AspNetUserClaim[] $aspNetUserClaims
 * @property AspNetUserLogin[] $aspNetUserLogins
 * @property AspNetRole[] $aspNetRoles
 * @property AspNetUserToken[] $aspNetUserTokens
 * @property Post[] $posts
 * @property RefreshToken[] $refreshTokens
 */
class AspNetUsers extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'AspNetUsers';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    //protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    //protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    //public $incrementing = false;

    /**
     * @var array
     */
    /*protected $fillable = ['UserName', 'NormalizedUserName', 'Email', 'NormalizedEmail', 'EmailConfirmed', 'PasswordHash', 'SecurityStamp', 'ConcurrencyStamp', 'PhoneNumber', 'PhoneNumberConfirmed', 'TwoFactorEnabled', 'LockoutEnd', 'LockoutEnabled', 'AccessFailedCount', 'Name', 'UserGroupCode', 'UserDeviation', 'UserApprovalRight', 'UserMinAppAmt', 'UserAppMaxAmt', 'UserAppPassowrd', 'PasswordCreateDate', 'RouteFlag', 'RouteDate', 'RenewPassword', 'Activate', 'BrCode', 'CSUFlag', 'DCGrpCode', 'SIBSCode', 'DualSign', 'SuprOvrId', 'UserIdCreatedDate', 'UserIdCreatedBy', 'UserStaffId', 'Salt', 'StafDept', 'StaffId', 'userRole', 'CodeProcessId'];*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspNetRole()
    {
        return $this->belongsTo('App\Models\Table2\AspNetRole', 'UserGroupCode', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeBranch()
    {
        return $this->belongsTo('App\Models\Table2\CodeBranch', 'BrCode', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeDCGroup()
    {
        return $this->belongsTo('App\Models\Table2\CodeDCGroup', 'DCGrpCode', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function codeProcess()
    {
        return $this->belongsTo('App\Models\Table2\CodeProcess', 'CodeProcessId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function aspNetUserClaims()
    {
        return $this->hasMany('App\Models\Table2\AspNetUserClaim', 'UserId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function aspNetUserLogins()
    {
        return $this->hasMany('App\Models\Table2\AspNetUserLogin', 'UserId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function aspNetRoles()
    {
        return $this->belongsToMany('App\Models\Table2\AspNetRole', 'AspNetUserRoles', 'UserId', 'RoleId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function aspNetUserTokens()
    {
        return $this->hasMany('App\Models\Table2\AspNetUserToken', 'UserId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Table2\Post', 'UserId', 'Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function refreshTokens()
    {
        return $this->hasMany('App\Models\Table2\RefreshToken', 'UserId', 'Id');
    }
}
