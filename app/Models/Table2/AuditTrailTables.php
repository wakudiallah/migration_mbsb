<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $Id
 * @property string $AuditActionType
 * @property string $DataModel
 * @property string $DateChanged
 * @property string $OldValue
 * @property string $NewValue
 * @property string $Changes
 * @property string $UserName
 */
class AuditTrailTables extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'AuditTrailTables';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['AuditActionType', 'DataModel', 'DateChanged', 'OldValue', 'NewValue', 'Changes', 'UserName'];

}
