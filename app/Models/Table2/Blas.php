<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $ApplId
 * @property string $ProdCode
 * @property string $BiroIndc
 * @property string $OriBrch
 * @property string $ApplDate
 * @property string $ApplBackDate
 * @property float $ApplyAmt
 * @property string $PurposeCode
 * @property string $SectoralCode
 * @property string $FacilityType
 * @property string $StateCode
 * @property boolean $Smiindicator
 * @property string $PropSector
 * @property string $MarketBy
 * @property string $SecuType
 * @property string $AdvisoryId
 * @property string $AvailDate
 * @property float $QuotedRate
 * @property float $LpiquoteRate
 * @property string $ApplyPeriod
 * @property float $LoanAmt
 * @property float $InstRentalAmt
 * @property float $InstAfterDeduct
 * @property boolean $LatePayIndicator
 * @property string $EarlyInst
 * @property string $UnearnProfit
 * @property string $CoopDealer
 * @property float $AmtPayable
 * @property string $CoopCode
 * @property string $CoopBrchCode
 * @property string $CoopDealerCode
 * @property string $CoopDealerBrchCode
 * @property string $MoffIdtype
 * @property string $MoffIdno
 * @property string $Agind
 * @property string $MrgDt
 * @property string $Lmsflag
 * @property string $ApplSts
 * @property string $ApplBrchCd
 * @property string $AprvAuth
 * @property float $IntSprd
 * @property float $PltyRt
 * @property float $MrgnFinc
 * @property string $LastUpdUsrId
 * @property string $Csuflag
 * @property string $ClerkId
 * @property string $OffId
 */
class Blas extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'Blas';


    
}
