<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property boolean $Act
 * @property string $AppType
 * @property string $FacilityType
 * @property string $PurposeOfLoan
 * @property string $SpecialFund
 * @property string $DPIFinance
 * @property string $Syndicated
 * @property string $Location
 * @property string $FInConcept
 * @property string $PrioritySector
 * @property string $MemberBankRakyat
 * @property string $CorporateStatus
 * @property string $IndustrialSector
 * @property string $EntityType
 * @property string $AppicationStatus
 * @property string $FICode
 * @property string $ProdId
 * @property string $NewCust
 * @property string $Registered
 * @property string $SMEFinance
 * @property string $AssetPurchase
 */
class CCRISTaxonomys extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';

    protected $table = 'CCRISTaxonomys';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['CreatedBy', 'CreatedDate', 'Act', 'AppType', 'FacilityType', 'PurposeOfLoan', 'SpecialFund', 'DPIFinance', 'Syndicated', 'Location', 'FInConcept', 'PrioritySector', 'MemberBankRakyat', 'CorporateStatus', 'IndustrialSector', 'EntityType', 'AppicationStatus', 'FICode', 'ProdId', 'NewCust', 'Registered', 'SMEFinance', 'AssetPurchase'];

}
