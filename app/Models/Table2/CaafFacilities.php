<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property string $ApplId
 * @property float $FacLoanLmt
 * @property float $FacLoanLmtWlh
 * @property float $FacInstBpa
 * @property float $FacInstAf
 * @property float $FacInstAfwlh
 * @property float $FacProcFee
 * @property float $FacLawyerFee
 * @property float $FacStampDuty
 * @property float $FacLolg
 * @property float $FacInsurance
 * @property float $FacMshipFee
 * @property float $FacCccharges
 * @property float $FacOtherFee
 * @property float $FacNetLoanAmt
 * @property string $FacRate
 * @property string $Facdeferment
 * @property float $FacAdInst
 * @property float $Factpc
 * @property float $Facshm
 * @property float $FacInsLife
 * @property float $FacOa
 * @property float $FacArb
 * @property float $FacLetterGua
 * @property float $FacMemoDep
 * @property float $FacSetOff
 * @property float $FacWadiah
 * @property float $FacTeman
 * @property string $Csuflag
 * @property float $FacReimbFee
 * @property float $FacNetLoanAmt2
 * @property float $FacInstAf2
 * @property float $FacTopUp1
 * @property float $FacTopUp2
 * @property float $FacFinalInst
 * @property float $FacGprocFee
 * @property float $FacGstampDuty
 * @property float $FacGlolg
 * @property float $FacGinsurance
 * @property float $FacGotherFee
 * @property float $FacGinsLife
 * @property float $FacGoa
 * @property float $FacGarb
 * @property float $FacGletterGua
 * @property float $FacGmemoDep
 * @property float $FacGsetOff
 * @property float $FacGwadiah
 * @property float $FacGteman
 */
class CaafFacilities extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'CaafFacilities';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['CreatedBy', 'CreatedDate', 'ApplId', 'FacLoanLmt', 'FacLoanLmtWlh', 'FacInstBpa', 'FacInstAf', 'FacInstAfwlh', 'FacProcFee', 'FacLawyerFee', 'FacStampDuty', 'FacLolg', 'FacInsurance', 'FacMshipFee', 'FacCccharges', 'FacOtherFee', 'FacNetLoanAmt', 'FacRate', 'Facdeferment', 'FacAdInst', 'Factpc', 'Facshm', 'FacInsLife', 'FacOa', 'FacArb', 'FacLetterGua', 'FacMemoDep', 'FacSetOff', 'FacWadiah', 'FacTeman', 'Csuflag', 'FacReimbFee', 'FacNetLoanAmt2', 'FacInstAf2', 'FacTopUp1', 'FacTopUp2', 'FacFinalInst', 'FacGprocFee', 'FacGstampDuty', 'FacGlolg', 'FacGinsurance', 'FacGotherFee', 'FacGinsLife', 'FacGoa', 'FacGarb', 'FacGletterGua', 'FacGmemoDep', 'FacGsetOff', 'FacGwadiah', 'FacGteman'];

}
