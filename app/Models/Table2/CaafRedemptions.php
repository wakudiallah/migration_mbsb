<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $ApplId
 * @property string $SetPtyCode
 * @property float $DeducVal
 * @property float $OsloanAmt
 * @property string $DtSubmit
 * @property string $DtObtReceipt
 * @property string $VerifyBy
 * @property string $ExpDt
 * @property string $Csuflag
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property int $RedempId
 */
class CaafRedemptions extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'CaafRedemptions';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'RedempId';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'SetPtyCode', 'DeducVal', 'OsloanAmt', 'DtSubmit', 'DtObtReceipt', 'VerifyBy', 'ExpDt', 'Csuflag', 'CreatedBy', 'CreatedDate'];

}
