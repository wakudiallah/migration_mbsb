<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property string $ApplId
 * @property string $SameMember
 * @property float $MaxDeduc
 * @property float $MaxDeducWlh
 * @property float $CurrTotDeduc
 * @property float $InsPrem
 * @property float $TotRedemptD
 * @property float $TotRedemptAmt
 * @property float $TotDeduc
 * @property float $AddDeduc
 * @property float $AddDeducWlh
 * @property float $CalcLoanLmt
 * @property float $CalcLoanLmtWlh
 * @property float $PolicyLoanLmt
 * @property float $PolicyLoanLmtWlh
 * @property float $RecommLimit
 * @property float $RecommLimitWlh
 * @property float $PropLoanLmt
 * @property float $PropLoanLmtWlh
 * @property float $Check2
 * @property float $TotCurrUnsExpB
 * @property float $TotCurrUnsExpG
 * @property float $TotUnsExp
 * @property string $ExistLoan
 * @property string $Nricverify
 * @property string $AcardSubmit
 * @property string $BcertConfirm
 * @property string $VerifyBy
 * @property string $SalaryFont
 * @property string $PaySlipCertify
 * @property string $EmpStatConfirm
 * @property string $PersContact
 * @property string $DepartName
 * @property string $PhoneNo
 * @property string $CallDt
 * @property string $BpaexDeduc
 * @property string $BpacurrSalary
 * @property string $BpaauthSign
 * @property string $Mbscheck
 * @property string $Ctoscheck
 * @property string $Ccrischeck
 * @property string $TermCond
 * @property string $DevCode
 * @property string $PropTenure
 * @property float $InTransit
 * @property string $AgbranchCode
 * @property string $AgpayCentre
 * @property string $AgdepartmentCode
 * @property string $AgpersonnelNo
 * @property string $Agind
 * @property string $AgbranchCodeRev
 * @property string $AgpayCentreRev
 * @property string $AgdepartmentCodeRev
 * @property string $AgpersonnelNoRev
 * @property string $AngkasaCode
 * @property string $Agstate
 * @property string $AngkasaCode2
 * @property float $Dsrrate
 * @property string $CreditGrade
 * @property float $Epf
 * @property float $Socso
 * @property float $Zakat
 * @property float $Tax
 * @property string $CreditGradeRmk
 * @property float $TotActComm
 * @property string $AngkasaId
 * @property string $NoGaji
 * @property string $NoGajiRev
 * @property string $SalaryDt
 * @property boolean $GuarReq
 */
class CaafTabs extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'CaafTabs';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['CreatedBy', 'CreatedDate', 'ApplId', 'SameMember', 'MaxDeduc', 'MaxDeducWlh', 'CurrTotDeduc', 'InsPrem', 'TotRedemptD', 'TotRedemptAmt', 'TotDeduc', 'AddDeduc', 'AddDeducWlh', 'CalcLoanLmt', 'CalcLoanLmtWlh', 'PolicyLoanLmt', 'PolicyLoanLmtWlh', 'RecommLimit', 'RecommLimitWlh', 'PropLoanLmt', 'PropLoanLmtWlh', 'Check2', 'TotCurrUnsExpB', 'TotCurrUnsExpG', 'TotUnsExp', 'ExistLoan', 'Nricverify', 'AcardSubmit', 'BcertConfirm', 'VerifyBy', 'SalaryFont', 'PaySlipCertify', 'EmpStatConfirm', 'PersContact', 'DepartName', 'PhoneNo', 'CallDt', 'BpaexDeduc', 'BpacurrSalary', 'BpaauthSign', 'Mbscheck', 'Ctoscheck', 'Ccrischeck', 'TermCond', 'DevCode', 'PropTenure', 'InTransit', 'AgbranchCode', 'AgpayCentre', 'AgdepartmentCode', 'AgpersonnelNo', 'Agind', 'AgbranchCodeRev', 'AgpayCentreRev', 'AgdepartmentCodeRev', 'AgpersonnelNoRev', 'AngkasaCode', 'Agstate', 'AngkasaCode2', 'Dsrrate', 'CreditGrade', 'Epf', 'Socso', 'Zakat', 'Tax', 'CreditGradeRmk', 'TotActComm', 'AngkasaId', 'NoGaji', 'NoGajiRev', 'SalaryDt', 'GuarReq'];

}
