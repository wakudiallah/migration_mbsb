<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property string $ApplId
 * @property string $CustIdno
 * @property string $Facility
 * @property float $BorrowerVal
 * @property float $GuarantorVal
 */
class CaafUnsExposures extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';

    protected $table = 'CaafUnsExposures';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['CreatedBy', 'CreatedDate', 'ApplId', 'CustIdno', 'Facility', 'BorrowerVal', 'GuarantorVal'];

}
