<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property boolean $Act
 * @property string $UserId
 * @property string $EndDate
 * @property string $StartDate
 * @property string $Name
 */
class CalAbsentTabs extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv2';
    protected $table = 'CalAbsentTabs';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['CreatedBy', 'CreatedDate', 'Act', 'UserId', 'EndDate', 'StartDate', 'Name'];

}
