<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $ApplId
 * @property string $CustName
 * @property string $CustAbbreName
 * @property string $CustTitle
 * @property string $CustNewIdty
 * @property string $CustNewIdno
 * @property string $CustPrevIdty
 * @property string $CustPrevIdno
 * @property boolean $ResStatus
 * @property string $CustBumi
 * @property string $CustHp
 * @property string $CustFax
 * @property string $CustPagerCenter
 * @property string $CustPagerNo
 * @property string $CustEmail
 * @property string $CustNation
 * @property string $CustRace
 * @property string $CustGender
 * @property string $CustDob
 * @property string $CustMarital
 * @property string $CustReligion
 * @property string $CustLanguage
 * @property string $CustDepend
 * @property string $CustProffesion
 * @property string $CustEmployer
 * @property string $CustNob
 * @property string $CustDesignation
 * @property string $CustEmployDt
 * @property string $CustIncomeTy
 * @property string $CustIncomeRange
 * @property float $CustActualIncome
 * @property string $CustIncomeDt
 * @property string $CustJobStat
 * @property string $AgbranchCode
 * @property string $AgpayCentre
 * @property string $AgdepartmentCode
 * @property string $AgpersonnelNo
 * @property string $Agind
 * @property string $CustEduLvl
 * @property string $CustIncmTyp
 * @property string $CustWrkSec
 * @property string $CustDeptNm
 * @property float $CustFixedIncome
 * @property float $CustOtherIncome
 * @property boolean $CustWorkFinc
 * @property boolean $WaveFlag
 * @property string $Lmsflag
 * @property string $UploadDt
 * @property boolean $CustPdpa
 * @property string $CustGstregNo
 * @property string $CustEmpSec
 * @property string $CustEmpTy
 * @property string $CustOffP
 * @property int $ApplRefNoTabTempId
 * @property string $CustHouseP
 * @property ApplRefNoTabTemp $applRefNoTabTemp
 */
class Cifs extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'Cifs';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'CustName', 'CustAbbreName', 'CustTitle', 'CustNewIdty', 'CustNewIdno', 'CustPrevIdty', 'CustPrevIdno', 'ResStatus', 'CustBumi', 'CustHp', 'CustFax', 'CustPagerCenter', 'CustPagerNo', 'CustEmail', 'CustNation', 'CustRace', 'CustGender', 'CustDob', 'CustMarital', 'CustReligion', 'CustLanguage', 'CustDepend', 'CustProffesion', 'CustEmployer', 'CustNob', 'CustDesignation', 'CustEmployDt', 'CustIncomeTy', 'CustIncomeRange', 'CustActualIncome', 'CustIncomeDt', 'CustJobStat', 'AgbranchCode', 'AgpayCentre', 'AgdepartmentCode', 'AgpersonnelNo', 'Agind', 'CustEduLvl', 'CustIncmTyp', 'CustWrkSec', 'CustDeptNm', 'CustFixedIncome', 'CustOtherIncome', 'CustWorkFinc', 'WaveFlag', 'Lmsflag', 'UploadDt', 'CustPdpa', 'CustGstregNo', 'CustEmpSec', 'CustEmpTy', 'CustOffP', 'ApplRefNoTabTempId', 'CustHouseP'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applRefNoTabTemp()
    {
        return $this->belongsTo('App\Models\Table2\ApplRefNoTabTemp', 'ApplRefNoTabTempId', 'Id');
    }
}
