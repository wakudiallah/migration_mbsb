<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property boolean $Act
 * @property string $AcadCode
 * @property string $AcadDesc
 */
class CodeBranches extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv2';
    protected $table = 'CodeBranches';

    /**
     * The primary key for the model.
     * 
     * @var string
     */


     
  

}

