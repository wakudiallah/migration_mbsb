<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property boolean $Act
 * @property string $AcadCode
 * @property string $AcadDesc
 */
class CodeCheckList extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    protected $connection = 'sqlsrv2';
    protected $table = 'CodeCheckLists';

    /**
     * The primary key for the model.
     * 
     * @var string
     */


     protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
   
  

}

