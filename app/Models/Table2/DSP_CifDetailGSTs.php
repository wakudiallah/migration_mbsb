<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;


class DSP_CifDetailGSTs extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'DSP_CifDetailGSTs';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'No';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'Hdrnum', 'ClientDt', 'ClientTm', 'UserId', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCobcifNo', 'CustIdno', 'Id1', 'CustomerIdno2', 'IdtypeCode2', 'IdissueCountry2', 'Shortname', 'CustomerIdno1', 'IdtypeCode1', 'IdissueCountry1', 'CustomerIdnoNob2', 'IdtypeCodeNob2', 'IdissueCountryNob2', 'Branchnumber', 'Customernumber', 'Salutation', 'Customername1', 'Customername2', 'Class1', 'Dob', 'Individual', 'Countryofcitizenship', 'RaceCountry', 'ResidenceBusOper', 'Gender', 'Marital', 'Residentcode', 'Businesstype', 'Id2', 'Addresssequence1', 'Addresstype1', 'Addressformat1', 'Foreignaddress1', 'Addressline11', 'Addressline12', 'Addressline13', 'Citystatezip1', 'Postalcode1', 'State1', 'Country1', 'Addressseq2', 'Addresstype2', 'Addressformat2', 'Foreignaddress2', 'Addressline21', 'Addressline22', 'Addressline23', 'Citystatezip2', 'Postalcode2', 'State2', 'Country2', 'ContactSeq1', 'ContactType1', 'ContactDetail1', 'ContactName1', 'ContactSeq2', 'ContactType2', 'ContactDetail2', 'ContactName2', 'Costcenter', 'CustomerSegmentationCode', 'NumberofEmployees', 'HighRiskFlag', 'HighRiskCode', 'StaffFlag', 'StaffIdnumber', 'Religioncode', 'CustomerAge', 'Insidercode', 'DurationStay', 'HouseOwnershipCode', 'TypeofDwelling', 'Numberofdependents', 'BusinessPremisesOwnershipCode', 'TypeofBusinessPremises', 'BusinessOperatingHours', 'Startoperationhours', 'Endoperationhours', 'BnmsectorCode', 'BnmstateCode', 'BnmbumiCode', 'Vipcustomercode', 'BlacklistedFlag', 'Sourcecode', 'Reasoncode', 'Listeddescription', 'GstregNo', 'GstregFlag', 'DataSend', 'DataReturn', 'DateSend', 'DateReceive'];

}
