<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $No
 * @property string $ApplId
 * @property string $ApplRefNo
 * @property string $ScenarioNo
 * @property string $ActionCode
 * @property string $TransactionCode
 * @property string $Hdrnum
 * @property string $ClientDt
 * @property string $ClientTm
 * @property string $UserId
 * @property string $UserGroup
 * @property string $Branch
 * @property string $Status
 * @property string $ErrorCode
 * @property string $ErrorReason
 * @property string $MiCobcifNo
 * @property string $EmpHisSeq
 * @property string $EmpStat
 * @property string $EmpStDt
 * @property string $OccuCode
 * @property string $JobDsgnCode
 * @property string $TyEmp
 * @property string $EmpIndCode
 * @property string $EmployerCode
 * @property string $EmployerRate
 * @property string $EmployerName
 * @property string $IncomeBracketCode
 * @property string $Income
 * @property string $OthIncome
 * @property string $Expenditure
 * @property string $EmployeeId
 * @property string $Remark1
 * @property string $Remark2
 * @property string $Remark3
 * @property string $EmploymentEndDt1
 * @property string $EmploymentEndDt2
 * @property string $EmployerAdd1
 * @property string $EmployerAdd2
 * @property string $EmployerAdd3
 * @property string $EmployerAdd4
 * @property string $EmployerAdd5
 * @property string $EmployerCity
 * @property string $ForeignAdd
 * @property string $City
 * @property string $State
 * @property string $Postalcode
 * @property string $Country
 * @property string $Dept
 * @property string $BusinessType
 * @property string $EmpSec
 * @property string $CustPayClass
 * @property string $OffNo
 * @property string $FaxNo
 * @property string $HpNo
 * @property string $HrphoneNo
 * @property string $Remark4
 * @property string $MonthlyIncome
 * @property string $MonthlyOtherIncome
 * @property string $MonthlyExpenditure
 * @property string $DateSend
 * @property string $DateReceive
 */
class DSP_EmpDetails extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'DSP_EmpDetails';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'No';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'Hdrnum', 'ClientDt', 'ClientTm', 'UserId', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCobcifNo', 'EmpHisSeq', 'EmpStat', 'EmpStDt', 'OccuCode', 'JobDsgnCode', 'TyEmp', 'EmpIndCode', 'EmployerCode', 'EmployerRate', 'EmployerName', 'IncomeBracketCode', 'Income', 'OthIncome', 'Expenditure', 'EmployeeId', 'Remark1', 'Remark2', 'Remark3', 'EmploymentEndDt1', 'EmploymentEndDt2', 'EmployerAdd1', 'EmployerAdd2', 'EmployerAdd3', 'EmployerAdd4', 'EmployerAdd5', 'EmployerCity', 'ForeignAdd', 'City', 'State', 'Postalcode', 'Country', 'Dept', 'BusinessType', 'EmpSec', 'CustPayClass', 'OffNo', 'FaxNo', 'HpNo', 'HrphoneNo', 'Remark4', 'MonthlyIncome', 'MonthlyOtherIncome', 'MonthlyExpenditure', 'DateSend', 'DateReceive'];

}
