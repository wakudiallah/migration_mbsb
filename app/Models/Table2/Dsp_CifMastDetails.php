<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $No
 * @property string $ApplId
 * @property string $ApplRefNo
 * @property string $ScenarioNo
 * @property string $ActionCode
 * @property string $TransactionCode
 * @property string $Hdrnum
 * @property string $ClientDt
 * @property string $ClientTm
 * @property string $UserId
 * @property string $UserGroup
 * @property string $Branch
 * @property string $Status
 * @property string $ErrorCode
 * @property string $ErrorReason
 * @property string $MiCobcifNo
 * @property string $CustIdno
 * @property string $Individual
 * @property string $ShortName
 * @property string $Vip
 * @property string $Insider
 * @property string $InquiryCode
 * @property string $MotherMaidenName
 * @property string $DateOfBirth
 * @property string $Country
 * @property string $Nationality
 * @property string $Bumi
 * @property string $Residence
 * @property string $Gender
 * @property string $Race
 * @property string $ReligionCode
 * @property string $LanguageIdentifier
 * @property string $MaritalStatus
 * @property string $MaritalStatusDate
 * @property string $Occupation
 * @property string $CustomerStatus
 * @property string $CustomerStatusDate
 * @property string $DeceasedStatus
 * @property string $DeceaseDate
 * @property string $CostCenter
 * @property string $HoldMailCode
 * @property string $PromotionMailCode
 * @property string $CombinedCycle
 * @property string $AddressSeq
 * @property string $StartRelationship
 * @property string $ReviewDate
 * @property string $CreationDate
 * @property string $Retention
 * @property string $SpecialInfoCode1
 * @property string $SpecialInfoCode2
 * @property string $SpecialInfoCode3
 * @property string $SpecialInfoCode4
 * @property string $SpecialInfoCode5
 * @property string $SpecialInfoCode6
 * @property string $SpecialInfoCode7
 * @property string $SpecialInfoCode8
 * @property string $SpecialInfoCode1Flag
 * @property string $SpecialInfoCode2Flag
 * @property string $SpecialInfoCode3Flag
 * @property string $SpecialInfoCode4Flag
 * @property string $SpecialInfoCode5Flag
 * @property string $SpecialInfoCode6Flag
 * @property string $SpecialInfoCode7Flag
 * @property string $SpecialInfoCode8Flag
 * @property string $CustomerInfoCode1
 * @property string $CustomerInfoCode2
 * @property string $CustomerInfoCode3
 * @property string $CustomerInfoCode4
 * @property string $CustomerInfoCode5
 * @property string $CustomerInfoCode6
 * @property string $CustomerInfoCode7
 * @property string $CustomerInfoCode8
 * @property string $CustomerInfCode1Flag
 * @property string $CustomerInfCode2Flag
 * @property string $CustomerInfCode3Flag
 * @property string $CustomerInfCode4Flag
 * @property string $CustomerInfCode5Flag
 * @property string $CustomerInfCode6Flag
 * @property string $CustomerInfCode7Flag
 * @property string $CustomerInfCode8Flag
 * @property string $Consitution
 * @property string $Placeofbirth
 * @property string $CustomerSegmentationCode
 * @property string $NumberofEmployees
 * @property string $HighRiskFlag
 * @property string $HighRiskCode
 * @property string $StaffFlag
 * @property string $StaffIdnumber
 * @property string $CustomerAge
 * @property string $Durationofstayatpresentaddress
 * @property string $HouseOwnershipCode
 * @property string $TypeofDwelling
 * @property string $Numberofdependents
 * @property string $BusinessPremisesOwnershipcode
 * @property string $TypeofBusinessPremises
 * @property string $BusinessOperatingHours
 * @property string $Idnumber
 * @property string $Idtype
 * @property string $Salutation
 * @property string $AddressLine1
 * @property string $AddressLine2
 * @property string $AddressLine3
 * @property string $AddressLine4
 * @property string $City
 * @property string $PostalCode
 * @property string $HousePhone
 * @property string $MobilePhone
 * @property string $EmailAddress
 * @property string $Dateofbirth2
 * @property string $Startoperationhours
 * @property string $Endoperationhours
 * @property string $BranchNumber
 * @property string $OfficePhone
 * @property string $ContactName
 * @property string $Fax
 * @property string $ContactName1
 * @property string $ContactName2
 * @property string $CreationDate2
 * @property string $Year
 * @property string $AnnualTurnover
 * @property string $Sector
 * @property string $NoofFullTimeEmp
 * @property string $TypeofClient
 * @property string $Customername1
 * @property string $DataSend
 * @property string $DataReturn
 * @property string $DateSend
 * @property string $DateReceive
 */
class Dsp_CifMastDetails extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'Dsp_CifMastDetails';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'No';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'Hdrnum', 'ClientDt', 'ClientTm', 'UserId', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCobcifNo', 'CustIdno', 'Individual', 'ShortName', 'Vip', 'Insider', 'InquiryCode', 'MotherMaidenName', 'DateOfBirth', 'Country', 'Nationality', 'Bumi', 'Residence', 'Gender', 'Race', 'ReligionCode', 'LanguageIdentifier', 'MaritalStatus', 'MaritalStatusDate', 'Occupation', 'CustomerStatus', 'CustomerStatusDate', 'DeceasedStatus', 'DeceaseDate', 'CostCenter', 'HoldMailCode', 'PromotionMailCode', 'CombinedCycle', 'AddressSeq', 'StartRelationship', 'ReviewDate', 'CreationDate', 'Retention', 'SpecialInfoCode1', 'SpecialInfoCode2', 'SpecialInfoCode3', 'SpecialInfoCode4', 'SpecialInfoCode5', 'SpecialInfoCode6', 'SpecialInfoCode7', 'SpecialInfoCode8', 'SpecialInfoCode1Flag', 'SpecialInfoCode2Flag', 'SpecialInfoCode3Flag', 'SpecialInfoCode4Flag', 'SpecialInfoCode5Flag', 'SpecialInfoCode6Flag', 'SpecialInfoCode7Flag', 'SpecialInfoCode8Flag', 'CustomerInfoCode1', 'CustomerInfoCode2', 'CustomerInfoCode3', 'CustomerInfoCode4', 'CustomerInfoCode5', 'CustomerInfoCode6', 'CustomerInfoCode7', 'CustomerInfoCode8', 'CustomerInfCode1Flag', 'CustomerInfCode2Flag', 'CustomerInfCode3Flag', 'CustomerInfCode4Flag', 'CustomerInfCode5Flag', 'CustomerInfCode6Flag', 'CustomerInfCode7Flag', 'CustomerInfCode8Flag', 'Consitution', 'Placeofbirth', 'CustomerSegmentationCode', 'NumberofEmployees', 'HighRiskFlag', 'HighRiskCode', 'StaffFlag', 'StaffIdnumber', 'CustomerAge', 'Durationofstayatpresentaddress', 'HouseOwnershipCode', 'TypeofDwelling', 'Numberofdependents', 'BusinessPremisesOwnershipcode', 'TypeofBusinessPremises', 'BusinessOperatingHours', 'Idnumber', 'Idtype', 'Salutation', 'AddressLine1', 'AddressLine2', 'AddressLine3', 'AddressLine4', 'City', 'PostalCode', 'HousePhone', 'MobilePhone', 'EmailAddress', 'Dateofbirth2', 'Startoperationhours', 'Endoperationhours', 'BranchNumber', 'OfficePhone', 'ContactName', 'Fax', 'ContactName1', 'ContactName2', 'CreationDate2', 'Year', 'AnnualTurnover', 'Sector', 'NoofFullTimeEmp', 'TypeofClient', 'Customername1', 'DataSend', 'DataReturn', 'DateSend', 'DateReceive'];

}
