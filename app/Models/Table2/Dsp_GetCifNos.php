<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $No
 * @property string $ApplId
 * @property string $ApplRefNo
 * @property string $ScenarioNo
 * @property string $ActionCode
 * @property string $TransactionCode
 * @property string $Hdrnum
 * @property string $ClientDt
 * @property string $ClientTm
 * @property string $UserId
 * @property string $UserGroup
 * @property string $Branch
 * @property string $Status
 * @property string $ErrorCode
 * @property string $ErrorReason
 * @property string $MiCobcifNo
 * @property string $CustIdno
 * @property string $CustIdty
 * @property string $Idno
 * @property string $IdtypeCode
 * @property string $AliasFlag
 * @property string $CustomerName1
 * @property string $FormattedIdNo
 * @property string $Country
 * @property string $BranchNo
 * @property string $CifNo
 * @property string $CustomerName2
 * @property string $ShortName
 * @property string $Individual
 * @property string $ElectronicAddDesc
 * @property string $City
 * @property string $ZipCode
 * @property string $CollateralName
 * @property string $Address1
 * @property string $Address2
 * @property string $StaffIdno
 * @property string $Idmnuusr
 * @property string $DataSend
 * @property string $DataReturn
 * @property string $DateSend
 * @property string $DateReceive
 */
class Dsp_GetCifNos extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'Dsp_GetCifNos';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'No';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'ApplRefNo', 'ScenarioNo', 'ActionCode', 'TransactionCode', 'Hdrnum', 'ClientDt', 'ClientTm', 'UserId', 'UserGroup', 'Branch', 'Status', 'ErrorCode', 'ErrorReason', 'MiCobcifNo', 'CustIdno', 'CustIdty', 'Idno', 'IdtypeCode', 'AliasFlag', 'CustomerName1', 'FormattedIdNo', 'Country', 'BranchNo', 'CifNo', 'CustomerName2', 'ShortName', 'Individual', 'ElectronicAddDesc', 'City', 'ZipCode', 'CollateralName', 'Address1', 'Address2', 'StaffIdno', 'Idmnuusr', 'DataSend', 'DataReturn', 'DateSend', 'DateReceive'];

}
