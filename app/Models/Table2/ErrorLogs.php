<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Date
 * @property string $ExPath
 * @property string $ExMessage
 * @property string $ExStackTrace
 * @property string $Id
 * @property string $UsrId
 */
class ErrorLogs extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'ErrorLogs';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['Date', 'ExPath', 'ExMessage', 'ExStackTrace', 'UsrId'];

}
