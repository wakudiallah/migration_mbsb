<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $ApplId
 * @property string $CustAdd
 * @property string $CustRelation
 * @property string $CustIdTy
 * @property string $CustIdNo
 * @property string $SendStatement
 * @property string $CustRemark
 */
class Guarantors extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'Guarantors';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * @var array
     */
    protected $fillable = ['ApplId', 'CustAdd', 'CustRelation', 'CustIdTy', 'CustIdNo', 'SendStatement', 'CustRemark'];

}
