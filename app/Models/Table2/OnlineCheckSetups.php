<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property boolean $Act
 * @property string $XML_LOADPGM
 * @property string $User_ID
 * @property string $User_Group
 * @property string $Version_No
 * @property string $Prod_ID
 * @property string $Rpt_Type_1
 * @property string $Rpt_Type_2
 * @property string $Rpt_Type_3
 * @property float $Limit
 * @property string $Date
 * @property string $UsrID
 * @property string $CCRIS_URL
 * @property string $CCRIS_Port
 * @property string $CTOS_URL
 * @property string $BIS_URL
 * @property string $NameSpace
 * @property string $Usr_Pwd
 * @property string $SMSPeriod
 * @property string $ASSIDQ_URL
 * @property string $DSP_URL
 * @property string $DSP_Port
 */
class OnlineCheckSetups extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'OnlineCheckSetups';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['CreatedBy', 'CreatedDate', 'Act', 'XML_LOADPGM', 'User_ID', 'User_Group', 'Version_No', 'Prod_ID', 'Rpt_Type_1', 'Rpt_Type_2', 'Rpt_Type_3', 'Limit', 'Date', 'UsrID', 'CCRIS_URL', 'CCRIS_Port', 'CTOS_URL', 'BIS_URL', 'NameSpace', 'Usr_Pwd', 'SMSPeriod', 'ASSIDQ_URL', 'DSP_URL', 'DSP_Port'];

}
