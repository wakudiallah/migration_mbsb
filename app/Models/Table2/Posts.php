<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $Name
 * @property string $UserId
 * @property AspNetUser $aspNetUser
 */
class Posts extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'Posts';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['Name', 'UserId'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspNetUser()
    {
        return $this->belongsTo('App\Models\Table2\AspNetUser', 'UserId', 'Id');
    }
}
