<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Token
 * @property string $JwtId
 * @property string $CreationDate
 * @property string $Expirydate
 * @property boolean $Used
 * @property boolean $Invalidated
 * @property string $UserId
 * @property AspNetUser $aspNetUser
 */
class RefreshTokens extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'RefreshTokens';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Token';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['JwtId', 'CreationDate', 'Expirydate', 'Used', 'Invalidated', 'UserId'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspNetUser()
    {
        return $this->belongsTo('App\Models\Table2\AspNetUser', 'UserId', 'Id');
    }
}
