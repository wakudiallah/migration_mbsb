<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $Id
 * @property string $ApplId
 * @property string $StatusDt
 * @property string $AppStatus
 */
class SumCurrentApplStats extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'SumCurrentApplStats';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    //protected $primaryKey = 'Id';

    /**
     * @var array
     */
    //protected $fillable = ['ApplId', 'StatusDt', 'AppStatus'];

    

}
