<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property boolean $Act
 * @property string $AcadCode
 * @property string $AcadDesc
 */
class Models/Table2/codeAcademic extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'CodeAcademics';

    /**
     * The primary key for the model.
     * 
     * @var string
     */

    
    protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['CreatedBy', 'CreatedDate', 'Act', 'AcadCode', 'AcadDesc'];

}
