<?php

namespace App\Models\Table2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $Id
 * @property string $UserRole
 * @property string $FrameId
 * @property string $UserView
 * @property string $UserAdd
 * @property string $UserEdit
 * @property string $UserDel
 */
class sysUserAccPrivTabs extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $connection = 'sqlsrv2';
    protected $table = 'sysUserAccPrivTabs';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['UserRole', 'FrameId', 'UserView', 'UserAdd', 'UserEdit', 'UserDel'];

}
