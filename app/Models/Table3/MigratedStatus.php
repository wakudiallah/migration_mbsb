<?php

namespace App\Models\Table3;

use Illuminate\Database\Eloquent\Model;
use DB;



class MigratedStatus extends Model
{
    
    
    protected $table = 'migrated_statuses'; 


    protected $guarded = ["id"];     
    public $timestamps = true;


}
