<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMigratedStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlsrv3')->create('migrated_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('table', 100)->nullable();
            $table->integer('total_row_origin')->nullable();
            $table->integer('total_row_destination')->nullable();
            $table->string('status', 10)->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('migrated_statuses');
    }
}
