<style>
  .footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    color: #818d9e;
    text-align: center;
  }
</style>

        <div class="nav-wrapper">
            <ul class="nav flex-column">

              <!-- Dashboard  -->    
             
              
              
              <li class="nav-item">
                <a class="nav-link" href="{{url('/waps')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Dashboard</span>
                </a>
              </li>

              <!-- <li class="nav-item">
                <a class="nav-link" href="{{url('/Update-FAName')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Update FAName</span>
                </a>
              </li> -->

              <li class="nav-item">
                <a class="nav-link" href="{{url('migration-status')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Status Migrate</span>
                </a>
              </li>


              <li class="nav-item">
                <a class="nav-link" href="{{url('migrate-user')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Migrate User Table</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="{{url('migrate-seeder')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Migrate Seeder Table</span>
                </a>
              </li>
              

              <li class="nav-item">
                <a class="nav-link" href="{{url('migrate-parameter')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Migrate Parameter Table</span>
                </a>
              </li>


              <li class="nav-item">
                <a class="nav-link" href="{{url('migrate-process')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Migrate Process Table</span>
                </a>
              </li>
         

               <!-- <img src="{{asset('images/mari-logo.png')}}"> -->

            </ul>
          </div>



      <div class="footer">
        <img src="{{asset('image/netxpert.jpg')}}" class="img-responsive" width="20%" height="20%">
        <p style="font-size: 10px">Powered By Netxpert <br>
        v1.0.0.0 [Feb2021]</p>
      </div>


      <footer class="main-footer">
        <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="#">Home</a>
            </li>
          </ul>
        <img src="{{asset('images/mari2.png')}}">
      </footer>


    <script src='http://code.jquery.com/jquery-latest.js'></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     


      <script type="text/javascript">

    $(function () {


      (function () {

        var aside = $('.side-nav'),

            showAsideBtn = $('.show-side-btn'),

            contents = $('#contents');

        showAsideBtn.on("click", function () {

          $("#" + $(this).data('show')).toggleClass('show-side-nav');

          contents.toggleClass('margin');

        });

        if ($(window).width() <= 767) {

          aside.addClass('show-side-nav');

        }
        $(window).on('resize', function () {

          if ($(window).width() > 767) {

            aside.removeClass('show-side-nav');

          }

        });

        // dropdown menu in the side nav
        var slideNavDropdown = $('.side-nav-dropdown');

        $('.side-nav .categories li').on('click', function () {

          $(this).toggleClass('opend').siblings().removeClass('opend');

          if ($(this).hasClass('opend')) {

            $(this).find('.side-nav-dropdown').slideToggle('fast');

            $(this).siblings().find('.side-nav-dropdown').slideUp('fast');

          } else {

            $(this).find('.side-nav-dropdown').slideUp('fast');

          }

        });

        $('.side-nav .close-aside').on('click', function () {

          $('#' + $(this).data('close')).addClass('show-side-nav');

          contents.removeClass('margin');

        });

      }());

      // Start chart

      

    });
      </script>