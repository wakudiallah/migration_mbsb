@extends('layout.template')

@section('content')


    <script src="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap.min.css"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap.min.css"></script>


<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Migration Status</span>
        <h3 class="page-title">Migration Database System MBSB</h3>
      </div>
    </div>
    <!-- End Page Header -->
    <!-- Small Stats Blocks --> 

    <!-- End Small Stats Blocks -->
    <div class="row">
      <!-- Users Stats -->
      <div class="col-lg-12 col-md-12">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Get Table Database1 </h6>
          </div>
          <div class="card-body pt-0">
            
            <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Table</th>
                      <th>Total Row Origin]</th>
                      <th>Total Row Destination</th>
                      <th>status</th>
                    </tr>
                    
                  </thead>

                  <tbody>
                     @php $i = 1; @endphp
                     @foreach($datas as $data)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$data->table}}</td>
                      <td>{{$data->total_row_origin}}</td>
                      <td>{{$data->total_row_destination}}</td>
                      <td>{{$data->status}}</td>

                    </tr>
                    @endforeach
                  </tbody>
              </table>
                </div>

          </div>
        </div>
      </div>
     
      <!-- End Users Stats -->

     
      
    </div>

    
</div>


@endsection

@push('js')


  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
     
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            } );
        } );


        $(document).ready(function() {
            $('#example2').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            } );
        } );

  </script>


  <script type="text/javascript">
    $(document).ready(function () {
$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');
});
  </script>


   <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>

@endpush