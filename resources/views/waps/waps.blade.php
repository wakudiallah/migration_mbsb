@extends('layout.template')

@section('content')



    <script src="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap.min.css"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap.min.css"></script>


<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        
        <h3 class="page-title">Migration System MBSB</h3>
      </div>
    </div>
    <!-- End Page Header -->
    <!-- Small Stats Blocks -->


    <div class="row">
      
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Database Origin </span>

                

               
                <h6 class="stats-small__value count my-3" style="color: red">
                {{$database1}} </h6>


               
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>
      
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Database Dest </span>
                <h6 class="stats-small__value count my-3">{{$database2}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-2"></canvas>
          </div>
        </div>
      </div>
 
    </div>


    <!-- End Small Stats Blocks -->
    <div class="row">
      <!-- Users Stats -->
      <div class="col-lg-6 col-md-6">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Get Table Database1 </h6>
          </div>
          <div class="card-body pt-0">
            
            <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
                  <thead>
                    <tr>
                      <th>No </th>
                      <th>Table</th>
                      <th>Field</th>
                    </tr>
                    
                  </thead>

                  <tbody>
                     @php $i = 1; @endphp
                     @foreach($table1 as $data)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$data->TABLE_NAME}}</td>
                      <td>
                        @php

                            $columns = Schema::connection('sqlsrv')->getColumnListing($data->TABLE_NAME);
                        @endphp

                        {{json_encode($columns,TRUE)}}
                        
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
                </div>

          </div>
        </div>
      </div>
     
      <!-- End Users Stats -->

      <!-- Users By Device Stats -->
      <div class="col-lg-6 col-md-6">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Get Table Database2</h6>
          </div>
          <div class="card-body d-flex py-0">
            
            <div class="table-responsive">
            <table id="example2" class="table table-striped table-bordered nowrap" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Table</th>
                      <th>Seeder</th>
                      <th>Process</th>
                      <th>Field</th>
                      
                    </tr>
                    
                  </thead>

                  <tbody>
                    @php $i = 1; @endphp
                    @foreach($table2 as $data)
                      <tr>
                        <td>{{$i++}}</td>
                        <td>
                            <a href="{{'migrate/'.$data->TABLE_NAME}}">{{$data->TABLE_NAME}}</a>
                        </td>
                        <td>
                            <a href="{{'seeder/'.$data->TABLE_NAME}}">{{$data->TABLE_NAME}}</a>
                        </td>

                        <td>
                            <a href="{{'process/'.$data->TABLE_NAME}}">{{$data->TABLE_NAME}}</a>
                        </td>
                        <td>
                            @php
                            $columns = Schema::connection('sqlsrv2')->getColumnListing($data->TABLE_NAME);
                            @endphp

                        {{ json_encode($columns,TRUE)}}
                        </td>
                        

                      </tr>
                    @endforeach
                  </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
      <!-- End Users By Device Stats -->
      
      
    </div>

   
</div>


@endsection

@push('js')

   

  
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
     
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            } );
        } );


        $(document).ready(function() {
            $('#example2').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            } );
        } );

  </script>

  <script type="text/javascript">
    $(document).ready(function() {
    $('#dataTable').DataTable();
});
  </script>


  <script type="text/javascript">
    $(document).ready(function () {
$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');
});
  </script>


     <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>




@endpush