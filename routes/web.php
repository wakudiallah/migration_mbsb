<?php

use Illuminate\Support\Facades\Redis;



Route::get('/redis', function () {
    $p = Redis::incr('p');
    return $p;
});



Route::get('/', 'WapsController@index');
Route::get('/waps', 'WapsController@index');
Route::get('/wapsX', 'WapsController@index');
Route::get('/migration-status', 'WapsController@migration_status');
Route::get('/Update-FAName', 'WapsController@update_faname');

Route::get('/Update/ApplRefNoTabTemps', 'ProcessController@update_ApplRefNoTabTemps');



//Route::get('/seeder-user', 'WapsController@seeder_user');

/*Route::get('/migrate-user', function () {
    return view('waps.user');
});*/

Route::get('/migrate-user', 'WapsController@migrate_user');
Route::get('/migrate-seeder', 'WapsController@migrate_seeder');
Route::get('/migrate-parameter', 'WapsController@migrate_parameter');
Route::get('/migrate-process', 'WapsController@migrate_process');


	Route::group(['prefix' => 'User'], function () {
	
		Route::get('/AspNetUsers', 'WapsController@AspNetUsers');	

	});


Route::get('/alertxx', 'WapsController@alert');



	Route::group(['prefix' => 'seeder'], function () {
	
		//Route::get('/CodeProducts', 'SeederController@CodeProducts');
		Route::get('/CodeCollectionTypes', 'SeederController@CodeCollectionTypes');
		Route::get('/CodeSecuParams', 'SeederController@CodeSecuParams');
		Route::get('/CodeLoanTenors', 'SeederController@CodeLoanTenors');
		Route::get('/CodeTypeOfProducts', 'SeederController@CodeTypeOfProducts');
		
		Route::get('/AspNetRoles', 'SeederController@AspNetRoles');

		Route::get('/CodeNextRoutes', 'SeederController@CodeNextRoutes');
		Route::get('/CodeAngkasaCodes', 'SeederController@CodeAngkasaCodes');

		Route::get('/sysFrameTabs', 'SeederController@sysFrameTabs');

	});


	Route::group(['prefix' => 'seederNA'], function () {
	
		Route::get('/NA', 'SeederNAController@NA');	

	});


	Route::group(['prefix' => 'Parameter1'], function () {
	
		Route::get('/Parameter1', 'Parameter1Controller@Parameter1');	

	});


	Route::group(['prefix' => 'user'], function () {
	
		Route::get('/user', 'UserController@user');	

	});



	Route::group(['prefix' => 'process'], function () {
		
		Route::get('/ApplApproves', 'ProcessController@ApplApproves');
		Route::get('/ApplCheckLists', 'ProcessController@ApplCheckLists');
		Route::get('/ApplWorkFlows', 'ProcessController@ApplWorkFlows');
		Route::get('/ApplDisburses', 'ProcessController@ApplDisburses');
		Route::get('/ApplKpis', 'ProcessController@ApplKpis');
		Route::get('/CaafRedemptions', 'ProcessController@CaafRedemptions');
		Route::get('/CaafFacilities', 'ProcessController@CaafFacilities');

		Route::get('/Cifs', 'ProcessController@Cifs');
				
		Route::get('/Blas', 'ProcessController@Blas');
		Route::get('/CaafTabs', 'ProcessController@CaafTabs');
		Route::get('/ApplRefNoTabTemps', 'ProcessController@ApplRefNoTabTemps'); //
		
		Route::get('/ApplRefNoTabTempsNew', 'ProcessController@ApplRefNoTabTempsNew');

		Route::get('/wapsijal', 'ProcessController@ijal');

		Route::get('/ApplRefNoTabs', 'ProcessController@ApplRefNoTabs'); //
		Route::get('/ApplWorkFlows', 'ProcessController@ApplWorkFlows');
		Route::get('/Addresses', 'ProcessController@Addresses');

		Route::get('/CaafUnsExposures', 'ProcessController@CaafUnsExposures');
		Route::get('/CalAbsentTabs', 'ProcessController@CalAbsentTabs');
		Route::get('/DSP_CifDetailGSTs', 'ProcessController@DSP_CifDetailGSTs');
		Route::get('/DSP_CifDetailGST_1', 'ProcessController@DSP_CifDetailGST_1');

		

		Route::get('/Dsp_CifMastDetails', 'ProcessController@Dsp_CifMastDetails');
		Route::get('/Dsp_CifMastDetail_1', 'ProcessController@Dsp_CifMastDetail_1');



		Route::get('/DSP_EmpDetails', 'ProcessController@DSP_EmpDetails');
		Route::get('/DSP_EmpDetail_1', 'ProcessController@DSP_EmpDetail_1');

		Route::get('/Dsp_GetCifNos', 'ProcessController@Dsp_GetCifNos');
		Route::get('/DSP_GetCifNo_1', 'ProcessController@DSP_GetCifNo_1');

		Route::get('/Guarantors', 'ProcessController@Guarantors');


		
		Route::get('/SumCurrentApplStats', 'ProcessController@SumCurrentApplStats');//

		Route::get('/UserTaskLists', 'ProcessController@UserTaskLists'); //

		Route::get('/SubmissionTypes', 'ProcessController@SubmissionTypes');

		Route::get('/ISms_Incomp', 'ProcessController@ISms_Incomp');
		Route::get('/CMSCaafRedemptions', 'ProcessController@CMSCaafRedemptions');
		Route::get('/Dsp_Creation_AccNos', 'ProcessController@Dsp_Creation_AccNos');
		Route::get('/Dsp_AA_Facilitys', 'ProcessController@Dsp_AA_Facilitys');
		Route::get('/Dsp_Islamics', 'ProcessController@Dsp_Islamics');
		
	});



	Route::group(['prefix' => 'migrate'], function () {
		
		Route::get('/CodeAcademics', 'WapsController@CodeAcademics');
		Route::get('/CodeAddTypes', 'WapsController@CodeAddTypes');
		Route::get('/CodeAdminFees', 'WapsController@CodeAdminFees'); //pass
		Route::get('/CodeAGBranches', 'WapsController@CodeAGBranches');
		Route::get('/CodeAGDepts', 'WapsController@CodeAGDepts');
		Route::get('/CodeAGPayCenters', 'WapsController@CodeAGPayCenters');  //Pass sbb ada Postcode City State
		/*cek lagi*/	
		Route::get('/CodeBanks', 'WapsController@CodeBanks'); 
		Route::get('/CodeAkadScripts', 'WapsController@CodeAkadScripts');
		Route::get('/CodeAppAuts', 'WapsController@CodeAppAuts');
		Route::get('/CodeAppStatuss', 'WapsController@CodeAppStatuss');
		Route::get('/CodeBankrups', 'WapsController@CodeBankrups');
		Route::get('/CodeClrZones', 'WapsController@CodeClrZones');
		Route::get('/CodeCreditGrades', 'WapsController@CodeCreditGrades');
		Route::get('/CodeDCGroups', 'WapsController@CodeDCGroups');
		Route::get('/CodeDelimets', 'WapsController@CodeDelimets');
		Route::get('/codeDepartments', 'WapsController@codeDepartments');
		Route::get('/codeDeviations', 'WapsController@codeDeviations');
		Route::get('/CodeDispatchTos', 'WapsController@CodeDispatchTos');
		Route::get('/CodeEmpTys', 'WapsController@CodeEmpTys');
		Route::get('/CodeInss', 'WapsController@CodeInss');
		Route::get('/CodeMaritals', 'WapsController@CodeMaritals');
		Route::get('/CodeNaOfBusss', 'WapsController@CodeNaOfBusss');
		Route::get('/CodeIDTypes', 'WapsController@CodeIDTypes');
		Route::get('/CodeNegaras', 'WapsController@CodeNegaras');
		Route::get('/CodeNegeris', 'WapsController@CodeNegeris');
		Route::get('/CodeOccupations', 'WapsController@CodeOccupations');
		Route::get('/CodePayeeTypes', 'WapsController@CodePayeeTypes');
		Route::get('/CodePendingReass', 'WapsController@CodePendingReass');
		Route::get('/CodeProcess', 'WapsController@CodeProcess');
		Route::get('/CodeProducts', 'WapsController@CodeProducts');
		Route::get('/CodeRaces', 'WapsController@CodeRaces');
		Route::get('/CodeRegions', 'WapsController@CodeRegions');
		Route::get('/CodeRelations', 'WapsController@CodeRelations');
		Route::get('/CodeReligions', 'WapsController@CodeReligions');
		Route::get('/CodeRemarkss', 'WapsController@CodeRemarkss');
		Route::get('/CodeRepaymentTypes', 'WapsController@CodeRepaymentTypes');
		Route::get('/CodeResidencetys', 'WapsController@CodeResidencetys');
		Route::get('/CodeSubmitTypes', 'WapsController@CodeSubmitTypes');
		Route::get('/CodeTitles', 'WapsController@CodeTitles');
		Route::get('/CodeTransactions', 'WapsController@CodeTransactions');
		Route::get('/CodeUserStats', 'WapsController@CodeUserStats');
		Route::get('/CodeWorkSecs', 'WapsController@CodeWorkSecs');
		Route::get('/CodePositions', 'WapsController@CodePositions');
		Route::get('/CodeGLs', 'WapsController@CodeGLs');
		Route::get('/CodeGenders', 'WapsController@CodeGenders');
		Route::get('/codeEntities', 'WapsController@codeEntities');
		Route::get('/CodeEmploySector', 'WapsController@CodeEmploySector');
		Route::get('/CodeBumis', 'WapsController@CodeBumis');
		Route::get('/CodeAGStates', 'WapsController@CodeAGStates');
		Route::get('/CodePostCodes', 'WapsController@CodePostCodes');
		Route::get('/CodeJobStatuss', 'WapsController@CodeJobStatuss');
		Route::get('/CodeModeOfPayments', 'WapsController@CodeModeOfPayments');
		Route::get('/CodeMOfficers', 'WapsController@CodeMOfficers');
		Route::get('/CodeCmsCancels', 'WapsController@CodeCmsCancels');
		Route::get('/CodeConvertJobPosts', 'WapsController@CodeConvertJobPosts');
		Route::get('/CodeCeilingRates', 'WapsController@CodeCeilingRates');
		Route::get('/CodeCheckLists', 'WapsController@CodeCheckLists');
		Route::get('/CodeRejectReass', 'WapsController@CodeRejectReass');
		Route::get('/CodeVarianceRates', 'WapsController@CodeVarianceRates');
		Route::get('/CodeBranches', 'WapsController@CodeBranches');
		Route::get('/CodeCoopBranchs', 'WapsController@CodeCoopBranchs');
		Route::get('/CodeCoop', 'WapsController@CodeCoop');
		Route::get('/CodeSettlePtys', 'WapsController@CodeSettlePtys');
		//Route::get('/CodeSecuParams', 'WapsController@CodeSecuParams');
		Route::get('/CodeBeneBank', 'WapsController@CodeBeneBank');
		Route::get('/CodeBeneficiarys', 'WapsController@CodeBeneficiarys');
		Route::get('/CodeIntPackages', 'WapsController@CodeIntPackages');
		Route::get('/CodeDealers', 'WapsController@CodeDealers');
		Route::get('/CodeDealerBrchs', 'WapsController@CodeDealerBrchs');
		Route::get('/CodeARBs', 'WapsController@CodeARBs');
		Route::get('/CodeCollectionTypes', 'WapsController@CodeCollectionTypes');
		Route::get('/CodeISaves', 'WapsController@CodeISaves');
		Route::get('/CodeLoanPackages', 'WapsController@CodeLoanPackages');
		Route::get('/CodeMopRedemps', 'WapsController@CodeMopRedemps');
		Route::get('/CodeWakalahs', 'WapsController@CodeWakalahs');
		Route::get('/InsPrems', 'WapsController@InsPrems');
		Route::get('/OnlineCheckSetups', 'WapsController@OnlineCheckSetups');

		Route::get('/CCRISTaxonomys', 'WapsController@CCRISTaxonomys');


	});
